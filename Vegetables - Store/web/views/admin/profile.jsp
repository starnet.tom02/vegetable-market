<%-- 
    Document   : profile
    Created on : 21-10-2022, 18:27:43
    Author     : phanh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html class="no-js " lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

        <title>:: Aero Bootstrap4 Admin :: Blank</title>
        <!-- Favicon-->
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="admin/assets/plugins/bootstrap/css/bootstrap.min.css">
        <!-- Custom Css -->
        <link rel="stylesheet" href="admin/assets/css/style.min.css">
    </head>

    <body class="theme-blue">

        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="m-t-30"><img class="zmdi-hc-spin" src="admin/assets/images/loader.svg" width="48" height="48" alt="Aero"></div>
                <p>Please wait...</p>
            </div>
        </div>

        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>

        <!-- Main Search -->


        <!-- Right Icon menu Sidebar -->
        <div class="navbar-right">
            <ul class="navbar-nav">

                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" title="App" data-toggle="dropdown" role="button"><i class="zmdi zmdi-apps"></i></a>
                    <ul class="dropdown-menu slideUp2">
                        <li class="header">App Sortcute</li>
                        <li class="body">
                            <ul class="menu app_sortcut list-unstyled">
                                <li>
                                    <a href="image-gallery.html">
                                        <div class="icon-circle mb-2 bg-blue"><i class="zmdi zmdi-camera"></i></div>
                                        <p class="mb-0">Photos</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle mb-2 bg-amber"><i class="zmdi zmdi-translate"></i></div>
                                        <p class="mb-0">Translate</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="events.html">
                                        <div class="icon-circle mb-2 bg-green"><i class="zmdi zmdi-calendar"></i></div>
                                        <p class="mb-0">Calendar</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="contact.html">
                                        <div class="icon-circle mb-2 bg-purple"><i class="zmdi zmdi-account-calendar"></i></div>
                                        <p class="mb-0">Contacts</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle mb-2 bg-red"><i class="zmdi zmdi-tag"></i></div>
                                        <p class="mb-0">News</p>
                                    </a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="js-right-sidebar" title="Setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>
                <li><a href="a-logout" class="mega-menu" title="Sign Out"><i class="zmdi zmdi-power"></i></a></li>
            </ul>
        </div>

        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <div class="navbar-brand">
                <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>

                <a href="a-home"><img src="admin/assets/images/logo.svg" width="25" alt="Aero"><span class="m-l-10">ADMIN</span></a>
            </div>
            <div class="menu">
                <ul class="list">
                    <li>
                        <div class="user-info">
                            <a class="image" href="a-profile"><img src="${account.avatar}" alt="User"></a>
                            <div class="detail">
                                <h4>${account.fullName}</h4>
                                <small>Admin</small>                        
                            </div>
                        </div>
                    </li>
                    <li class="active"><a href="a-home"><i class="zmdi zmdi-home"></i><span>Home</span></a></li>
                    <li class="open"><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-shopping-cart"></i><span>Product</span></a>
                        <ul class="ml-menu">
                            <li><a href="a-product-list">Product List</a></li>
                            <li><a href="a-order-list">Order List</a></li>
                        </ul>
                    </li>   
                    <li class="open_top"><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-account"></i><span>Account Controller</span></a>
                        <ul class="ml-menu">
                            <li><a href="blank.html">Blank Page</a></li>
                            <li><a href="image-gallery.html">Image Gallery</a></li>
                            <li><a href="profile.html">Profile</a></li>
                            <li><a href="timeline.html">Timeline</a></li>
                            <li><a href="pricing.html">Pricing</a></li>
                            <li><a href="invoices.html">Invoices</a></li>
                            <li><a href="invoices-list.html">Invoices List</a></li>
                            <li><a href="search-results.html">Search Results</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-chart"></i><span>Charts</span></a>
                        <ul class="ml-menu">
                            <li><a href="c3.html">C3 Chart</a></li>
                            <li><a href="morris.html">Morris</a></li>
                            <li><a href="flot.html">Flot</a></li>
                            <li><a href="chartjs.html">ChartJS</a></li>
                            <li><a href="sparkline.html">Sparkline</a></li>
                            <li><a href="jquery-knob.html">Jquery Knob</a></li>
                        </ul>
                    </li>            

                </ul>
            </div>
        </aside>

        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs sm">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>

            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="setting">
                    <div class="slim_scroll">
                        <div class="card">
                            <h6>Theme Option</h6>
                            <div class="light_dark">
                                <div class="radio">
                                    <input type="radio" name="radio1" id="lighttheme" value="light" checked="">
                                    <label for="lighttheme">Light Mode</label>
                                </div>
                                <div class="radio mb-0">
                                    <input type="radio" name="radio1" id="darktheme" value="dark">
                                    <label for="darktheme">Dark Mode</label>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>                
            </div>       
        </div>
    </aside>

        <!-- Main Content -->
        <section class="content">
            <div class="body_scroll">
                <div class="container-fluid">
                    <div class="row clearfix">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="header">
                                    <h2><strong>Profiles </strong></h2>
                                </div>
                                <div class="body">
                                    <form action="" method="post" name="Formedit">
                                        <div class="row clearfix">

                                            <div class="col-md-8">
                                                <div class="form-group form-float">
                                                    <label>UserName: </label>
                                                    <input type="text" name="userName" class="form-control" value="${account.userName}" placeholder="First Name" required disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-md-4">
                                                <div class="form-group form-float">
                                                    <label>First Name:</label>
                                                    <input type="text" id="firstName" name="firstName" class="form-control" value="${account.firstName}" placeholder="First Name" required disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-float">
                                                    <label>Last Name:</label>
                                                    <input type="text" id="lastName" name="lastName" class="form-control" value="${account.lastName}" placeholder="First Name" required disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-md-8">
                                                <div class="form-group form-float">
                                                    <label>Email:</label>
                                                    <input type="email" id="email" name="email" class="form-control" value="${account.email}" placeholder="First Name" required disabled>
                                                </div>
                                            </div>

                                            <div class="col-md-8">
                                                <div class="form-group form-float">
                                                    <label>Phone:</label>
                                                    <input type="text" id="phone" name="phone" class="form-control" value="${account.phone}" placeholder="First Name" required disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group form-float">
                                                    <label>Avatar: </label>
                                                    <img src="${account.avatar}" class="ml-4" style="border-radius: 50%" width="100px" alt="avatar"/>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group form-float">
                                                    <div class="radio inlineblock m-r-20">
                                                        <input type="radio" name="gender" id="male" class="with-gap" value="1" ${account.gender == 1 ? 'checked' : ''} disabled>
                                                        <label for="male">Male</label>
                                                    </div>                                
                                                    <div class="radio inlineblock">
                                                        <input type="radio" name="gender" id="Female" class="with-gap" value="2" ${account.gender == 0 ? 'checked' : ''} disabled>
                                                        <label for="Female">Female</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-8">
                                                <div class="form-group form-float">
                                                    <label>Country:</label>
                                                    <select class="form-control show-tick ms select2" data-placeholder="Select" id="countryID" name="countryID" disabled>
                                                        <c:forEach items="${country}" var="c">
                                                            <option value="${c.countryID}">${c.countryName}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>


                                            </div>  


                                            <div class="col-md-8">
                                                <div class="form-group form-float">
                                                    <label>Address:</label>
                                                    <input type="text" id="address" name="address" class="form-control" value="${account.address}" placeholder="First Name" required disabled>
                                                </div>
                                            </div>   
                                        </div>
                                        <input onclick="return editProfile()" id="edit-profiles" class="btn btn-raised btn-primary" type="button" value="Edit">
                                        
                                        <script type="text/javascript">
                                            function editProfile() {
                                                document.getElementById('firstName').disabled = false;
                                                document.getElementById('lastName').disabled = false;
                                                document.getElementById('address').disabled = false;
                                                document.getElementById('phone').disabled = false;
                                                document.getElementById('email').disabled = false;
                                                document.getElementById('male').disabled = false;
                                                document.getElementById('Female').disabled = false;
                                                document.getElementById('countryID').disabled = false;
                                                document.getElementById('edit-profiles').value = "SAVE";
                                                if (document.getElementById('edit-profiles').type !== "submit") {
                                                    document.getElementById('edit-profiles').type = "submit";
                                                    return false;
                                                } else {
                                                    document.Formedit.submit();
                                                    return true;
                                                }
                                            }
                                        </script>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- Jquery Core Js --> 
        <script src="admin/assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
        <script src="admin/assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

        <script src="admin/assets/bundles/mainscripts.bundle.js"></script>
    </body>

</html>
