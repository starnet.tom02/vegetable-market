<%-- 
    Document   : order
    Created on : 20-10-2022, 19:30:32
    Author     : phanh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html class="no-js " lang="en">


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

        <title>:: Aero Bootstrap4 Admin :: Invoice</title>
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <!-- Favicon-->
        <link rel="stylesheet" href="admin/assets/plugins/bootstrap/css/bootstrap.min.css">
        <!-- Custom Css -->
        <link rel="stylesheet" href="admin/assets/css/style.min.css">
    </head>

    <body class="theme-blue">

        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="m-t-30"><img class="zmdi-hc-spin" src="admin/assets/images/loader.svg" width="48" height="48" alt="Aero"></div>
                <p>Please wait...</p>
            </div>
        </div>

        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>

        <!-- Main Search -->


        <!-- Right Icon menu Sidebar -->
        <div class="navbar-right">
            <ul class="navbar-nav">

                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" title="App" data-toggle="dropdown" role="button"><i class="zmdi zmdi-apps"></i></a>
                    <ul class="dropdown-menu slideUp2">
                        <li class="header">App Sortcute</li>
                        <li class="body">
                            <ul class="menu app_sortcut list-unstyled">
                                <li>
                                    <a href="image-gallery.html">
                                        <div class="icon-circle mb-2 bg-blue"><i class="zmdi zmdi-camera"></i></div>
                                        <p class="mb-0">Photos</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle mb-2 bg-amber"><i class="zmdi zmdi-translate"></i></div>
                                        <p class="mb-0">Translate</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="events.html">
                                        <div class="icon-circle mb-2 bg-green"><i class="zmdi zmdi-calendar"></i></div>
                                        <p class="mb-0">Calendar</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="contact.html">
                                        <div class="icon-circle mb-2 bg-purple"><i class="zmdi zmdi-account-calendar"></i></div>
                                        <p class="mb-0">Contacts</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle mb-2 bg-red"><i class="zmdi zmdi-tag"></i></div>
                                        <p class="mb-0">News</p>
                                    </a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="js-right-sidebar" title="Setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>
                <li><a href="a-logout" class="mega-menu" title="Sign Out"><i class="zmdi zmdi-power"></i></a></li>
            </ul>
        </div>

        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <div class="navbar-brand">
                <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>

                <a href="a-home"><img src="admin/assets/images/logo.svg" width="25" alt="Aero"><span class="m-l-10">ADMIN</span></a>
            </div>
            <div class="menu">
                <ul class="list">
                    <li>
                        <div class="user-info">
                            <a class="image" href="a-profile"><img src="${account.avatar}" alt="User"></a>
                            <div class="detail">
                                <h4>${account.fullName}</h4>
                                <small>Admin</small>                        
                            </div>
                        </div>
                    </li>
                    <li class="active"><a href="a-home"><i class="zmdi zmdi-home"></i><span>Home</span></a></li>
                    <li class="open"><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-shopping-cart"></i><span>Product</span></a>
                        <ul class="ml-menu">
                            <li><a href="a-product-list">Product List</a></li>
                            <li><a href="a-order-list">Order List</a></li>
                        </ul>
                    </li>   
                    <li class="open_top"><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-account"></i><span>Account Controller</span></a>
                        <ul class="ml-menu">
                            <li><a href="blank.html">Blank Page</a></li>
                            <li><a href="image-gallery.html">Image Gallery</a></li>
                            <li><a href="profile.html">Profile</a></li>
                            <li><a href="timeline.html">Timeline</a></li>
                            <li><a href="pricing.html">Pricing</a></li>
                            <li><a href="invoices.html">Invoices</a></li>
                            <li><a href="invoices-list.html">Invoices List</a></li>
                            <li><a href="search-results.html">Search Results</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-chart"></i><span>Charts</span></a>
                        <ul class="ml-menu">
                            <li><a href="c3.html">C3 Chart</a></li>
                            <li><a href="morris.html">Morris</a></li>
                            <li><a href="flot.html">Flot</a></li>
                            <li><a href="chartjs.html">ChartJS</a></li>
                            <li><a href="sparkline.html">Sparkline</a></li>
                            <li><a href="jquery-knob.html">Jquery Knob</a></li>
                        </ul>
                    </li>            

                </ul>
            </div>
        </aside>

        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs sm">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>

            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="setting">
                    <div class="slim_scroll">
                        <div class="card">
                            <h6>Theme Option</h6>
                            <div class="light_dark">
                                <div class="radio">
                                    <input type="radio" name="radio1" id="lighttheme" value="light" checked="">
                                    <label for="lighttheme">Light Mode</label>
                                </div>
                                <div class="radio mb-0">
                                    <input type="radio" name="radio1" id="darktheme" value="dark">
                                    <label for="darktheme">Dark Mode</label>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>                
            </div>       
        </div>
    </aside>

        <section class="content">
            <div class="body_scroll">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-7 col-md-6 col-sm-12">
                            <h2>Chi Tiết Đơn Hàng</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="a-home"><i class="zmdi zmdi-home"></i> Home</a></li>
                                <li class="breadcrumb-item active"><a href="a-order-list">Order List</a></li>
                                <li class="breadcrumb-item active">Invoice</li>
                            </ul>
                            <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                        </div>
                        <div class="col-lg-5 col-md-6 col-sm-12">                
                            <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row clearfix">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="body">
                                    <h5><strong>Order ID: </strong> #${order.orderID}</h5>
                                    <div class="row">                                
                                        <div class="col-md-6 col-sm-6">
                                            <address>
                                                <strong>${order.account.country.countryName}</strong><br>
                                                ${order.address}<br>
                                                <abbr title="Phone">Phone:</abbr> (+84) 0838456798
                                            </address>
                                        </div>
                                        <div class="col-md-6 col-sm-6 text-right">
                                            <p class="mb-0"><strong>Order Date: </strong> ${order.orderTime}</p>
                                            <p class="mb-0"><strong>Order Status: </strong><span class="badge ${order.status eq 'Chờ Xử Lý' ? 'badge-warning' : 'badge-success'} ">${order.status}</span></p>                                    
                                        </div>
                                    </div>
                                </div>                            
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-hover c_table theme-color">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th width="10%">Item</th>
                                                        <th>Tên Sản Phẩm</th>
                                                        <th>Số Lượng</th>
                                                        <th class="hidden-sm-down">Giá Tiền</th>
                                                        <th>Thành Tiền</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:set var="subTotal" value="0"/>
                                                    <c:forEach items="${orderDetails}" var="o">
                                                        <tr>
                                                            <td>${o.orderDetailID}</td>
                                                            <td><img src="${o.product.image.image1}" width="70%" alt="Product img"></td>
                                                            <td>${o.product.productName}</td>
                                                            <td>${o.quantity}</td>
                                                            <td class="hidden-sm-down">${o.price} đ</td>
                                                            <td>${o.totalMoney} đ</td>
                                                        </tr>
                                                        <c:set var="subTotal" value="${subTotal + o.totalMoney}"/>
                                                    </c:forEach>
                                                    <tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>Note</h5>
                                            <p>${order.note}</p>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <ul class="list-unstyled">
                                                <li><strong>Sub-Total:-</strong> ${subTotal} đ</li>
                                                <li class="text-danger"><strong>Discout: </strong> 0%</li>
                                                <li><strong>VAT: </strong> 0%</li>
                                            </ul>                                    
                                            <h3 class="mb-0 text-success">${order.totalMoney} VND</h3>
                                            <a href="javascript:void(0);" class="btn btn-info"><i class="zmdi zmdi-print"></i></a>
                                            <a href="javascript:void(0);" class="btn btn-primary">Submit</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Jquery Core Js --> 
        <script src="admin/assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
        <script src="admin/assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

        <script src="admin/assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
    </body>


</html>
