<%-- 
    Document   : shop
    Created on : 18-10-2022, 13:44:23
    Author     : phanh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Cairo:400,600,700&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:600&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400i,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu&amp;display=swap" rel="stylesheet">
        <link rel="shortcut icon" type="image/x-icon" href="user/assets/images/favicon.png" />
        <link rel="stylesheet" href="user/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="user/assets/css/animate.min.css">
        <link rel="stylesheet" href="user/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="user/assets/css/nice-select.css">
        <link rel="stylesheet" href="user/assets/css/slick.min.css">
        <link rel="stylesheet" href="user/assets/css/style.css">
        <link rel="stylesheet" href="user/assets/css/main-color.css">

        <style>
            * {
                font-family: initial;
            }
        </style>
        <title>Candles Store</title>
    </head>
    <body class="biolife-body">

        <!-- Preloader -->
        <div id="biof-loading">
            <div class="biof-loading-center">
                <div class="biof-loading-center-absolute">
                    <div class="dot dot-one"></div>
                    <div class="dot dot-two"></div>
                    <div class="dot dot-three"></div>
                </div>
            </div>
        </div>

        <!-- HEADER -->
        <header id="header" class="header-area style-01 layout-03">
            <div class="header-top bg-main hidden-xs">
                <div class="container-fluid">
                    <div class="top-bar left">
                        <ul class="horizontal-menu">
                            <li><a href="#"><i class="fa fa-truck" aria-hidden="true"></i>Miễn Phí Ship Với Đơn Hàng Từ 399k</a></li>
                            <li><a href="#"></a></li>
                        </ul> 
                    </div>
                    <div class="top-bar right">
                        <ul class="horizontal-menu">
                            <li>
                                <c:if test="${account == null}">
                                    <a href="login" class="login-link">
                                        <i class="biolife-icon icon-login"></i>Login/Register</a>
                                    </c:if>

                                <c:if test="${account != null}">
                                    <a href="profiles" class="login-link">
                                        <i class="biolife-icon icon-login"></i>Hello: ${account.fullName}</a>
                                    <a href="logout" class="login-link" style="margin-left: 1rem">
                                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                                        Logout</a>
                                    </c:if>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="header-middle biolife-sticky-object ">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-2 col-md-6 col-xs-6">
                            <a href="home" class="biolife-logo">
                                <img src="user/assets/images/organic-3.png" alt="biolife logo" width="135" height="34">
                            </a>
                        </div>
                        <div class="col-lg-6 col-md-7 hidden-sm hidden-xs">
                            <div class="primary-menu">
                                <ul class="menu biolife-menu clone-main-menu clone-primary-menu" id="primary-menu" data-menuname="main menu">
                                    <li class="menu-item"><a href="home">Home</a></li>
                                    <li class="menu-item"> <a href="shop">Shop</a></li>
                                    <li class="menu-item menu-item-has-children has-child">
                                        <a href="#" class="menu-name" data-title="Products">Nổi Bật</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item"><a href="#">top1</a></li>
                                            <li class="menu-item"><a href="#">top2</a></li>
                                            <li class="menu-item menu-item-has-children has-child"><a href="#" class="menu-name" data-title="Eggs & other considerations">top3</a>
                                                <ul class="sub-menu">
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item"><a href="#">top4</a></li>
                                            <li class="menu-item"><a href="#">top5</a></li>
                                        </ul>
                                    </li>

                                    <li class="menu-item menu-item-has-children has-megamenu">
                                        <a href="#" class="menu-name" data-title="Blog">Đồ Ăn Nhanh</a>
                                        <div class="wrap-megamenu lg-width-800 md-width-750">
                                            <div class="mega-content">
                                                <div class="col-lg-3 col-md-3 col-xs-6">
                                                    <div class="wrap-custom-menu vertical-menu">
                                                        <h4 class="menu-title">Đồ Ăn Nhanh</h4>
                                                        <ul class="menu">
                                                            <li><a href="#">top1</a></li>
                                                            <li><a href="#">top2</a></li>
                                                            <li><a href="#">top3</a></li>
                                                            <li><a href="#">top4</a></li>
                                                            <li><a href="#">top5</a></li>
                                                            <li><a href="#">top6</a></li>
                                                            <li><a href="#">top7</a></li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-xs-12 md-margin-top-0 xs-margin-top-25px">
                                                    <div class="block-posts">
                                                        <h4 class="menu-title">Đồ Ăn Nhanh Có Đánh Giá Cao</h4>
                                                        <ul class="posts">
                                                            <li>
                                                                <div class="block-post-item">
                                                                    <div class="thumb"><a href="#"><img src="user/assets/images/megamenu/thumb-05.jpg" width="100" height="73" alt=""></a></div>
                                                                    <div class="left-info">
                                                                        <h4 class="post-name"><a href="#">Ashwagandha: The #1 Herb in the World for Anxiety?</a></h4>
                                                                        <span class="p-date">Jan 05, 2019</span>
                                                                        <span class="p-comment">2 Comments</span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="block-post-item">
                                                                    <div class="thumb"><a href="#"><img src="user/assets/images/megamenu/thumb-06.jpg" width="100" height="73" alt=""></a></div>
                                                                    <div class="left-info">
                                                                        <h4 class="post-name"><a href="#">Ashwagandha: The #1 Herb in the World for Anxiety?</a></h4>
                                                                        <span class="p-date">May 15, 2019</span>
                                                                        <span class="p-comment">8 Comments</span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="block-post-item">
                                                                    <div class="thumb"><a href="#"><img src="user/assets/images/megamenu/thumb-07.jpg" width="100" height="73" alt=""></a></div>
                                                                    <div class="left-info">
                                                                        <h4 class="post-name"><a href="#">Ashwagandha: The #1 Herb in the World for Anxiety?</a></h4>
                                                                        <span class="p-date">Apr 26, 2019</span>
                                                                        <span class="p-comment">10 Comments</span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="menu-item"><a href="contact.html">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-md-6 col-xs-6">
                            <div class="biolife-cart-info">
                                <div class="mobile-search">
                                    <a href="javascript:void(0)" class="open-searchbox"><i class="biolife-icon icon-search"></i></a>
                                    <div class="mobile-search-content">
                                        <form action="#" class="form-search" method="get">
                                            <a href="#" class="btn-close"><span class="biolife-icon icon-close-menu"></span></a>
                                            <input type="text" name="s" class="input-text" value="" placeholder="Search here.....">
                                            <select name="category">
                                                <option value="0" selected>All Categories</option>
                                                <c:forEach items="${listCategory}" var="c">
                                                    <option value="${c.categoryID}">${c.categoryName}</option>
                                                </c:forEach>

                                            </select>
                                            <button type="submit" class="btn-submit">Search</button>
                                        </form>
                                    </div>
                                </div>

                                <div class="minicart-block">
                                    <div class="minicart-contain">
                                        <a href="cart" class="link-to">
                                            <span class="icon-qty-combine">
                                                <i class="icon-cart-mini biolife-icon"></i>
                                                <span class="qty">${cart.items.size()}</span>
                                            </span>
                                            <span class="title">My Cart -</span>
                                            <span class="sub-total">${cart.totalMoney} đ</span>
                                        </a>
                                        <div class="cart-content">
                                            <div class="cart-inner">
                                                <c:if test="${cart.items.size() == 0}">
                                                    <h3 style="text-align: center">Chưa Có Sản Phẩm Nào</h3>
                                                </c:if>

                                                <c:if test="${cart.items.size() != 0}">
                                                    <ul class="products">
                                                        <c:forEach items="${cart.items}" var="i">
                                                            <li>
                                                                <div class="minicart-item">
                                                                    <div class="thumb">
                                                                        <a href="product?id=${i.product.productID}">
                                                                            <c:if test="${i.product.image.image1 != null}">
                                                                                <img src="${i.product.image.image1}" width="90" height="90" alt="National Fresh">
                                                                            </c:if>
                                                                            <c:if test="${i.product.image.image1 == null || i.product.image.image1 == ''}">
                                                                                <img src="${i.product.image.image2}" width="90" height="90" alt="National Fresh">
                                                                            </c:if>
                                                                        </a>
                                                                    </div>
                                                                    <div class="left-info">
                                                                        <div class="product-title"><a href="product?id=${i.product.productID}" class="product-name">${i.product.productName}</a></div>
                                                                        <div class="price">
                                                                            <ins><span class="price-amount">${i.product.lastPrice}<span class="currencySymbol"> đ</span></span></ins>
                                                                            <del><span class="price-amount">${i.product.price}<span class="currencySymbol"> đ</span></del>
                                                                        </div>
                                                                        <div class="qty">
                                                                            <label for="cart[id123][qty]">Qty:</label>
                                                                            <input type="number" class="input-qty" name="cart[id123][qty]" id="cart[id123][qty]" value="${i.quantity}" disabled>
                                                                        </div>
                                                                    </div>                                                                   
                                                                </div>
                                                            </li>
                                                        </c:forEach>
                                                    </ul>
                                                </c:if>
                                                <p class="btn-control">
                                                    <a href="cart" class="btn view-cart">view cart</a>
                                                    <a href="checkout" class="btn">checkout</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mobile-menu-toggle">
                                    <a class="btn-toggle" data-object="open-mobile-menu" href="javascript:void(0)">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom hidden-sm hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-4">
                            <div class="vertical-menu vertical-category-block">
                                <div class="block-title">
                                    <span class="menu-icon">
                                        <span class="line-1"></span>
                                        <span class="line-2"></span>
                                        <span class="line-3"></span>
                                    </span>
                                    <span class="menu-title">All departments</span>
                                    <span class="angle" data-tgleclass="fa fa-caret-down"><i class="fa fa-caret-up" aria-hidden="true"></i></span>
                                </div>
                                <div class="wrap-menu">
                                    <ul class="menu clone-main-menu">
                                        <c:forEach items="${listCategory}" var="c">
                                            <c:if test="${c.categoryID == 1}">
                                                <li class="menu-item menu-item-has-children has-megamenu">
                                                    <a href="#" class="menu-name" data-title="Fruit & Nut Gifts"><i class="biolife-icon icon-fruits"></i>${c.categoryName}</a>
                                                    <div class="wrap-megamenu lg-width-900 md-width-640">
                                                        <div class="mega-content">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-8 col-sm-12 xs-margin-bottom-25 md-margin-bottom-0">
                                                                    <div class="wrap-custom-menu vertical-menu">
                                                                        <h4 class="menu-title">${c.categoryName}</h4>
                                                                        <ul class="menu">
                                                                            <c:forEach items="${fruit}" var="p">
                                                                                <li><a href="product?id=${p.productID}">${p.productName}</a></li>
                                                                                </c:forEach>
                                                                        </ul>
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-6 col-md-4 col-sm-12 lg-padding-left-50 xs-margin-bottom-25 md-margin-bottom-0">
                                                                    <div class="biolife-products-block max-width-270">
                                                                        <h4 class="menu-title">Bestseller Products</h4>
                                                                        <ul class="products-list default-product-style biolife-carousel nav-none-after-1k2 nav-center" data-slick='{"rows":1,"arrows":true,"dots":false,"infinite":false,"speed":400,"slidesMargin":30,"slidesToShow":1, "responsive":[{"breakpoint":767, "settings":{ "arrows": false}}]}' >
                                                                            <c:forEach items="${listFruitSeller}" var="p">
                                                                                <li class="product-item">
                                                                                    <div class="contain-product none-overlay">
                                                                                        <div class="product-thumb">
                                                                                            <a href="product?id=${p.productID}" class="link-to-product">
                                                                                                <img src="${p.image.image1}" alt="dd" width="270" height="270" class="product-thumnail">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="info">
                                                                                            <b class="categories">${p.category.categoryName}</b>
                                                                                            <h4 class="product-title"><a href="product?id=${p.productID}" class="pr-name">${p.productName}</a></h4>
                                                                                            <div class="price">
                                                                                                <ins><span class="price-amount">${p.lastPrice}<span class="currencySymbol"> đ</span></span></ins>
                                                                                                <del><span class="price-amount">${p.price}<span class="currencySymbol"> đ</span></span></del>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                            </c:forEach>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 md-margin-top-9">
                                                                    <div class="biolife-brand" >
                                                                        <ul class="brands">
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/brand-organic.png" width="161" height="136" alt="organic"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/brand-explore.png" width="160" height="136" alt="explore"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/brand-organic-2.png" width="99" height="136" alt="organic 2"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/brand-eco-teas.png" width="164"  height="136" alt="eco teas"></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </c:if>
                                            <c:if test="${c.categoryID == 2}">
                                                <li class="menu-item menu-item-has-children has-megamenu">
                                                    <a href="shop?cid=${c.categoryID}" class="menu-name" data-title="Vegetables"><i class="biolife-icon icon-broccoli-1"></i>${c.categoryName}</a>
                                                    <div class="wrap-megamenu lg-width-900 md-width-640 background-mega-01">
                                                        <div class="mega-content">
                                                            <div class="row">
                                                                <div class="col-lg-7 col-md-8 col-sm-12 xs-margin-bottom-25 md-margin-bottom-0">
                                                                    <div class="wrap-custom-menu vertical-menu">
                                                                        <h4 class="menu-title">Vegetables</h4>
                                                                        <ul class="menu">
                                                                            <li><a href="#">Fruit & Nut Gifts</a></li>
                                                                            <li><a href="#">Mixed Fruits</a></li>
                                                                            <li><a href="#">Oranges</a></li>
                                                                            <li><a href="#">Bananas & Plantains</a></li>
                                                                            <li><a href="#">Fresh Gala Apples</a></li>
                                                                            <li><a href="#">Berries</a></li>
                                                                            <li><a href="#">Pears</a></li>
                                                                            <li><a href="#">Produce</a></li>
                                                                            <li><a href="#">Snack Foods</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-5 col-md-4 col-sm-12 lg-padding-left-57 md-margin-bottom-30">
                                                                    <div class="biolife-brand vertical md-boder-left-30">
                                                                        <h4 class="menu-title">Hot Brand</h4>
                                                                        <ul class="brands">
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/v-brand-organic.png" width="167" height="74" alt="organic"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/v-brand-explore.png" width="167" height="72" alt="explore"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/v-brand-organic-2.png" width="167" height="99" alt="organic 2"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/v-brand-eco-teas.png" width="167" height="67" alt="eco teas"></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </c:if>

                                            <c:if test="${c.categoryID == 3}">
                                                <li class="menu-item menu-item-has-children has-megamenu">
                                                    <a href="shop?cid=${c.categoryID}" class="menu-name" data-title="Fresh Berries"><i class="biolife-icon icon-grape"></i>${c.categoryName}</a>
                                                    <div class="wrap-megamenu lg-width-900 md-width-640 background-mega-02">
                                                        <div class="mega-content">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-4 sm-col-12 md-margin-bottom-83 xs-margin-bottom-25">
                                                                    <div class="wrap-custom-menu vertical-menu">
                                                                        <h4 class="menu-title">Fresh Berries</h4>
                                                                        <ul class="menu">
                                                                            <li><a href="#">Fruit & Nut Gifts</a></li>
                                                                            <li><a href="#">Mixed Fruits</a></li>
                                                                            <li><a href="#">Oranges</a></li>
                                                                            <li><a href="#">Bananas & Plantains</a></li>
                                                                            <li><a href="#">Fresh Gala Apples</a></li>
                                                                            <li><a href="#">Berries</a></li>
                                                                            <li><a href="#">Pears</a></li>
                                                                            <li><a href="#">Produce</a></li>
                                                                            <li><a href="#">Snack Foods</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-4 sm-col-12 lg-padding-left-23 xs-margin-bottom-36px md-margin-bottom-0">
                                                                    <div class="wrap-custom-menu vertical-menu">
                                                                        <h4 class="menu-title">Gifts</h4>
                                                                        <ul class="menu">
                                                                            <li><a href="#">Non-Dairy Coffee Creamers</a></li>
                                                                            <li><a href="#">Coffee Creamers</a></li>
                                                                            <li><a href="#">Mayonnaise</a></li>
                                                                            <li><a href="#">Almond Milk</a></li>
                                                                            <li><a href="#">Ghee</a></li>
                                                                            <li><a href="#">Beverages</a></li>
                                                                            <li><a href="#">Ranch Salad Dressings</a></li>
                                                                            <li><a href="#">Hemp Milk</a></li>
                                                                            <li><a href="#">Nuts & Seeds</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-4 sm-col-12 lg-padding-left-25 md-padding-top-55">
                                                                    <div class="biolife-banner layout-01">
                                                                        <h3 class="top-title">Farm Fresh</h3>
                                                                        <p class="content"> All the Lorem Ipsum generators on the Internet tend.</p>
                                                                        <b class="bottomm-title">Berries Series</b>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </c:if>

                                            <c:if test="${c.categoryID != 1 && c.categoryID != 2 && c.categoryID != 3}">
                                                <li class="menu-item"><a href="shop?cid=${c.categoryID}" class="menu-name" data-title="">
                                                        <i class="biolife-icon ${c.categoryName eq 'Fresh Meat' ? 'icon-beef' : ''}
                                                           ${c.categoryName eq 'Ocean Foods' ? 'icon-fish' : ''}
                                                           ${c.categoryName eq 'Fresh Onion' ? 'icon-onions' : ''}
                                                           ${c.categoryName eq 'Spice' ? 'icon-contain' : ''}"></i>${c.categoryName}</a></li>
                                                    </c:if>   
                                                </c:forEach>


                                        <li class="menu-item"><a href="#" class="menu-name" data-title="Butter & Eggs"><i class="biolife-icon icon-honey"></i>Butter & Eggs</a></li>
                                        <li class="menu-item"><a href="#" class="menu-title"><i class="biolife-icon icon-fast-food"></i>Fastfood</a></li>
                                        <li class="menu-item"><a href="#" class="menu-title"><i class="biolife-icon icon-avocado"></i>Papaya & Crisps</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-8 padding-top-2px">
                            <div class="header-search-bar layout-01">
                                <form action="shop"  method="get">
                                    <input type="text" name="search" class="input-text" placeholder="Tìm Kiếm Sản Phẩm ở Đây">
                                    <select name="cid">
                                        <option value="0" selected>All Categories</option>
                                        <c:forEach items="${listCategory}" var="c">
                                            <option value="${c.categoryID}">${c.categoryName}</option>
                                        </c:forEach>


                                    </select>
                                    <button type="submit" class="btn-submit"><i class="biolife-icon icon-search"></i></button>
                                </form>
                            </div>
                            <div class="live-info">
                                <p class="telephone"><i class="fa fa-phone" aria-hidden="true"></i><b class="phone-number">0838456798</b></p>
                                <p class="working-time">Mon-Fri: 8:30am-7:30pm; Sat-Sun: 9:30am-4:30pm</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!--Hero Section-->
        <div class="hero-section hero-background">
            <h1 class="page-title">Organic Fruits</h1>
        </div>

        <!--Navigation section-->
        <div class="container">
            <nav class="biolife-nav">
                <ul>
                    <li class="nav-item"><a href="index-2.html" class="permal-link">Home</a></li>
                    <!--<li class="nav-item"><a href="#" class="permal-link">Natural Organic</a></li>-->
                    <li class="nav-item"><span class="current-page">Fresh Fruit</span></li>
                </ul>
            </nav>
        </div>

        <div class="page-contain category-page left-sidebar">
            <div class="container">
                <div class="row">
                    <!-- Main content -->
                    <div id="main-content" class="main-content col-lg-9 col-md-8 col-sm-12 col-xs-12">

                        <div class="block-item recently-products-cat md-margin-bottom-39">
                            <ul class="products-list biolife-carousel nav-center-02 nav-none-on-mobile" data-slick='{"rows":1,"arrows":true,"dots":false,"infinite":false,"speed":400,"slidesMargin":0,"slidesToShow":5, "responsive":[{"breakpoint":1200, "settings":{ "slidesToShow": 3}},{"breakpoint":992, "settings":{ "slidesToShow": 3, "slidesMargin":30}},{"breakpoint":768, "settings":{ "slidesToShow": 2, "slidesMargin":10}}]}' >
                                <c:forEach items="${listProduct}" var="p">
                                    <li class="product-item">
                                        <div class="contain-product layout-02">
                                            <div class="product-thumb">
                                                <a href="product?id=${p.productID}" class="link-to-product">
                                                    <img src="${p.image.image1}" alt="dd" width="270" height="270" class="product-thumnail">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <b class="categories">${p.category.categoryName}</b>
                                                <h4 class="product-title" style="min-height: 37.4px"><a href="product?id=${p.productID}" class="pr-name">${p.productName}</a></h4>
                                                <div class="price">
                                                    <ins><span class="price-amount">${p.lastPrice}</span></ins>
                                                    <del><span class="price-amount">${p.price}</span></del>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>

                        <div class="product-category grid-style">

                            <div id="top-functions-area" class="top-functions-area" >
                                <div class="flt-item to-left group-on-mobile">
                                    <span class="flt-title">Refine</span>
                                    <a href="#" class="icon-for-mobile">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </a>
                                    <div class="wrap-selectors">
                                        <form action="#" name="frm-refine" method="get">
                                            <span class="title-for-mobile">Refine Products By</span>
                                            <div data-title="Price:" class="selector-item">
                                                <select name="price" class="selector">
                                                    <option value="all">Price</option>
                                                    <option value="class-1st">Less than 5$</option>
                                                    <option value="class-2nd">$5-10$</option>
                                                    <option value="class-3rd">$10-20$</option>
                                                    <option value="class-4th">$20-45$</option>
                                                    <option value="class-5th">$45-100$</option>
                                                    <option value="class-6th">$100-150$</option>
                                                    <option value="class-7th">More than 150$</option>
                                                </select>
                                            </div>
                                            <div data-title="Brand:" class="selector-item">
                                                <select name="brad" class="selector">
                                                    <option value="all">Top brands</option>
                                                    <option value="br2">Brand first</option>
                                                    <option value="br3">Brand second</option>
                                                    <option value="br4">Brand third</option>
                                                    <option value="br5">Brand fourth</option>
                                                    <option value="br6">Brand fiveth</option>
                                                </select>
                                            </div>
                                            <div data-title="Avalability:" class="selector-item">
                                                <select name="ability" class="selector">
                                                    <option value="all">Availability</option>
                                                    <option value="vl2">Availability 1</option>
                                                    <option value="vl3">Availability 2</option>
                                                    <option value="vl4">Availability 3</option>
                                                    <option value="vl5">Availability 4</option>
                                                    <option value="vl6">Availability 5</option>
                                                </select>
                                            </div>
                                            <p class="btn-for-mobile"><button type="submit" class="btn-submit">Go</button></p>
                                        </form>
                                    </div>
                                </div>
                                <div class="flt-item to-right">
                                    <span class="flt-title">Sort</span>
                                    <div class="wrap-selectors">
                                        <div class="selector-item orderby-selector">
                                            <select name="orderby" class="orderby" aria-label="Shop order">
                                                <option value="menu_order" selected="selected">Default sorting</option>
                                                <option value="popularity">popularity</option>
                                                <option value="rating">average rating</option>
                                                <option value="date">newness</option>
                                                <option value="price">price: low to high</option>
                                                <option value="price-desc">price: high to low</option>
                                            </select>
                                        </div>
                                        <div class="selector-item viewmode-selector">
                                            <a href="category-grid-left-sidebar.html" class="viewmode grid-mode active"><i class="biolife-icon icon-grid"></i></a>
                                            <a href="category-list-left-sidebar.html" class="viewmode detail-mode"><i class="biolife-icon icon-list"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <ul class="products-list">

                                    <c:forEach begin="0" end="${listByPage.size() - 1}" var="i">
                                        <li class="product-item col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                            <form action="" method="post">
                                                <div class="contain-product layout-default">
                                                    <div class="product-thumb">
                                                        <a href="product?id=${listByPage.get(i).productID}" class="link-to-product">
                                                            <img src="${listByPage.get(i).image.image2}" alt="dd" width="270" height="270" class="product-thumnail">
                                                        </a>
                                                    </div>
                                                    <div class="info">
                                                        <b class="categories">${listByPage.get(i).category.categoryName}</b>
                                                        <h4 class="product-title" style="min-height: 56.1px;"><a href="product?id=${listByPage.get(i).productID}" class="pr-name">${listByPage.get(i).productName}</a></h4>
                                                        <div class="price">
                                                            <ins><span class="price-amount">${listByPage.get(i).lastPrice}<span class="currencySymbol"> đ</span></span></ins>
                                                            <del><span class="price-amount">${listByPage.get(i).price}<span class="currencySymbol"> đ</span></span></del>
                                                        </div>
                                                        <div class="shipping-info">
                                                            <p class="shipping-day">3-Day Shipping</p>
                                                            <p class="for-today">Pree Pickup Today</p>
                                                        </div>
                                                        <div class="slide-down-box">
                                                            <p class="message">${listByPage.get(i).describe}</p>
                                                            <div class="buttons">

                                                                <input type="submit" class="btn add-to-cart-btn" value="Thêm Vào Giỏ Hàng">
                                                                <input type="text" name="cid" value="${cid}" hidden>
                                                                <input type="text" name="id" value="${listByPage.get(i).productID}" hidden>
                                                                <input type="text" name="quantity" value="1" hidden>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </div>

                            <div class="biolife-panigations-block">

                                <ul class="panigation-contain">
                                    <li class="page-item ${page == 1 ? 'hidden' : ''}" >
                                        <a class="link-page"  href="shop?page=${page-1}&cid=${cid}">Previous</a></li>
                                        <c:forEach begin="1" end="${numberPage}" var="i">
                                            <c:if test="${page == i}">
                                            <li class="page-item"><a class="page-link" href="shop?page=${i}&cid=${cid}"><span class="current-page">${i}</span></a></li>
                                                </c:if>
                                                <c:if test="${page != i}">
                                            <li class="page-item"><a class="page-link" href="shop?page=${i}&cid=${cid}">${i}</a></li>
                                            </c:if>
                                        </c:forEach>
                                    <li class="page-item ${page == numberPage ? 'hidden' : ''}"><a class="page-link" href="shop?page=${page+1}&cid=${cid}">Next</a></li>

                                    <!--                                    <li><span class="current-page">1</span></li>
                                                                        <li><a href="#" class="link-page">2</a></li>
                                                                        <li><a href="#" class="link-page">3</a></li>
                                                                        <li><span class="sep">....</span></li>
                                                                        <li><a href="#" class="link-page">20</a></li>
                                                                        <li><a href="#" class="link-page next"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>-->
                                </ul>
                            </div>

                        </div>

                    </div>
                    <!-- Sidebar -->
                    <aside id="sidebar" class="sidebar col-lg-3 col-md-4 col-sm-12 col-xs-12">
                        <div class="biolife-mobile-panels">
                            <span class="biolife-current-panel-title">Sidebar</span>
                            <a class="biolife-close-btn" href="#" data-object="open-mobile-filter">&times;</a>
                        </div>
                        <div class="sidebar-contain">
                            <div class="widget biolife-filter">
                                <h4 class="wgt-title">Category</h4>
                                <div class="wgt-content">
                                    <ul class="cat-list">
                                        <c:forEach items="${listCategory}" var="c">
                                            <li class="cat-list-item"><a href="shop?cid=${c.categoryID}" class="cat-link">${c.categoryName}</a></li>
                                            </c:forEach>

                                    </ul>
                                </div>
                            </div>

                            <div class="widget price-filter biolife-filter">
                                <h4 class="wgt-title">Price</h4>
                                <div class="wgt-content">
                                    <div class="frm-contain">
                                        <form action="" name="price-filter" id="price-filter" method="post">
                                            <p class="f-item">

                                                <input class="input-number" type="number" id="pr-from" value="0" name="priceFrom">
                                            </p>
                                            <p class="f-item">
                                                <label for="pr-to"> To </label>
                                                <input class="input-number" type="number" id="pr-to" value="" name="priceTo">
                                            </p>
                                            <p class="f-item"><button class="btn-submit" type="submit">GO</button></p>
                                        </form>
                                    </div>

                                </div>
                            </div>

                            <div class="widget biolife-filter">
                                <h4 class="wgt-title">Sản Phẩm Bán Chạy</h4>
                                <div class="wgt-content">
                                    <ul class="products">
                                        <c:forEach items="${listProductSeller}" var="p">
                                            <li class="pr-item">
                                                <div class="contain-product style-widget">
                                                    <div class="product-thumb">
                                                        <a href="#" class="link-to-product" tabindex="0">
                                                            <img src="${p.image.image1}" alt="dd" width="270" height="270" class="product-thumnail">
                                                        </a>
                                                    </div>
                                                    <div class="info">
                                                        <b class="categories">${p.category.categoryName}</b>
                                                        <h4 class="product-title"><a href="#" class="pr-name" tabindex="0">${p.productName}</a></h4>
                                                        <div class="price">
                                                            <ins><span class="price-amount">${p.lastPrice}</span></ins>
                                                            <del><span class="price-amount">${p.price}</span></del>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </div>
                            </div>

                            <div class="widget biolife-filter">
                                <h4 class="wgt-title">Product Tags</h4>
                                <div class="wgt-content">
                                    <ul class="tag-cloud">
                                        <li class="tag-item"><a href="#" class="tag-link">Fresh Fruit</a></li>
                                        <li class="tag-item"><a href="#" class="tag-link">Natural Food</a></li>
                                        <li class="tag-item"><a href="#" class="tag-link">Hot</a></li>
                                        <li class="tag-item"><a href="#" class="tag-link">Organics</a></li>
                                        <li class="tag-item"><a href="#" class="tag-link">Dried Organic</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </aside>
                </div>
            </div>
        </div>

        <!-- FOOTER -->
        <footer id="footer" class="footer layout-03">
            <div class="footer-content background-footer-03">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-9">
                            <section class="footer-item">
                                <a href="home" class="logo footer-logo"><img src="user/assets/images/organic-3.png" alt="biolife logo" width="135" height="34"></a>
                                <div class="footer-phone-info">
                                    <i class="biolife-icon icon-head-phone"></i>
                                    <p class="r-info">
                                        <span>Need Help ?</span>
                                        <span>(84)  0838456798</span>
                                    </p>
                                </div>
                                <div class="newsletter-block layout-01">
                                    <h4 class="title">Newsletter Signup</h4>
                                    <div class="form-content">
                                        <form action="#" name="new-letter-foter">
                                            <input type="email" class="input-text email" value="" placeholder="Your email here...">
                                            <button type="submit" class="bnt-submit" name="ok">Sign up</button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 md-margin-top-5px sm-margin-top-50px xs-margin-top-40px">
                            <section class="footer-item">
                                <h3 class="section-title">Useful Links</h3>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6 col-xs-6">
                                        <div class="wrap-custom-menu vertical-menu-2">
                                            <ul class="menu">
                                                <li><a href="#">About Us</a></li>
                                                <li><a href="#">About Our Shop</a></li>
                                                <li><a href="#">Secure Shopping</a></li>
                                                <li><a href="#">Delivery infomation</a></li>
                                                <li><a href="#">Privacy Policy</a></li>
                                                <li><a href="#">Our Sitemap</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-6 col-xs-6">
                                        <div class="wrap-custom-menu vertical-menu-2">
                                            <ul class="menu">
                                                <li><a href="#">Who We Are</a></li>
                                                <li><a href="#">Our Services</a></li>
                                                <li><a href="#">Projects</a></li>
                                                <li><a href="#">Contacts Us</a></li>
                                                <li><a href="#">Innovation</a></li>
                                                <li><a href="#">Testimonials</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 md-margin-top-5px sm-margin-top-50px xs-margin-top-40px">
                            <section class="footer-item">
                                <h3 class="section-title">Contact</h3>
                                <div class="contact-info-block footer-layout xs-padding-top-10px">
                                    <ul class="contact-lines">
                                        <li>
                                            <p class="info-item">
                                                <i class="biolife-icon icon-location"></i>
                                                <b class="desc">Đại Học FPT Hà Nội, Hòa Lạc</b>
                                            </p>
                                        </li>
                                        <li>
                                            <p class="info-item">
                                                <i class="biolife-icon icon-phone"></i>
                                                <b class="desc">Phone: (+084) 0838456798</b>
                                            </p>
                                        </li>
                                        <li>
                                            <p class="info-item">
                                                <i class="biolife-icon icon-letter"></i>
                                                <b class="desc">Email:  phanhieu000lc@gmail.com</b>
                                            </p>
                                        </li>
                                        <li>
                                            <p class="info-item">
                                                <i class="biolife-icon icon-clock"></i>
                                                <b class="desc">Hours: 10:00 AM - 20:00 PM</b>
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="biolife-social inline">
                                    <ul class="socials">
                                        <li><a href="#" title="twitter" class="socail-btn"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#" title="facebook" class="socail-btn"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#" title="pinterest" class="socail-btn"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                        <li><a href="#" title="youtube" class="socail-btn"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                        <li><a href="#" title="instagram" class="socail-btn"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="separator sm-margin-top-70px xs-margin-top-40px"></div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">

                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="payment-methods">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <!--Footer For Mobile-->
        <div class="mobile-footer">
            <div class="mobile-footer-inner">
                <div class="mobile-block block-menu-main">
                    <a class="menu-bar menu-toggle btn-toggle" data-object="open-mobile-menu" href="javascript:void(0)">
                        <span class="fa fa-bars"></span>
                        <span class="text">Menu</span>
                    </a>
                </div>
                <div class="mobile-block block-sidebar">
                    <a class="menu-bar filter-toggle btn-toggle" data-object="open-mobile-filter" href="javascript:void(0)">
                        <i class="fa fa-sliders" aria-hidden="true"></i>
                        <span class="text">Sidebar</span>
                    </a>
                </div>
                <div class="mobile-block block-minicart">
                    <a class="link-to-cart" href="#">
                        <span class="fa fa-shopping-bag" aria-hidden="true"></span>
                        <span class="text">Cart</span>
                    </a>
                </div>
                <div class="mobile-block block-global">
                    <a class="menu-bar myaccount-toggle btn-toggle" data-object="global-panel-opened" href="javascript:void(0)">
                        <span class="fa fa-globe"></span>
                        <span class="text">More</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="mobile-block-global">
            <div class="biolife-mobile-panels">
                <span class="biolife-current-panel-title">More</span>
                <a class="biolife-close-btn" data-object="global-panel-opened" href="#">&times;</a>
            </div>
            <div class="block-global-contain">
                <div class="glb-item my-account">
                    <b class="title">My Account</b>
                    <ul class="list">
                        <li class="list-item"><a href="login">Login/register</a></li>
                        <li class="list-item"><a href="#">Wishlist <span class="index">(8)</span></a></li>
                        <li class="list-item"><a href="checkout">Checkout</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Scroll Top Button -->
        <a class="btn-scroll-top"><i class="biolife-icon icon-left-arrow"></i></a>

        <script src="user/assets/js/jquery-3.4.1.min.js"></script>
        <script src="user/assets/js/bootstrap.min.js"></script>
        <script src="user/assets/js/jquery.countdown.min.js"></script>
        <script src="user/assets/js/jquery.nice-select.min.js"></script>
        <script src="user/assets/js/jquery.nicescroll.min.js"></script>
        <script src="user/assets/js/slick.min.js"></script>
        <script src="user/assets/js/biolife.framework.js"></script>
        <script src="user/assets/js/functions.js"></script>
    </body>

</html>
