<%-- 
    Document   : product
    Created on : 18-10-2022, 15:44:37
    Author     : phanh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html >

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Cairo:400,600,700&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:600&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400i,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu&amp;display=swap" rel="stylesheet">
        <link rel="shortcut icon" type="image/x-icon" href="user/assets/images/favicon.png" />
        <link rel="stylesheet" href="user/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="user/assets/css/animate.min.css">
        <link rel="stylesheet" href="user/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="user/assets/css/nice-select.css">
        <link rel="stylesheet" href="user/assets/css/slick.min.css">
        <link rel="stylesheet" href="user/assets/css/style.css">
        <link rel="stylesheet" href="user/assets/css/main-color.css">
        <style>
            * {
                font-family: initial;
            }
        </style>
        <title>Candles Store</title>
    </head>
    <body class="biolife-body">

        <!-- Preloader -->
        <div id="biof-loading">
            <div class="biof-loading-center">
                <div class="biof-loading-center-absolute">
                    <div class="dot dot-one"></div>
                    <div class="dot dot-two"></div>
                    <div class="dot dot-three"></div>
                </div>
            </div>
        </div>

        <!-- HEADER -->
        <header id="header" class="header-area style-01 layout-03">
            <div class="header-top bg-main hidden-xs">
                <div class="container-fluid">
                    <div class="top-bar left">
                        <ul class="horizontal-menu">
                            <li><a href="#"><i class="fa fa-truck" aria-hidden="true"></i>Miễn Phí Ship Với Đơn Hàng Từ 399k</a></li>
                            <li><a href="#"></a></li>
                        </ul> 
                    </div>
                    <div class="top-bar right">
                        <ul class="horizontal-menu">
                            <li>
                                <c:if test="${account == null}">
                                    <a href="login" class="login-link">
                                        <i class="biolife-icon icon-login"></i>Login/Register</a>
                                    </c:if>

                                <c:if test="${account != null}">
                                    <a href="profiles" class="login-link">
                                        <i class="biolife-icon icon-login"></i>Hello: ${account.fullName}</a>
                                    <a href="logout" class="login-link" style="margin-left: 1rem">
                                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                                        Logout</a>
                                    </c:if>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="header-middle biolife-sticky-object ">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-2 col-md-6 col-xs-6">
                            <a href="home" class="biolife-logo">
                                <img src="user/assets/images/organic-3.png" alt="biolife logo" width="135" height="34">
                            </a>
                        </div>
                        <div class="col-lg-6 col-md-7 hidden-sm hidden-xs">
                            <div class="primary-menu">
                                <ul class="menu biolife-menu clone-main-menu clone-primary-menu" id="primary-menu" data-menuname="main menu">
                                    <li class="menu-item"><a href="home">Home</a></li>
                                    <li class="menu-item"> <a href="shop">Shop</a></li>
                                    <li class="menu-item menu-item-has-children has-child">
                                        <a href="#" class="menu-name" data-title="Products">Nổi Bật</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item"><a href="#">top1</a></li>
                                            <li class="menu-item"><a href="#">top2</a></li>
                                            <li class="menu-item menu-item-has-children has-child"><a href="#" class="menu-name" data-title="Eggs & other considerations">top3</a>
                                                <ul class="sub-menu">
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item"><a href="#">top4</a></li>
                                            <li class="menu-item"><a href="#">top5</a></li>
                                        </ul>
                                    </li>

                                    <li class="menu-item menu-item-has-children has-megamenu">
                                        <a href="#" class="menu-name" data-title="Blog">Đồ Ăn Nhanh</a>
                                        <div class="wrap-megamenu lg-width-800 md-width-750">
                                            <div class="mega-content">
                                                <div class="col-lg-3 col-md-3 col-xs-6">
                                                    <div class="wrap-custom-menu vertical-menu">
                                                        <h4 class="menu-title">Đồ Ăn Nhanh</h4>
                                                        <ul class="menu">
                                                            <li><a href="#">top1</a></li>
                                                            <li><a href="#">top2</a></li>
                                                            <li><a href="#">top3</a></li>
                                                            <li><a href="#">top4</a></li>
                                                            <li><a href="#">top5</a></li>
                                                            <li><a href="#">top6</a></li>
                                                            <li><a href="#">top7</a></li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-xs-12 md-margin-top-0 xs-margin-top-25px">
                                                    <div class="block-posts">
                                                        <h4 class="menu-title">Đồ Ăn Nhanh Có Đánh Giá Cao</h4>
                                                        <ul class="posts">
                                                            <li>
                                                                <div class="block-post-item">
                                                                    <div class="thumb"><a href="#"><img src="user/assets/images/megamenu/thumb-05.jpg" width="100" height="73" alt=""></a></div>
                                                                    <div class="left-info">
                                                                        <h4 class="post-name"><a href="#">Ashwagandha: The #1 Herb in the World for Anxiety?</a></h4>
                                                                        <span class="p-date">Jan 05, 2019</span>
                                                                        <span class="p-comment">2 Comments</span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="block-post-item">
                                                                    <div class="thumb"><a href="#"><img src="user/assets/images/megamenu/thumb-06.jpg" width="100" height="73" alt=""></a></div>
                                                                    <div class="left-info">
                                                                        <h4 class="post-name"><a href="#">Ashwagandha: The #1 Herb in the World for Anxiety?</a></h4>
                                                                        <span class="p-date">May 15, 2019</span>
                                                                        <span class="p-comment">8 Comments</span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="block-post-item">
                                                                    <div class="thumb"><a href="#"><img src="user/assets/images/megamenu/thumb-07.jpg" width="100" height="73" alt=""></a></div>
                                                                    <div class="left-info">
                                                                        <h4 class="post-name"><a href="#">Ashwagandha: The #1 Herb in the World for Anxiety?</a></h4>
                                                                        <span class="p-date">Apr 26, 2019</span>
                                                                        <span class="p-comment">10 Comments</span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="menu-item"><a href="contact.html">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-md-6 col-xs-6">
                            <div class="biolife-cart-info">
                                <div class="mobile-search">
                                    <a href="javascript:void(0)" class="open-searchbox"><i class="biolife-icon icon-search"></i></a>
                                    <div class="mobile-search-content">
                                        <form action="#" class="form-search" method="get">
                                            <a href="#" class="btn-close"><span class="biolife-icon icon-close-menu"></span></a>
                                            <input type="text" name="s" class="input-text" value="" placeholder="Search here.....">
                                            <select name="category">
                                                <option value="0" selected>All Categories</option>
                                                <c:forEach items="${listCategory}" var="c">
                                                    <option value="${c.categoryID}">${c.categoryName}</option>
                                                </c:forEach>

                                            </select>
                                            <button type="submit" class="btn-submit">Search</button>
                                        </form>
                                    </div>
                                </div>

                                <div class="minicart-block">
                                    <div class="minicart-contain">
                                        <a href="cart" class="link-to">
                                            <span class="icon-qty-combine">
                                                <i class="icon-cart-mini biolife-icon"></i>
                                                <span class="qty">${cart.items.size()}</span>
                                            </span>
                                            <span class="title">My Cart -</span>
                                            <span class="sub-total">${cart.totalMoney} đ</span>
                                        </a>
                                        <div class="cart-content">
                                            <div class="cart-inner">
                                                <c:if test="${cart.items.size() == 0}">
                                                    <h3 style="text-align: center">Chưa Có Sản Phẩm Nào</h3>
                                                </c:if>

                                                <c:if test="${cart.items.size() != 0}">
                                                    <ul class="products">
                                                        <c:forEach items="${cart.items}" var="i">
                                                            <li>
                                                                <div class="minicart-item">
                                                                    <div class="thumb">
                                                                        <a href="product?id=${i.product.productID}">
                                                                            <c:if test="${i.product.image.image1 != null}">
                                                                                <img src="${i.product.image.image1}" width="90" height="90" alt="National Fresh">
                                                                            </c:if>
                                                                            <c:if test="${i.product.image.image1 == null || i.product.image.image1 == ''}">
                                                                                <img src="${i.product.image.image2}" width="90" height="90" alt="National Fresh">
                                                                            </c:if>
                                                                        </a>
                                                                    </div>
                                                                    <div class="left-info">
                                                                        <div class="product-title"><a href="product?id=${i.product.productID}" class="product-name">${i.product.productName}</a></div>
                                                                        <div class="price">
                                                                            <ins><span class="price-amount">${i.product.lastPrice}<span class="currencySymbol"> đ</span></span></ins>
                                                                            <del><span class="price-amount">${i.product.price}<span class="currencySymbol"> đ</span></del>
                                                                        </div>
                                                                        <div class="qty">
                                                                            <label for="cart[id123][qty]">Qty:</label>
                                                                            <input type="number" class="input-qty" name="cart[id123][qty]" id="cart[id123][qty]" value="${i.quantity}" disabled>
                                                                        </div>
                                                                    </div>                                                                   
                                                                </div>
                                                            </li>
                                                        </c:forEach>
                                                    </ul>
                                                </c:if>
                                                <p class="btn-control">
                                                    <a href="cart" class="btn view-cart">view cart</a>
                                                    <a href="checkout" class="btn">checkout</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mobile-menu-toggle">
                                    <a class="btn-toggle" data-object="open-mobile-menu" href="javascript:void(0)">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom hidden-sm hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-4">
                            <div class="vertical-menu vertical-category-block">
                                <div class="block-title">
                                    <span class="menu-icon">
                                        <span class="line-1"></span>
                                        <span class="line-2"></span>
                                        <span class="line-3"></span>
                                    </span>
                                    <span class="menu-title">All departments</span>
                                    <span class="angle" data-tgleclass="fa fa-caret-down"><i class="fa fa-caret-up" aria-hidden="true"></i></span>
                                </div>
                                <div class="wrap-menu">
                                    <ul class="menu clone-main-menu">
                                        <c:forEach items="${listCategory}" var="c">
                                            <c:if test="${c.categoryID == 1}">
                                                <li class="menu-item menu-item-has-children has-megamenu">
                                                    <a href="#" class="menu-name" data-title="Fruit & Nut Gifts"><i class="biolife-icon icon-fruits"></i>${c.categoryName}</a>
                                                    <div class="wrap-megamenu lg-width-900 md-width-640">
                                                        <div class="mega-content">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-8 col-sm-12 xs-margin-bottom-25 md-margin-bottom-0">
                                                                    <div class="wrap-custom-menu vertical-menu">
                                                                        <h4 class="menu-title">${c.categoryName}</h4>
                                                                        <ul class="menu">
                                                                            <c:forEach items="${fruit}" var="p">
                                                                                <li><a href="product?id=${p.productID}">${p.productName}</a></li>
                                                                                </c:forEach>
                                                                        </ul>
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-6 col-md-4 col-sm-12 lg-padding-left-50 xs-margin-bottom-25 md-margin-bottom-0">
                                                                    <div class="biolife-products-block max-width-270">
                                                                        <h4 class="menu-title">Bestseller Products</h4>
                                                                        <ul class="products-list default-product-style biolife-carousel nav-none-after-1k2 nav-center" data-slick='{"rows":1,"arrows":true,"dots":false,"infinite":false,"speed":400,"slidesMargin":30,"slidesToShow":1, "responsive":[{"breakpoint":767, "settings":{ "arrows": false}}]}' >
                                                                            <c:forEach items="${listFruitSeller}" var="p">
                                                                                <li class="product-item">
                                                                                    <div class="contain-product none-overlay">
                                                                                        <div class="product-thumb">
                                                                                            <a href="product?id=${p.productID}" class="link-to-product">
                                                                                                <img src="${p.image.image1}" alt="dd" width="270" height="270" class="product-thumnail">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="info">
                                                                                            <b class="categories">${p.category.categoryName}</b>
                                                                                            <h4 class="product-title"><a href="product?id=${p.productID}" class="pr-name">${p.productName}</a></h4>
                                                                                            <div class="price">
                                                                                                <ins><span class="price-amount">${p.lastPrice}<span class="currencySymbol"> đ</span></span></ins>
                                                                                                <del><span class="price-amount">${p.price}<span class="currencySymbol"> đ</span></span></del>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                            </c:forEach>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 md-margin-top-9">
                                                                    <div class="biolife-brand" >
                                                                        <ul class="brands">
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/brand-organic.png" width="161" height="136" alt="organic"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/brand-explore.png" width="160" height="136" alt="explore"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/brand-organic-2.png" width="99" height="136" alt="organic 2"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/brand-eco-teas.png" width="164"  height="136" alt="eco teas"></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </c:if>
                                            <c:if test="${c.categoryID == 2}">
                                                <li class="menu-item menu-item-has-children has-megamenu">
                                                    <a href="shop?cid=${c.categoryID}" class="menu-name" data-title="Vegetables"><i class="biolife-icon icon-broccoli-1"></i>${c.categoryName}</a>
                                                    <div class="wrap-megamenu lg-width-900 md-width-640 background-mega-01">
                                                        <div class="mega-content">
                                                            <div class="row">
                                                                <div class="col-lg-7 col-md-8 col-sm-12 xs-margin-bottom-25 md-margin-bottom-0">
                                                                    <div class="wrap-custom-menu vertical-menu">
                                                                        <h4 class="menu-title">Vegetables</h4>
                                                                        <ul class="menu">
                                                                            <li><a href="#">Fruit & Nut Gifts</a></li>
                                                                            <li><a href="#">Mixed Fruits</a></li>
                                                                            <li><a href="#">Oranges</a></li>
                                                                            <li><a href="#">Bananas & Plantains</a></li>
                                                                            <li><a href="#">Fresh Gala Apples</a></li>
                                                                            <li><a href="#">Berries</a></li>
                                                                            <li><a href="#">Pears</a></li>
                                                                            <li><a href="#">Produce</a></li>
                                                                            <li><a href="#">Snack Foods</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-5 col-md-4 col-sm-12 lg-padding-left-57 md-margin-bottom-30">
                                                                    <div class="biolife-brand vertical md-boder-left-30">
                                                                        <h4 class="menu-title">Hot Brand</h4>
                                                                        <ul class="brands">
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/v-brand-organic.png" width="167" height="74" alt="organic"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/v-brand-explore.png" width="167" height="72" alt="explore"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/v-brand-organic-2.png" width="167" height="99" alt="organic 2"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/v-brand-eco-teas.png" width="167" height="67" alt="eco teas"></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </c:if>

                                            <c:if test="${c.categoryID == 3}">
                                                <li class="menu-item menu-item-has-children has-megamenu">
                                                    <a href="shop?cid=${c.categoryID}" class="menu-name" data-title="Fresh Berries"><i class="biolife-icon icon-grape"></i>${c.categoryName}</a>
                                                    <div class="wrap-megamenu lg-width-900 md-width-640 background-mega-02">
                                                        <div class="mega-content">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-4 sm-col-12 md-margin-bottom-83 xs-margin-bottom-25">
                                                                    <div class="wrap-custom-menu vertical-menu">
                                                                        <h4 class="menu-title">Fresh Berries</h4>
                                                                        <ul class="menu">
                                                                            <li><a href="#">Fruit & Nut Gifts</a></li>
                                                                            <li><a href="#">Mixed Fruits</a></li>
                                                                            <li><a href="#">Oranges</a></li>
                                                                            <li><a href="#">Bananas & Plantains</a></li>
                                                                            <li><a href="#">Fresh Gala Apples</a></li>
                                                                            <li><a href="#">Berries</a></li>
                                                                            <li><a href="#">Pears</a></li>
                                                                            <li><a href="#">Produce</a></li>
                                                                            <li><a href="#">Snack Foods</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-4 sm-col-12 lg-padding-left-23 xs-margin-bottom-36px md-margin-bottom-0">
                                                                    <div class="wrap-custom-menu vertical-menu">
                                                                        <h4 class="menu-title">Gifts</h4>
                                                                        <ul class="menu">
                                                                            <li><a href="#">Non-Dairy Coffee Creamers</a></li>
                                                                            <li><a href="#">Coffee Creamers</a></li>
                                                                            <li><a href="#">Mayonnaise</a></li>
                                                                            <li><a href="#">Almond Milk</a></li>
                                                                            <li><a href="#">Ghee</a></li>
                                                                            <li><a href="#">Beverages</a></li>
                                                                            <li><a href="#">Ranch Salad Dressings</a></li>
                                                                            <li><a href="#">Hemp Milk</a></li>
                                                                            <li><a href="#">Nuts & Seeds</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-4 sm-col-12 lg-padding-left-25 md-padding-top-55">
                                                                    <div class="biolife-banner layout-01">
                                                                        <h3 class="top-title">Farm Fresh</h3>
                                                                        <p class="content"> All the Lorem Ipsum generators on the Internet tend.</p>
                                                                        <b class="bottomm-title">Berries Series</b>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </c:if>

                                            <c:if test="${c.categoryID != 1 && c.categoryID != 2 && c.categoryID != 3}">
                                                <li class="menu-item"><a href="shop?cid=${c.categoryID}" class="menu-name" data-title="">
                                                        <i class="biolife-icon ${c.categoryName eq 'Fresh Meat' ? 'icon-beef' : ''}
                                                           ${c.categoryName eq 'Ocean Foods' ? 'icon-fish' : ''}
                                                           ${c.categoryName eq 'Fresh Onion' ? 'icon-onions' : ''}
                                                           ${c.categoryName eq 'Spice' ? 'icon-contain' : ''}"></i>${c.categoryName}</a></li>
                                                    </c:if>   
                                                </c:forEach>


                                        <li class="menu-item"><a href="#" class="menu-name" data-title="Butter & Eggs"><i class="biolife-icon icon-honey"></i>Butter & Eggs</a></li>
                                        <li class="menu-item"><a href="#" class="menu-title"><i class="biolife-icon icon-fast-food"></i>Fastfood</a></li>
                                        <li class="menu-item"><a href="#" class="menu-title"><i class="biolife-icon icon-avocado"></i>Papaya & Crisps</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-8 padding-top-2px">
                            <div class="header-search-bar layout-01">
                                <form action="shop"  method="get">
                                    <input type="text" name="search" class="input-text" placeholder="Tìm Kiếm Sản Phẩm ở Đây">
                                    <select name="cid">
                                        <option value="0" selected>All Categories</option>
                                        <c:forEach items="${listCategory}" var="c">
                                            <option value="${c.categoryID}">${c.categoryName}</option>
                                        </c:forEach>


                                    </select>
                                    <button type="submit" class="btn-submit"><i class="biolife-icon icon-search"></i></button>
                                </form>
                            </div>
                            <div class="live-info">
                                <p class="telephone"><i class="fa fa-phone" aria-hidden="true"></i><b class="phone-number">0838456798</b></p>
                                <p class="working-time">Mon-Fri: 8:30am-7:30pm; Sat-Sun: 9:30am-4:30pm</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!--Hero Section-->
        <div class="hero-section hero-background">
            <h1 class="page-title">Organic Fruits</h1>
        </div>

        <!--Navigation section-->
        <div class="container">
            <nav class="biolife-nav">
                <ul>
                    <li class="nav-item"><a href="index-2.html" class="permal-link">Home</a></li>
                    <li class="nav-item"><a href="shop?cid=${category.categoryID}" class="permal-link">${category.categoryName}</a></li>
                    <li class="nav-item"><span class="current-page">${product.productName}</span></li>
                </ul>
            </nav>
        </div>

        <div class="page-contain single-product">
            <div class="container">

                <!-- Main content -->
                <div id="main-content" class="main-content">

                    <form action="product" method="post">
                        <!-- summary info -->
                        <div class="sumary-product single-layout">
                            <div class="media">
                                <ul class="biolife-carousel slider-for" data-slick='{"arrows":false,"dots":false,"slidesMargin":30,"slidesToShow":1,"slidesToScroll":1,"fade":true,"asNavFor":".slider-nav"}'>
                                    <c:if test="${product.image.image1 != null && product.image.image1 != ''}">
                                        <li><img src="${product.image.image1}" alt="" width="500" height="500"></li>
                                        </c:if>

                                    <c:if test="${product.image.image2 != null && product.image.image2 != ''}">
                                        <li><img src="${product.image.image2}" alt="" width="500" height="500"></li>
                                        </c:if>

                                    <c:if test="${product.image.image3 != null && product.image.image3 != ''}">
                                        <li><img src="${product.image.image3}" alt="" width="500" height="500"></li>
                                        </c:if>
                                </ul>
                                <ul class="biolife-carousel slider-nav" data-slick='{"arrows":false,"dots":false,"centerMode":false,"focusOnSelect":true,"slidesMargin":10,"slidesToShow":4,"slidesToScroll":1,"asNavFor":".slider-for"}'>
                                    <c:if test="${product.image.image1 != null && product.image.image1 != ''}">
                                        <li><img src="${product.image.image1}" alt="" width="88" height="88"></li>
                                        </c:if>

                                    <c:if test="${product.image.image2 != null && product.image.image2 != ''}">
                                        <li><img src="${product.image.image2}" alt="" width="88" height="88"></li>
                                        </c:if>

                                    <c:if test="${product.image.image3 != null && product.image.image3 != ''}">
                                        <li><img src="${product.image.image3}" alt="" width="88" height="88"></li>
                                        </c:if>
                                </ul>
                            </div>
                            <div class="product-attribute">
                                <h3 class="title">${product.productName}</h3>
                                <div class="rating">
                                    <p class="star-rating"><span class="width-80percent"></span></p>
                                    <span class="review-count">(${review.size()} Reviews)</span>

                                </div>
                                <span class="sku">Danh Mục: ${product.category.categoryName}</span>
                                <p class="excerpt">${product.productTitle}</p>
                                <div class="price">
                                    <ins><span class="price-amount">${product.lastPrice}<span class="currencySymbol"> đ</span></span></ins>
                                    <del><span class="price-amount">${product.price}<span class="currencySymbol"> đ</span></span></del>
                                </div>
                                <div class="shipping-info">
                                    <p class="shipping-day">3-Day Shipping</p>
                                    <p class="for-today">Pree Pickup Today</p>
                                </div>
                            </div>

                            <div class="action-form">
                                <div class="quantity-box">
                                    <span class="title">Quantity:</span>
                                    <div class="qty-input">
                                        <input type="text" name="qty12554" value="1" data-max_value="20" data-min_value="1" data-step="1">
                                        <a href="#" class="qty-btn btn-up"><i class="fa fa-caret-up" aria-hidden="true"></i></a>
                                        <a href="#" class="qty-btn btn-down"><i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                    </div>  
                                </div>
                                <div class="buttons">

                                    <input style="margin-left: auto;margin-right: auto"  type="submit" class="btn add-to-cart-btn" value="Thêm Vào Giỏ Hàng">

                                    <p class="pull-row">
                                        <a href="#" class="btn wishlist-btn">wishlist</a>
                                        <a href="#" class="btn compare-btn">compare</a>
                                    </p>
                                </div>
                                <div class="location-shipping-to">
                                    <span class="title">Ship to:</span>
                                    <select name="shipping" class="country" >
                                        <c:forEach items="${country}" var="c">
                                            <option value="${c.countryID}">${c.countryName}</option>
                                        </c:forEach>
                                    </select>
                                </div>

                                <div class="acepted-payment-methods">
                                    <ul class="payment-methods">
                                        <li><img src="user/assets/images/card1.jpg" alt="" width="51" height="36"></li>
                                        <li><img src="user/assets/images/card2.jpg" alt="" width="51" height="36"></li>
                                        <li><img src="user/assets/images/card3.jpg" alt="" width="51" height="36"></li>
                                        <li><img src="user/assets/images/card4.jpg" alt="" width="51" height="36"></li>
                                    </ul>
                                </div>
                            </div>
                            <input type="text" name="id" value="${product.productID}" hidden>
                        </div>
                    </form>

                    <!-- Tab info -->
                    <div class="product-tabs single-layout biolife-tab-contain">
                        <div class="tab-head">
                            <ul class="tabs">
                                <li class="tab-element active"><a href="#tab_1st" class="tab-link">Thông Tin Sản Phẩm</a></li>
                                <li class="tab-element" ><a href="#tab_3rd" class="tab-link">Shipping & Delivery</a></li>
                                <li class="tab-element" ><a href="#tab_4th" class="tab-link">Khách Hàng Reviews <sup>(${review.size()})</sup></a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div id="tab_1st" class="tab-contain desc-tab active">
                                <div class="desc-expand">
                                    <span class="title">Giá trị dinh dưỡng của ${product.productName} </span>
                                </div>

                                <p class="desc">&emsp; - ${product.describe}</p>
                                <div class="desc-expand">
                                    <span class="title">Cách bảo quản ${product.productName}</span>
                                    <p class="desc">&emsp; - ${product.preservation}</p>
                                </div>

                                <div class="desc-expand">
                                    <span class="title">Cam Kết Cửa Hàng</span>
                                    <p class="desc">&emsp; - ${product.productTitle}</p>
                                </div>
                            </div>

                            <div id="tab_3rd" class="tab-contain shipping-delivery-tab">
                                <div class="accodition-tab biolife-accodition">
                                    <ul class="tabs">
                                        <li class="tab-item">
                                            <span class="title btn-expand">How long will it take to receive my order?</span>
                                            <div class="content">
                                                <p>Orders placed before 3pm eastern time will normally be processed and shipped by the following business day. For orders received after 3pm, they will generally be processed and shipped on the second business day. For example if you place your order after 3pm on Monday the order will ship on Wednesday. Business days do not include Saturday and Sunday and all Holidays. Please allow additional processing time if you order is placed on a weekend or holiday. Once an order is processed, speed of delivery will be determined as follows based on the shipping mode selected:</p>
                                                <div class="desc-expand">
                                                    <span class="title">Shipping mode</span>
                                                    <ul class="list">
                                                        <li>Standard (in transit 3-5 business days)</li>
                                                        <li>Priority (in transit 2-3 business days)</li>
                                                        <li>Express (in transit 1-2 business days)</li>
                                                        <li>Gift Card Orders are shipped via USPS First Class Mail. First Class mail will be delivered within 8 business days</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="tab-item">
                                            <span class="title btn-expand">How is the shipping cost calculated?</span>
                                            <div class="content">
                                                <p>You will pay a shipping rate based on the weight and size of the order. Large or heavy items may include an oversized handling fee. Total shipping fees are shown in your shopping cart. Please refer to the following shipping table:</p>
                                                <p>Note: Shipping weight calculated in cart may differ from weights listed on product pages due to size and actual weight of the item.</p>
                                            </div>
                                        </li>
                                        <li class="tab-item">
                                            <span class="title btn-expand">Why Didn’t My Order Qualify for FREE shipping?</span>
                                            <div class="content">
                                                <p>We do not deliver to P.O. boxes or military (APO, FPO, PSC) boxes. We deliver to all 50 states plus Puerto Rico. Certain items may be excluded for delivery to Puerto Rico. This will be indicated on the product page.</p>
                                            </div>
                                        </li>
                                        <li class="tab-item">
                                            <span class="title btn-expand">Shipping Restrictions?</span>
                                            <div class="content">
                                                <p>We do not deliver to P.O. boxes or military (APO, FPO, PSC) boxes. We deliver to all 50 states plus Puerto Rico. Certain items may be excluded for delivery to Puerto Rico. This will be indicated on the product page.</p>
                                            </div>
                                        </li>
                                        <li class="tab-item">
                                            <span class="title btn-expand">Undeliverable Packages?</span>
                                            <div class="content">
                                                <p>Occasionally packages are returned to us as undeliverable by the carrier. When the carrier returns an undeliverable package to us, we will cancel the order and refund the purchase price less the shipping charges. Here are a few reasons packages may be returned to us as undeliverable:</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div id="tab_4th" class="tab-contain review-tab">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                                            <div class="rating-info">

                                                <c:if test="${review.size() == 0}">
                                                    <div class="review-form-wrapper text-center" style="margin-top: 10rem; margin-bottom: 10rem">
                                                        <span class="title"> Chưa Có Review Nào ! <span><a href="login"></a></span></span>
                                                    </div>
                                                </c:if>

                                                <c:if test="${review.size() > 0}">
                                                    <p class="index"><strong class="rating">${avarageReview}</strong>out of 5</p>
                                                    <div class="rating">
                                                        <ul class="" style="display: flex;padding-left: 0; list-style-type: none">
                                                            <c:set var="count" value="1"/>
                                                            <c:forEach begin="1" end="${avarageReview}" >
                                                                <li style="padding-right: 0.3rem"><i class="fa fa-star" style="color: #ffdd5a;font-size: 16px"></i></li>
                                                                    <c:set var="count" value="${count + 1}"/>
                                                                </c:forEach>
                                                                <c:if test="${count <= 5}">
                                                                    <c:forEach begin="${count}" end="5" >
                                                                    <li style="padding-right: 0.3rem"><i class="fa fa-star-o"></i></li>
                                                                    </c:forEach>
                                                                </c:if>
                                                        </ul>

                                                    </div>
                                                    <p class="see-all">See all ${review.size()} reviews</p>
                                                    <ul class="options">
                                                        <li>
                                                            <div class="detail-for">
                                                                <span class="option-name">5stars</span>
                                                                <span class="progres">
                                                                    <span class="line-100percent"><span class="percent ${fivePercent}"></span></span>
                                                                </span>
                                                                <span class="number">${fiveStart}</span>
                                                            </div>
                                                        </li>

                                                        <li>
                                                            <div class="detail-for">
                                                                <span class="option-name">4stars</span>
                                                                <span class="progres">
                                                                    <span class="line-100percent"><span class="percent ${fourPercent}"></span></span>
                                                                </span>
                                                                <span class="number">${fourStart}</span>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="detail-for">
                                                                <span class="option-name">3stars</span>
                                                                <span class="progres">
                                                                    <span class="line-100percent"><span class="percent ${threePercent}"></span></span>
                                                                </span>
                                                                <span class="number">${threeStart}</span>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="detail-for">
                                                                <span class="option-name">2stars</span>
                                                                <span class="progres">
                                                                    <span class="line-100percent"><span class="percent ${secondPercent}"></span></span>
                                                                </span>
                                                                <span class="number">${secondStart}</span>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="detail-for">
                                                                <span class="option-name">1star</span>
                                                                <span class="progres">
                                                                    <span class="line-100percent"><span class="percent ${onePercent}"></span></span>
                                                                </span>
                                                                <span class="number">${oneStart}</span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </c:if>

                                            </div>
                                        </div>

                                        <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                                            <c:if test="${account == null}">
                                                <div class="review-form-wrapper text-center" style="margin-top: 10rem; margin-bottom: 10rem">
                                                    <span class="title">Bạn Phải Đăng Nhập Để Viết Review <span><a href="login">Đăng Nhập ?</a></span></span>
                                                </div>
                                            </c:if>

                                            <c:if test="${account != null}">
                                                <div class="review-form-wrapper">
                                                    <span class="title">Submit your review</span>
                                                    <form action="review"  method="post">
                                                        <div class="comment-form-rating">

                                                            <label>1. Your rating of this products:</label>
                                                            <p class="stars">
                                                                <span>
                                                                    <a class="btn-rating" onclick="change(1)" data-value="star-1" href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                                                    <a class="btn-rating" onclick="change(2)" data-value="star-2" href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                                                    <a class="btn-rating" onclick="change(3)" data-value="star-3" href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                                                    <a class="btn-rating" onclick="change(4)" data-value="star-4" href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                                                    <a class="btn-rating" onclick="change(5)" data-value="star-5" href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                                                </span>
                                                            </p>
                                                            <input type="number" name="rate" id="starReview" value="1" hidden>

                                                        </div>

                                                        <p class="form-row wide-half">
                                                            <input type="text" name="name" value="${account.fullName}" placeholder="Your name" readonly>
                                                        </p>
                                                        <p class="form-row wide-half">
                                                            <input type="email" name="email" value="${account.email}" placeholder="Email address" required>
                                                        </p>
                                                        <p class="form-row">
                                                            <textarea name="content" id="txt-comment" cols="30" rows="10" placeholder="Write your review here..."></textarea>
                                                        </p>
                                                        <p class="form-row">
                                                            <button type="submit">submit review</button>

                                                        </p>
                                                        <input type="text" name="userName"  value="${account.userName}" hidden>
                                                        <input type="text" name="productID" value="${product.productID}" hidden>
                                                    </form>
                                                </div>
                                            </c:if>

                                        </div>
                                    </div>
                                    <div id="comments">
                                        <ol class="commentlist">
                                            <li class="review">
                                                <div class="comment-container">
                                                    <c:forEach items="${review}" var="r">
                                                        <div class="row">
                                                            <div class="comment-content col-lg-8 col-md-9 col-sm-8 col-xs-12">
                                                                <p class="comment-in"><span class="post-name">${r.account.fullName}</span><span class="post-date">${r.reviewTimeCreate}</span></p>
                                                                <!--<div class="rating"><p class="star-rating"><span class="width-80percent"></span></p></div>-->
                                                                <ul class="" style="display: flex;padding-left: 0; list-style-type: none">
                                                                    <c:set var="count" value="1"/>
                                                                    <c:forEach begin="1" end="${r.rate}" >
                                                                        <li style="padding-right: 0.3rem"><i class="fa fa-star" style="color: #ffdd5a"></i></li>
                                                                            <c:set var="count" value="${count + 1}"/>
                                                                        </c:forEach>
                                                                        <c:if test="${count <= 5}">
                                                                            <c:forEach begin="${count}" end="5" >
                                                                            <li style="padding-right: 0.3rem"><i class="fa fa-star-o"></i></li>
                                                                            </c:forEach>
                                                                        </c:if>
                                                                </ul>

                                                                <p class="comment-text">${r.content}.</p>
                                                            </div>
                                                            <div class="comment-review-form col-lg-3 col-lg-offset-1 col-md-3 col-sm-4 col-xs-12">
                                                                <span class="title">Was this review helpful?</span>
                                                                <ul class="actions">
                                                                    <li><a href="#" class="btn-act like" data-type="like"><i class="fa fa-thumbs-up" aria-hidden="true"></i>Yes (100)</a></li>
                                                                    <li><a href="#" class="btn-act hate" data-type="dislike"><i class="fa fa-thumbs-down" aria-hidden="true"></i>No (20)</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </c:forEach>
                                                </div>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- related products -->
                    <div class="product-related-box single-layout">
                        <div class="biolife-title-box lg-margin-bottom-26px-im">
                            <span class="biolife-icon icon-organic"></span>
                            <span class="subtitle">All the best item for You</span>
                            <h3 class="main-title">Related Products</h3>
                        </div>
                        <ul class="products-list biolife-carousel nav-center-02 nav-none-on-mobile" data-slick='{"rows":1,"arrows":true,"dots":false,"infinite":false,"speed":400,"slidesMargin":0,"slidesToShow":5, "responsive":[{"breakpoint":1200, "settings":{ "slidesToShow": 4}},{"breakpoint":992, "settings":{ "slidesToShow": 3, "slidesMargin":20 }},{"breakpoint":768, "settings":{ "slidesToShow": 2, "slidesMargin":10}}]}'>
                            <c:forEach items="${relatedProduct}" var="p">
                                <li class="product-item">
                                    <div class="contain-product layout-default">
                                        <div class="product-thumb">
                                            <a href="product?id=${p.productID}" class="link-to-product">
                                                <img src="${p.image.image2}" alt="dd" width="270" height="270" class="product-thumnail">
                                            </a>
                                        </div>
                                        <div class="info">
                                            <b class="categories">${p.category.categoryName}</b>
                                            <h4 class="product-title"><a href="product?id=${p.productID}" class="pr-name">${p.productName}</a></h4>
                                            <div class="price">
                                                <ins><span class="price-amount">${p.lastPrice}</span></ins>
                                                <del><span class="price-amount">${p.price}</span></del>
                                            </div>
                                            <div class="slide-down-box">
                                                <p class="message">All products are carefully selected to ensure food safety.</p>
                                                <div class="buttons">
                                                    <a href="#" class="btn wishlist-btn"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                                    <a href="#" class="btn add-to-cart-btn"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>add to cart</a>
                                                    <a href="#" class="btn compare-btn"><i class="fa fa-random" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- FOOTER -->
        <footer id="footer" class="footer layout-03">
            <div class="footer-content background-footer-03">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-9">
                            <section class="footer-item">
                                <a href="home" class="logo footer-logo"><img src="user/assets/images/organic-3.png" alt="biolife logo" width="135" height="34"></a>
                                <div class="footer-phone-info">
                                    <i class="biolife-icon icon-head-phone"></i>
                                    <p class="r-info">
                                        <span>Need Help ?</span>
                                        <span>(84)  0838456798</span>
                                    </p>
                                </div>
                                <div class="newsletter-block layout-01">
                                    <h4 class="title">Newsletter Signup</h4>
                                    <div class="form-content">
                                        <form action="#" name="new-letter-foter">
                                            <input type="email" class="input-text email" value="" placeholder="Your email here...">
                                            <button type="submit" class="bnt-submit" name="ok">Sign up</button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 md-margin-top-5px sm-margin-top-50px xs-margin-top-40px">
                            <section class="footer-item">
                                <h3 class="section-title">Useful Links</h3>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6 col-xs-6">
                                        <div class="wrap-custom-menu vertical-menu-2">
                                            <ul class="menu">
                                                <li><a href="#">About Us</a></li>
                                                <li><a href="#">About Our Shop</a></li>
                                                <li><a href="#">Secure Shopping</a></li>
                                                <li><a href="#">Delivery infomation</a></li>
                                                <li><a href="#">Privacy Policy</a></li>
                                                <li><a href="#">Our Sitemap</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-6 col-xs-6">
                                        <div class="wrap-custom-menu vertical-menu-2">
                                            <ul class="menu">
                                                <li><a href="#">Who We Are</a></li>
                                                <li><a href="#">Our Services</a></li>
                                                <li><a href="#">Projects</a></li>
                                                <li><a href="#">Contacts Us</a></li>
                                                <li><a href="#">Innovation</a></li>
                                                <li><a href="#">Testimonials</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 md-margin-top-5px sm-margin-top-50px xs-margin-top-40px">
                            <section class="footer-item">
                                <h3 class="section-title">Contact</h3>
                                <div class="contact-info-block footer-layout xs-padding-top-10px">
                                    <ul class="contact-lines">
                                        <li>
                                            <p class="info-item">
                                                <i class="biolife-icon icon-location"></i>
                                                <b class="desc">Đại Học FPT Hà Nội, Hòa Lạc</b>
                                            </p>
                                        </li>
                                        <li>
                                            <p class="info-item">
                                                <i class="biolife-icon icon-phone"></i>
                                                <b class="desc">Phone: (+084) 0838456798</b>
                                            </p>
                                        </li>
                                        <li>
                                            <p class="info-item">
                                                <i class="biolife-icon icon-letter"></i>
                                                <b class="desc">Email:  phanhieu000lc@gmail.com</b>
                                            </p>
                                        </li>
                                        <li>
                                            <p class="info-item">
                                                <i class="biolife-icon icon-clock"></i>
                                                <b class="desc">Hours: 10:00 AM - 20:00 PM</b>
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="biolife-social inline">
                                    <ul class="socials">
                                        <li><a href="#" title="twitter" class="socail-btn"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#" title="facebook" class="socail-btn"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#" title="pinterest" class="socail-btn"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                        <li><a href="#" title="youtube" class="socail-btn"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                        <li><a href="#" title="instagram" class="socail-btn"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="separator sm-margin-top-70px xs-margin-top-40px"></div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">

                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="payment-methods">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <!--Footer For Mobile-->
        <div class="mobile-footer">
            <div class="mobile-footer-inner">
                <div class="mobile-block block-menu-main">
                    <a class="menu-bar menu-toggle btn-toggle" data-object="open-mobile-menu" href="javascript:void(0)">
                        <span class="fa fa-bars"></span>
                        <span class="text">Menu</span>
                    </a>
                </div>
                <div class="mobile-block block-sidebar">
                    <a class="menu-bar filter-toggle btn-toggle" data-object="open-mobile-filter" href="javascript:void(0)">
                        <i class="fa fa-sliders" aria-hidden="true"></i>
                        <span class="text">Sidebar</span>
                    </a>
                </div>
                <div class="mobile-block block-minicart">
                    <a class="link-to-cart" href="#">
                        <span class="fa fa-shopping-bag" aria-hidden="true"></span>
                        <span class="text">Cart</span>
                    </a>
                </div>
                <div class="mobile-block block-global">
                    <a class="menu-bar myaccount-toggle btn-toggle" data-object="global-panel-opened" href="javascript:void(0)">
                        <span class="fa fa-globe"></span>
                        <span class="text">More</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="mobile-block-global">
            <div class="biolife-mobile-panels">
                <span class="biolife-current-panel-title">More</span>
                <a class="biolife-close-btn" data-object="global-panel-opened" href="#">&times;</a>
            </div>
            <div class="block-global-contain">
                <div class="glb-item my-account">
                    <b class="title">My Account</b>
                    <ul class="list">
                        <li class="list-item"><a href="login">Login/register</a></li>
                        <li class="list-item"><a href="#">Wishlist <span class="index">(8)</span></a></li>
                        <li class="list-item"><a href="checkout">Checkout</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Scroll Top Button -->
        <a class="btn-scroll-top"><i class="biolife-icon icon-left-arrow"></i></a>

        <script src="user/assets/js/jquery-3.4.1.min.js"></script>
        <script src="user/assets/js/bootstrap.min.js"></script>
        <script src="user/assets/js/jquery.countdown.min.js"></script>
        <script src="user/assets/js/jquery.nice-select.min.js"></script>
        <script src="user/assets/js/jquery.nicescroll.min.js"></script>
        <script src="user/assets/js/slick.min.js"></script>
        <script src="user/assets/js/biolife.framework.js"></script>
        <script src="user/assets/js/functions.js"></script>
        <script src="user/assets/js/mycode.js"></script>

    </body>

</html>