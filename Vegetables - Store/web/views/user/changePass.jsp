<%-- 
    Document   : profile
    Created on : 20-10-2022, 23:16:51
    Author     : phanh
--%>

<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html >

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Cairo:400,600,700&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:600&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400i,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu&amp;display=swap" rel="stylesheet">
        <link rel="shortcut icon" type="image/x-icon" href="user/assets/images/favicon.png" />
        <link rel="stylesheet" href="user/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="user/assets/css/animate.min.css">
        <link rel="stylesheet" href="user/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="user/assets/css/nice-select.css">
        <link rel="stylesheet" href="user/assets/css/slick.min.css">
        <link rel="stylesheet" href="user/assets/css/style.css">
        <link rel="stylesheet" href="user/assets/css/main-color.css"><style>
            * {
                font-family: initial;
            }
        </style>
        <title>Candles Store</title>
    </head>
    <body class="biolife-body">

        <!-- Preloader -->
        <div id="biof-loading">
            <div class="biof-loading-center">
                <div class="biof-loading-center-absolute">
                    <div class="dot dot-one"></div>
                    <div class="dot dot-two"></div>
                    <div class="dot dot-three"></div>
                </div>
            </div>
        </div>

        <!-- HEADER -->
        <header id="header" class="header-area style-01 layout-03">
            <div class="header-top bg-main hidden-xs">
                <div class="container-fluid">
                    <div class="top-bar left">
                        <ul class="horizontal-menu">
                            <li><a href="#"><i class="fa fa-truck" aria-hidden="true"></i>Miễn Phí Ship Với Đơn Hàng Từ 399k</a></li>
                            <li><a href="#"></a></li>
                        </ul> 
                    </div>
                    <div class="top-bar right">
                        <ul class="horizontal-menu">
                            <li>
                                <c:if test="${account == null}">
                                    <a href="login" class="login-link">
                                        <i class="biolife-icon icon-login"></i>Login/Register</a>
                                    </c:if>

                                <c:if test="${account != null}">
                                    <a href="profiles" class="login-link">
                                        <i class="biolife-icon icon-login"></i>Hello: ${account.fullName}</a>
                                    <a href="logout" class="login-link" style="margin-left: 1rem">
                                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                                        Logout</a>
                                    </c:if>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="header-middle biolife-sticky-object ">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-2 col-md-6 col-xs-6">
                            <a href="home" class="biolife-logo">
                                <img src="user/assets/images/organic-3.png" alt="biolife logo" width="135" height="34">
                            </a>
                        </div>
                        <div class="col-lg-6 col-md-7 hidden-sm hidden-xs">
                            <div class="primary-menu">
                                <ul class="menu biolife-menu clone-main-menu clone-primary-menu" id="primary-menu" data-menuname="main menu">
                                    <li class="menu-item"><a href="home">Home</a></li>
                                    <li class="menu-item"> <a href="shop">Shop</a></li>
                                    <li class="menu-item menu-item-has-children has-child">
                                        <a href="#" class="menu-name" data-title="Products">Nổi Bật</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item"><a href="#">top1</a></li>
                                            <li class="menu-item"><a href="#">top2</a></li>
                                            <li class="menu-item menu-item-has-children has-child"><a href="#" class="menu-name" data-title="Eggs & other considerations">top3</a>
                                                <ul class="sub-menu">
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                    <li class="menu-item"><a href="#">top3-sub</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item"><a href="#">top4</a></li>
                                            <li class="menu-item"><a href="#">top5</a></li>
                                        </ul>
                                    </li>

                                    <li class="menu-item menu-item-has-children has-megamenu">
                                        <a href="#" class="menu-name" data-title="Blog">Đồ Ăn Nhanh</a>
                                        <div class="wrap-megamenu lg-width-800 md-width-750">
                                            <div class="mega-content">
                                                <div class="col-lg-3 col-md-3 col-xs-6">
                                                    <div class="wrap-custom-menu vertical-menu">
                                                        <h4 class="menu-title">Đồ Ăn Nhanh</h4>
                                                        <ul class="menu">
                                                            <li><a href="#">top1</a></li>
                                                            <li><a href="#">top2</a></li>
                                                            <li><a href="#">top3</a></li>
                                                            <li><a href="#">top4</a></li>
                                                            <li><a href="#">top5</a></li>
                                                            <li><a href="#">top6</a></li>
                                                            <li><a href="#">top7</a></li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-xs-12 md-margin-top-0 xs-margin-top-25px">
                                                    <div class="block-posts">
                                                        <h4 class="menu-title">Đồ Ăn Nhanh Có Đánh Giá Cao</h4>
                                                        <ul class="posts">
                                                            <li>
                                                                <div class="block-post-item">
                                                                    <div class="thumb"><a href="#"><img src="user/assets/images/megamenu/thumb-05.jpg" width="100" height="73" alt=""></a></div>
                                                                    <div class="left-info">
                                                                        <h4 class="post-name"><a href="#">Ashwagandha: The #1 Herb in the World for Anxiety?</a></h4>
                                                                        <span class="p-date">Jan 05, 2019</span>
                                                                        <span class="p-comment">2 Comments</span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="block-post-item">
                                                                    <div class="thumb"><a href="#"><img src="user/assets/images/megamenu/thumb-06.jpg" width="100" height="73" alt=""></a></div>
                                                                    <div class="left-info">
                                                                        <h4 class="post-name"><a href="#">Ashwagandha: The #1 Herb in the World for Anxiety?</a></h4>
                                                                        <span class="p-date">May 15, 2019</span>
                                                                        <span class="p-comment">8 Comments</span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="block-post-item">
                                                                    <div class="thumb"><a href="#"><img src="user/assets/images/megamenu/thumb-07.jpg" width="100" height="73" alt=""></a></div>
                                                                    <div class="left-info">
                                                                        <h4 class="post-name"><a href="#">Ashwagandha: The #1 Herb in the World for Anxiety?</a></h4>
                                                                        <span class="p-date">Apr 26, 2019</span>
                                                                        <span class="p-comment">10 Comments</span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="menu-item"><a href="contact.html">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-md-6 col-xs-6">
                            <div class="biolife-cart-info">
                                <div class="mobile-search">
                                    <a href="javascript:void(0)" class="open-searchbox"><i class="biolife-icon icon-search"></i></a>
                                    <div class="mobile-search-content">
                                        <form action="#" class="form-search" method="get">
                                            <a href="#" class="btn-close"><span class="biolife-icon icon-close-menu"></span></a>
                                            <input type="text" name="s" class="input-text" value="" placeholder="Search here.....">
                                            <select name="category">
                                                <option value="0" selected>All Categories</option>
                                                <c:forEach items="${listCategory}" var="c">
                                                    <option value="${c.categoryID}">${c.categoryName}</option>
                                                </c:forEach>

                                            </select>
                                            <button type="submit" class="btn-submit">Search</button>
                                        </form>
                                    </div>
                                </div>

                                <div class="minicart-block">
                                    <div class="minicart-contain">
                                        <a href="cart" class="link-to">
                                            <span class="icon-qty-combine">
                                                <i class="icon-cart-mini biolife-icon"></i>
                                                <span class="qty">${cart.items.size()}</span>
                                            </span>
                                            <span class="title">My Cart -</span>
                                            <span class="sub-total">${cart.totalMoney} đ</span>
                                        </a>
                                        <div class="cart-content">
                                            <div class="cart-inner">

                                                <c:if test="${cart.items.size() == 0}">
                                                    <h3 style="text-align: center">Chưa Có Sản Phẩm Nào</h3>
                                                </c:if>

                                                <c:if test="${cart.items.size() != 0}">
                                                    <ul class="products">
                                                        <c:forEach items="${cart.items}" var="i">
                                                            <li>
                                                                <div class="minicart-item">
                                                                    <div class="thumb">
                                                                        <a href="product?id=${i.product.productID}">
                                                                            <c:if test="${i.product.image.image1 != null}">
                                                                                <img src="${i.product.image.image1}" width="90" height="90" alt="National Fresh">
                                                                            </c:if>
                                                                            <c:if test="${i.product.image.image1 == null || i.product.image.image1 == ''}">
                                                                                <img src="${i.product.image.image2}" width="90" height="90" alt="National Fresh">
                                                                            </c:if>
                                                                        </a>
                                                                    </div>
                                                                    <div class="left-info">
                                                                        <div class="product-title"><a href="product?id=${i.product.productID}" class="product-name">${i.product.productName}</a></div>
                                                                        <div class="price">
                                                                            <ins><span class="price-amount">${i.product.lastPrice}<span class="currencySymbol"> đ</span></span></ins>
                                                                            <del><span class="price-amount">${i.product.price}<span class="currencySymbol"> đ</span></del>
                                                                        </div>
                                                                        <div class="qty">
                                                                            <label for="cart[id123][qty]">Qty:</label>
                                                                            <input type="number" class="input-qty" name="cart[id123][qty]" id="cart[id123][qty]" value="${i.quantity}" disabled>
                                                                        </div>
                                                                    </div>                                                                   
                                                                </div>
                                                            </li>
                                                        </c:forEach>
                                                    </ul>
                                                </c:if>


                                                <p class="btn-control">
                                                    <a href="cart" class="btn view-cart">view cart</a>
                                                    <a href="checkout" class="btn">checkout</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mobile-menu-toggle">
                                    <a class="btn-toggle" data-object="open-mobile-menu" href="javascript:void(0)">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom hidden-sm hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-4">
                            <div class="vertical-menu vertical-category-block">
                                <div class="block-title">
                                    <span class="menu-icon">
                                        <span class="line-1"></span>
                                        <span class="line-2"></span>
                                        <span class="line-3"></span>
                                    </span>
                                    <span class="menu-title">All departments</span>
                                    <span class="angle" data-tgleclass="fa fa-caret-down"><i class="fa fa-caret-up" aria-hidden="true"></i></span>
                                </div>
                                <div class="wrap-menu">
                                    <ul class="menu clone-main-menu">
                                        <c:forEach items="${listCategory}" var="c">
                                            <c:if test="${c.categoryID == 1}">
                                                <li class="menu-item menu-item-has-children has-megamenu">
                                                    <a href="#" class="menu-name" data-title="Fruit & Nut Gifts"><i class="biolife-icon icon-fruits"></i>${c.categoryName}</a>
                                                    <div class="wrap-megamenu lg-width-900 md-width-640">
                                                        <div class="mega-content">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-8 col-sm-12 xs-margin-bottom-25 md-margin-bottom-0">
                                                                    <div class="wrap-custom-menu vertical-menu">
                                                                        <h4 class="menu-title">${c.categoryName}</h4>
                                                                        <ul class="menu">
                                                                            <c:forEach items="${fruit}" var="p">
                                                                                <li><a href="product?id=${p.productID}">${p.productName}</a></li>
                                                                                </c:forEach>
                                                                        </ul>
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-6 col-md-4 col-sm-12 lg-padding-left-50 xs-margin-bottom-25 md-margin-bottom-0">
                                                                    <div class="biolife-products-block max-width-270">
                                                                        <h4 class="menu-title">Bestseller Products</h4>
                                                                        <ul class="products-list default-product-style biolife-carousel nav-none-after-1k2 nav-center" data-slick='{"rows":1,"arrows":true,"dots":false,"infinite":false,"speed":400,"slidesMargin":30,"slidesToShow":1, "responsive":[{"breakpoint":767, "settings":{ "arrows": false}}]}' >
                                                                            <c:forEach items="${listFruitSeller}" var="p">
                                                                                <li class="product-item">
                                                                                    <div class="contain-product none-overlay">
                                                                                        <div class="product-thumb">
                                                                                            <a href="product?id=${p.productID}" class="link-to-product">
                                                                                                <img src="${p.image.image1}" alt="dd" width="270" height="270" class="product-thumnail">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="info">
                                                                                            <b class="categories">${p.category.categoryName}</b>
                                                                                            <h4 class="product-title"><a href="product?id=${p.productID}" class="pr-name">${p.productName}</a></h4>
                                                                                            <div class="price">
                                                                                                <ins><span class="price-amount">${p.lastPrice}<span class="currencySymbol"> đ</span></span></ins>
                                                                                                <del><span class="price-amount">${p.price}<span class="currencySymbol"> đ</span></span></del>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                            </c:forEach>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 md-margin-top-9">
                                                                    <div class="biolife-brand" >
                                                                        <ul class="brands">
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/brand-organic.png" width="161" height="136" alt="organic"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/brand-explore.png" width="160" height="136" alt="explore"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/brand-organic-2.png" width="99" height="136" alt="organic 2"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/brand-eco-teas.png" width="164"  height="136" alt="eco teas"></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </c:if>
                                            <c:if test="${c.categoryID == 2}">
                                                <li class="menu-item menu-item-has-children has-megamenu">
                                                    <a href="shop?cid=${c.categoryID}" class="menu-name" data-title="Vegetables"><i class="biolife-icon icon-broccoli-1"></i>${c.categoryName}</a>
                                                    <div class="wrap-megamenu lg-width-900 md-width-640 background-mega-01">
                                                        <div class="mega-content">
                                                            <div class="row">
                                                                <div class="col-lg-7 col-md-8 col-sm-12 xs-margin-bottom-25 md-margin-bottom-0">
                                                                    <div class="wrap-custom-menu vertical-menu">
                                                                        <h4 class="menu-title">Vegetables</h4>
                                                                        <ul class="menu">
                                                                            <li><a href="#">Fruit & Nut Gifts</a></li>
                                                                            <li><a href="#">Mixed Fruits</a></li>
                                                                            <li><a href="#">Oranges</a></li>
                                                                            <li><a href="#">Bananas & Plantains</a></li>
                                                                            <li><a href="#">Fresh Gala Apples</a></li>
                                                                            <li><a href="#">Berries</a></li>
                                                                            <li><a href="#">Pears</a></li>
                                                                            <li><a href="#">Produce</a></li>
                                                                            <li><a href="#">Snack Foods</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-5 col-md-4 col-sm-12 lg-padding-left-57 md-margin-bottom-30">
                                                                    <div class="biolife-brand vertical md-boder-left-30">
                                                                        <h4 class="menu-title">Hot Brand</h4>
                                                                        <ul class="brands">
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/v-brand-organic.png" width="167" height="74" alt="organic"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/v-brand-explore.png" width="167" height="72" alt="explore"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/v-brand-organic-2.png" width="167" height="99" alt="organic 2"></a></li>
                                                                            <li><a href="#"><img src="user/assets/images/megamenu/v-brand-eco-teas.png" width="167" height="67" alt="eco teas"></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </c:if>

                                            <c:if test="${c.categoryID == 3}">
                                                <li class="menu-item menu-item-has-children has-megamenu">
                                                    <a href="shop?cid=${c.categoryID}" class="menu-name" data-title="Fresh Berries"><i class="biolife-icon icon-grape"></i>${c.categoryName}</a>
                                                    <div class="wrap-megamenu lg-width-900 md-width-640 background-mega-02">
                                                        <div class="mega-content">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-4 sm-col-12 md-margin-bottom-83 xs-margin-bottom-25">
                                                                    <div class="wrap-custom-menu vertical-menu">
                                                                        <h4 class="menu-title">Fresh Berries</h4>
                                                                        <ul class="menu">
                                                                            <li><a href="#">Fruit & Nut Gifts</a></li>
                                                                            <li><a href="#">Mixed Fruits</a></li>
                                                                            <li><a href="#">Oranges</a></li>
                                                                            <li><a href="#">Bananas & Plantains</a></li>
                                                                            <li><a href="#">Fresh Gala Apples</a></li>
                                                                            <li><a href="#">Berries</a></li>
                                                                            <li><a href="#">Pears</a></li>
                                                                            <li><a href="#">Produce</a></li>
                                                                            <li><a href="#">Snack Foods</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-md-4 sm-col-12 lg-padding-left-23 xs-margin-bottom-36px md-margin-bottom-0">
                                                                    <div class="wrap-custom-menu vertical-menu">
                                                                        <h4 class="menu-title">Gifts</h4>
                                                                        <ul class="menu">
                                                                            <li><a href="#">Non-Dairy Coffee Creamers</a></li>
                                                                            <li><a href="#">Coffee Creamers</a></li>
                                                                            <li><a href="#">Mayonnaise</a></li>
                                                                            <li><a href="#">Almond Milk</a></li>
                                                                            <li><a href="#">Ghee</a></li>
                                                                            <li><a href="#">Beverages</a></li>
                                                                            <li><a href="#">Ranch Salad Dressings</a></li>
                                                                            <li><a href="#">Hemp Milk</a></li>
                                                                            <li><a href="#">Nuts & Seeds</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-4 sm-col-12 lg-padding-left-25 md-padding-top-55">
                                                                    <div class="biolife-banner layout-01">
                                                                        <h3 class="top-title">Farm Fresh</h3>
                                                                        <p class="content"> All the Lorem Ipsum generators on the Internet tend.</p>
                                                                        <b class="bottomm-title">Berries Series</b>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </c:if>

                                            <c:if test="${c.categoryID != 1 && c.categoryID != 2 && c.categoryID != 3}">
                                                <li class="menu-item"><a href="shop?cid=${c.categoryID}" class="menu-name" data-title="">
                                                        <i class="biolife-icon ${c.categoryName eq 'Fresh Meat' ? 'icon-beef' : ''}
                                                           ${c.categoryName eq 'Ocean Foods' ? 'icon-fish' : ''}
                                                           ${c.categoryName eq 'Fresh Onion' ? 'icon-onions' : ''}
                                                           ${c.categoryName eq 'Spice' ? 'icon-contain' : ''}"></i>${c.categoryName}</a></li>
                                                    </c:if>   
                                                </c:forEach>


                                        <li class="menu-item"><a href="#" class="menu-name" data-title="Butter & Eggs"><i class="biolife-icon icon-honey"></i>Butter & Eggs</a></li>
                                        <li class="menu-item"><a href="#" class="menu-title"><i class="biolife-icon icon-fast-food"></i>Fastfood</a></li>
                                        <li class="menu-item"><a href="#" class="menu-title"><i class="biolife-icon icon-avocado"></i>Papaya & Crisps</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-8 padding-top-2px">
                            <div class="header-search-bar layout-01">
                                <form action="shop"  method="get">
                                    <input type="text" name="search" class="input-text" placeholder="Tìm Kiếm Sản Phẩm ở Đây">
                                    <select name="cid">
                                        <option value="0" selected>All Categories</option>
                                        <c:forEach items="${listCategory}" var="c">
                                            <option value="${c.categoryID}">${c.categoryName}</option>
                                        </c:forEach>


                                    </select>
                                    <button type="submit" class="btn-submit"><i class="biolife-icon icon-search"></i></button>
                                </form>
                            </div>
                            <div class="live-info">
                                <p class="telephone"><i class="fa fa-phone" aria-hidden="true"></i><b class="phone-number">0838456798</b></p>
                                <p class="working-time">Mon-Fri: 8:30am-7:30pm; Sat-Sun: 9:30am-4:30pm</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!--Hero Section-->
        <div class="hero-section hero-background">
            <h1 class="page-title">Organic Fruits</h1>
        </div>

        <!--Navigation section-->
        <div class="container">
            <nav class="biolife-nav nav-86px">
                <ul>
                    <li class="nav-item"><a href="home" class="permal-link">Home</a></li>
                    <li class="nav-item"><span class="current-page">Profiles</span></li>
                </ul>
            </nav>
        </div>

        <div class="page-contain contact-us">

            <!-- Main content -->
            <div id="main-content" class="main-content">
                <div class="container">
                    <div class="row">
                        <!--Contact info-->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contact-info-container sm-margin-top-27px xs-margin-bottom-60px xs-margin-top-60px">
                                <h4 class="box-title">Thông Tin Của Bạn</h4>
                                <p class="frst-desc">Địa Chỉ Nhận Hàng</p>
                                <ul class="addr-info">
                                    <li>
                                        <div class="if-item">
                                            <b class="tie">Addess:</b>
                                            <p class="dsc">${account.address}, ${account.country.countryName}</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="if-item">
                                            <b class="tie">Phone:</b>
                                            <p class="dsc">(+84) ${account.phone}</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="if-item">
                                            <b class="tie">Email:</b>
                                            <p class="dsc">${account.email}</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="if-item">
                                            <b class="tie">Store Open:</b>
                                            <p class="dsc">8am - 08pm, Mon - Sat</p>
                                        </div>
                                    </li>
                                </ul>

                            </div>
                        </div>

                        <!--Contact form-->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="contact-form-container sm-margin-top-112px">
                                <form action=""  method="post" >
                                    <input type="text" value="${account.password}" hidden id="password">
                                    <div class="form-row row">
                                        <div class="col-md-12" style="margin-bottom: 2rem">

                                            <input type="password" id="oldPass" onchange="checkOldPass()"  placeholder="Mật Khẩu Hiện Tại" class="txt-input" required >
                                            <h5 style="color: red;text-align: center" hidden id="messageOld">Mật Khẩu Cũ Không Đúng</h5>

                                        </div>
                                        <div class="col-md-6" >
                                            <input type="password" id="newPass" onchange="checkNewPass()" name="newPass" placeholder="Mật Khẩu Mới" class="txt-input" required >
                                            <h5 style="color: red;text-align: center" hidden id="messageNew">Mật Khẩu Mới Không Được Giống Mật Khẩu Cũ</h5>

                                        </div>
                                        <div class="col-md-6" >
                                            <input type="password" id="re-newPass" onchange="checkReNewPass()" placeholder="Xác Nhận Mật Khẩu Mới" class="txt-input" required >
                                            <h5 style="color: red;text-align: center" hidden id="messageReNew">Mật Khẩu Xác Nhận Không Đúng</h5>

                                        </div>
                                    </div>


                                    <div class="form-row row" style="margin-top: 3rem">
                                        <div class="col-md-4">
                                            <input onclick="return checkChangePass()" class="btn btn-submit" type="submit" id="changePass" value="Change Pass" >   
                                        </div>
                                    </div>
                                </form>
                                <script type="text/javascript">
                                    function checkOldPass() {
                                        var password = document.getElementById('password').value;
                                        var oldPass = document.getElementById('oldPass').value;
                                        if (password !== oldPass) {
                                            document.getElementById('messageOld').hidden = false;
                                            return false;
                                        } else {
                                            document.getElementById('messageOld').hidden = true;
                                            return true;
                                        }

                                    }

                                    function checkNewPass() {
                                        var newPass = document.getElementById('newPass').value;
                                        var oldPass = document.getElementById('oldPass').value;
                                        if (newPass === oldPass) {
                                            document.getElementById('messageNew').hidden = false;
                                            return false;
                                        } else {
                                            document.getElementById('messageNew').hidden = true;
                                            return true;
                                        }

                                    }

                                    function checkReNewPass() {
                                        var newPass = document.getElementById('newPass').value;
                                        var reNewPass = document.getElementById('re-newPass').value;
                                        if (newPass !== reNewPass) {
                                            document.getElementById('messageReNew').hidden = false;
                                            return false;
                                        } else {
                                            document.getElementById('messageReNew').hidden = true;
                                            return true;
                                        }


                                    }
                                    
                                    function checkChangePass(){
                                        if(checkNewPass() && checkOldPass() && checkReNewPass()){
                                            return true;
                                        }
                                        return false;
                                    }
                                </script>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- FOOTER -->
        <footer id="footer" class="footer layout-03">
            <div class="footer-content background-footer-03">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-9">
                            <section class="footer-item">
                                <a href="#" class="logo footer-logo"><img src="user/assets/images/organic-3.png" alt="biolife logo" width="135" height="34"></a>
                                <div class="footer-phone-info">
                                    <i class="biolife-icon icon-head-phone"></i>
                                    <p class="r-info">
                                        <span>Got Questions ?</span>
                                        <span>(700)  9001-1909  (900) 689 -66</span>
                                    </p>
                                </div>
                                <div class="newsletter-block layout-01">
                                    <h4 class="title">Newsletter Signup</h4>
                                    <div class="form-content">
                                        <form action="#" name="new-letter-foter">
                                            <input type="email" class="input-text email" value="" placeholder="Your email here...">
                                            <button type="submit" class="bnt-submit" name="ok">Sign up</button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 md-margin-top-5px sm-margin-top-50px xs-margin-top-40px">
                            <section class="footer-item">
                                <h3 class="section-title">Useful Links</h3>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6 col-xs-6">
                                        <div class="wrap-custom-menu vertical-menu-2">
                                            <ul class="menu">
                                                <li><a href="#">About Us</a></li>
                                                <li><a href="#">About Our Shop</a></li>
                                                <li><a href="#">Secure Shopping</a></li>
                                                <li><a href="#">Delivery infomation</a></li>
                                                <li><a href="#">Privacy Policy</a></li>
                                                <li><a href="#">Our Sitemap</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-6 col-xs-6">
                                        <div class="wrap-custom-menu vertical-menu-2">
                                            <ul class="menu">
                                                <li><a href="#">Who We Are</a></li>
                                                <li><a href="#">Our Services</a></li>
                                                <li><a href="#">Projects</a></li>
                                                <li><a href="#">Contacts Us</a></li>
                                                <li><a href="#">Innovation</a></li>
                                                <li><a href="#">Testimonials</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 md-margin-top-5px sm-margin-top-50px xs-margin-top-40px">
                            <section class="footer-item">
                                <h3 class="section-title">Transport Offices</h3>
                                <div class="contact-info-block footer-layout xs-padding-top-10px">
                                    <ul class="contact-lines">
                                        <li>
                                            <p class="info-item">
                                                <i class="biolife-icon icon-location"></i>
                                                <b class="desc">7563 St. Vicent Place, Glasgow, Greater Newyork NH7689, UK </b>
                                            </p>
                                        </li>
                                        <li>
                                            <p class="info-item">
                                                <i class="biolife-icon icon-phone"></i>
                                                <b class="desc">Phone: (+067) 234 789  (+068) 222 888</b>
                                            </p>
                                        </li>
                                        <li>
                                            <p class="info-item">
                                                <i class="biolife-icon icon-letter"></i>
                                                <b class="desc">Email:  contact@company.com</b>
                                            </p>
                                        </li>
                                        <li>
                                            <p class="info-item">
                                                <i class="biolife-icon icon-clock"></i>
                                                <b class="desc">Hours: 7 Days a week from 10:00 am</b>
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="biolife-social inline">
                                    <ul class="socials">
                                        <li><a href="#" title="twitter" class="socail-btn"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#" title="facebook" class="socail-btn"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#" title="pinterest" class="socail-btn"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                        <li><a href="#" title="youtube" class="socail-btn"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                        <li><a href="#" title="instagram" class="socail-btn"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="separator sm-margin-top-70px xs-margin-top-40px"></div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="copy-right-text"><p><a href="templateshub.net">Templates Hub</a></p></div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="payment-methods">
                                <ul>
                                    <li><a href="#" class="payment-link"><img src="user/assets/images/card1.jpg" width="51" height="36" alt=""></a></li>
                                    <li><a href="#" class="payment-link"><img src="user/assets/images/card2.jpg" width="51" height="36" alt=""></a></li>
                                    <li><a href="#" class="payment-link"><img src="user/assets/images/card3.jpg" width="51" height="36" alt=""></a></li>
                                    <li><a href="#" class="payment-link"><img src="user/assets/images/card4.jpg" width="51" height="36" alt=""></a></li>
                                    <li><a href="#" class="payment-link"><img src="user/assets/images/card5.jpg" width="51" height="36" alt=""></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <!--Footer For Mobile-->
        <div class="mobile-footer">
            <div class="mobile-footer-inner">
                <div class="mobile-block block-menu-main">
                    <a class="menu-bar menu-toggle btn-toggle" data-object="open-mobile-menu" href="javascript:void(0)">
                        <span class="fa fa-bars"></span>
                        <span class="text">Menu</span>
                    </a>
                </div>
                <div class="mobile-block block-sidebar">
                    <a class="menu-bar filter-toggle btn-toggle" data-object="open-mobile-filter" href="javascript:void(0)">
                        <i class="fa fa-sliders" aria-hidden="true"></i>
                        <span class="text">Sidebar</span>
                    </a>
                </div>
                <div class="mobile-block block-minicart">
                    <a class="link-to-cart" href="#">
                        <span class="fa fa-shopping-bag" aria-hidden="true"></span>
                        <span class="text">Cart</span>
                    </a>
                </div>
                <div class="mobile-block block-global">
                    <a class="menu-bar myaccount-toggle btn-toggle" data-object="global-panel-opened" href="javascript:void(0)">
                        <span class="fa fa-globe"></span>
                        <span class="text">Global</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="mobile-block-global">
            <div class="biolife-mobile-panels">
                <span class="biolife-current-panel-title">Global</span>
                <a class="biolife-close-btn" data-object="global-panel-opened" href="#">&times;</a>
            </div>
            <div class="block-global-contain">
                <div class="glb-item my-account">
                    <b class="title">My Account</b>
                    <ul class="list">
                        <li class="list-item"><a href="#">Login/register</a></li>
                        <li class="list-item"><a href="#">Wishlist <span class="index">(8)</span></a></li>
                        <li class="list-item"><a href="#">Checkout</a></li>
                    </ul>
                </div>
                <div class="glb-item currency">
                    <b class="title">Currency</b>
                    <ul class="list">
                        <li class="list-item"><a href="#">€ EUR (Euro)</a></li>
                        <li class="list-item"><a href="#">$ USD (Dollar)</a></li>
                        <li class="list-item"><a href="#">£ GBP (Pound)</a></li>
                        <li class="list-item"><a href="#">¥ JPY (Yen)</a></li>
                    </ul>
                </div>
                <div class="glb-item languages">
                    <b class="title">Language</b>
                    <ul class="list inline">
                        <li class="list-item"><a href="#"><img src="user/assets/images/languages/us.jpg" alt="flag" width="24" height="18"></a></li>
                        <li class="list-item"><a href="#"><img src="user/assets/images/languages/fr.jpg" alt="flag" width="24" height="18"></a></li>
                        <li class="list-item"><a href="#"><img src="user/assets/images/languages/ger.jpg" alt="flag" width="24" height="18"></a></li>
                        <li class="list-item"><a href="#"><img src="user/assets/images/languages/jap.jpg" alt="flag" width="24" height="18"></a></li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Scroll Top Button -->
        <a class="btn-scroll-top"><i class="biolife-icon icon-left-arrow"></i></a>

        <script src="user/assets/js/jquery-3.4.1.min.js"></script>
        <script src="user/assets/js/bootstrap.min.js"></script>
        <script src="user/assets/js/jquery.countdown.min.js"></script>
        <script src="user/assets/js/jquery.nice-select.min.js"></script>
        <script src="user/assets/js/jquery.nicescroll.min.js"></script>
        <script src="user/assets/js/slick.min.js"></script>
        <script src="user/assets/js/biolife.framework.js"></script>
        <script src="user/assets/js/functions.js"></script>
    </body>

</html>
