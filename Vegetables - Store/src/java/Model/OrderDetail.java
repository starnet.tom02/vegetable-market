/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author phanh
 */
public class OrderDetail {
    private int orderDetailID;
    private double price;
    private int quantity;
    private Order order;
    private Product product;

    public OrderDetail() {
    }

    public OrderDetail(int orderDetailID, double price, int quantity, Order order, Product product) {
        this.orderDetailID = orderDetailID;
        this.price = price;
        this.quantity = quantity;
        this.order = order;
        this.product = product;
    }

    public int getOrderDetailID() {
        return orderDetailID;
    }

    public void setOrderDetailID(int orderDetailID) {
        this.orderDetailID = orderDetailID;
    }

    

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    
    public double getTotalMoney(){
        return price * quantity;
    }
}
