/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Date;

/**
 *
 * @author phanh
 */
public class ProductReview {
    private int reviewID;
    private String content;
    private int rate;
    private int like;
    private int dislike;
    private Date reviewTimeCreate;
    private Product product;
    private Account account;

    public ProductReview() {
    }

    public ProductReview(int reviewID, String content, int rate, int like, int dislike, Date reviewTimeCreate, Product product, Account account) {
        this.reviewID = reviewID;
        this.content = content;
        this.rate = rate;
        this.like = like;
        this.dislike = dislike;
        this.reviewTimeCreate = reviewTimeCreate;
        this.product = product;
        this.account = account;
    }

    public int getReviewID() {
        return reviewID;
    }

    public void setReviewID(int reviewID) {
        this.reviewID = reviewID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public int getDislike() {
        return dislike;
    }

    public void setDislike(int dislike) {
        this.dislike = dislike;
    }

    public Date getReviewTimeCreate() {
        return reviewTimeCreate;
    }

    public void setReviewTimeCreate(Date reviewTimeCreate) {
        this.reviewTimeCreate = reviewTimeCreate;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
    
    
}
