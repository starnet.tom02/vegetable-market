/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Date;

/**
 *
 * @author phanh
 */
public class Order {
    private int orderID;
    private String status;
    private double totalMoney;
    private String note;
    private Date orderTime;
    private String firstName;
    private String lastName;
    private String address;
    private String email;
    private String phone;
    private Account account;
    private CompanyShip companyShip;

    public Order() {
    }

    public Order(int orderID, String status, double totalMoney, String note, Date orderTime, String firstName, String lastName, String address, String email, String phone, Account account, CompanyShip companyShip) {
        this.orderID = orderID;
        this.status = status;
        this.totalMoney = totalMoney;
        this.note = note;
        this.orderTime = orderTime;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.email = email;
        this.phone = phone;
        this.account = account;
        this.companyShip = companyShip;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public CompanyShip getCompanyShip() {
        return companyShip;
    }

    public void setCompanyShip(CompanyShip companyShip) {
        this.companyShip = companyShip;
    }
    
    public String getFullName(){
        return firstName + " " + lastName;
    }
    
}
