/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author phanh
 */
public class CompanyShip {
    private int companyShipID;
    private String companyShipName;

    public CompanyShip() {
    }

    public CompanyShip(int companyShipID, String companyShipName) {
        this.companyShipID = companyShipID;
        this.companyShipName = companyShipName;
    }

    public int getCompanyShipID() {
        return companyShipID;
    }

    public void setCompanyShipID(int companyShipID) {
        this.companyShipID = companyShipID;
    }

    public String getCompanyShipName() {
        return companyShipName;
    }

    public void setCompanyShipName(String companyShipName) {
        this.companyShipName = companyShipName;
    }
    
    
}
