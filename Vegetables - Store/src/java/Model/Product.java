/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Date;

/**
 *
 * @author phanh
 */
public class Product {
    private int productID;
    private String productName;
    private String describe;
    private String preservation;
    private String productTitle;
    private String makerName;
    private double price;
    private double discount;
    private int quantity;   
    private int seller;
    private String weight;
    private String status;
    private Date createTime;
    private Date updateTime;
    private Category category;
    private ProductImage image;

    public Product() {
    }

    public Product(int productID, String productName, String describe, String preservation, String productTitle, String makerName, double price, double discount, int quantity, int seller, String weight, String status, Date createTime, Date updateTime, Category category, ProductImage image) {
        this.productID = productID;
        this.productName = productName;
        this.describe = describe;
        this.preservation = preservation;
        this.productTitle = productTitle;
        this.makerName = makerName;
        this.price = price;
        this.discount = discount;
        this.quantity = quantity;
        this.seller = seller;
        this.weight = weight;
        this.status = status;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.category = category;
        this.image = image;
    }

    

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getPreservation() {
        return preservation;
    }

    public void setPreservation(String preservation) {
        this.preservation = preservation;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getMakerName() {
        return makerName;
    }

    public void setMakerName(String makerName) {
        this.makerName = makerName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getSeller() {
        return seller;
    }

    public void setSeller(int seller) {
        this.seller = seller;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public ProductImage getImage() {
        return image;
    }

    public void setImage(ProductImage image) {
        this.image = image;
    }
    
    public double getLastPrice(){
        return price - discount;
    }
    
    
    
}
