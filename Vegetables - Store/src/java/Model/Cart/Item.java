/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model.Cart;

import Model.Product;

/**
 *
 * @author phanh
 */
public class Item {
    private Product product;
    private double unitPrice;
    private int quantity;

    public Item() {
    }

    public Item(Product product, double unitPrice, int quantity) {
        this.product = product;
        this.unitPrice = unitPrice;
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public double getTotalMoney(){
        return ( unitPrice  * quantity )  ;
    }
    
//    public String getUnitPriceFormat() {
//        Locale localeVN = new Locale("vi", "VN");
//        NumberFormat vn = NumberFormat.getInstance(localeVN);
//        return vn.format(unitPrice);
//    }
//    
//    public String getDiscountFormat() {
//        Locale localeVN = new Locale("vi", "VN");
//        NumberFormat vn = NumberFormat.getInstance(localeVN);
//        return vn.format(discount);
//    }
//    
//    public String getTotalMoneyFormat() {
//        Locale localeVN = new Locale("vi", "VN");
//        NumberFormat vn = NumberFormat.getInstance(localeVN);
//        return vn.format(getTotalMoney());
//    }
//    
//    public double getSubtotal(){
//        return unitPrice * quantity;
//    }
//
//    public double getTax(){
//        return 0.08;
//    }
//    
//    public String getTaxPercent() {
//        
//        DecimalFormat f = new DecimalFormat("###.#%");
//        return f.format(getTax());
//        
//    }
}
