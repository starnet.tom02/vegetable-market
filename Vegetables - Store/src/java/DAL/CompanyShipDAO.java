/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.CompanyShip;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author phanh
 */
public class CompanyShipDAO extends DBContext {

    public List<CompanyShip> getAll() {
        try {
            String sql = "SELECT [companyShipID]\n"
                    + "      ,[companyShipName]\n"
                    + "  FROM [project_duc].[dbo].[CompanyShips]";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            
            List<CompanyShip> list = new ArrayList<>();
            
            while (rs.next()) {                
                CompanyShip cs = new CompanyShip(rs.getInt(1), rs.getString(2));
                list.add(cs);
            }
            return list;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }
}
