/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.Account;
import Model.Cart.Cart;
import Model.Cart.Item;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author phanh
 */
public class CartDAO extends DBContext {

    public void addOrder(Account a, Cart cart, String note, int shippingID) {
        try {
            String sql = "INSERT INTO [dbo].[Orders] ([status],[totalMoney],[note],[orderTime],[firstName],[lastName]\n"
                    + "                              ,[addressShipping],[email],[phone],[userName],[shippingID])\n"
                    + "     VALUES	(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "Chờ Xử Lý");
            st.setDouble(2, cart.getTotalMoney());
            st.setString(3, note);
            st.setDate(4, Common.getCurrentDate());
            st.setString(5, a.getFirstName());
            st.setString(6, a.getLastName());
            st.setString(7, a.getAddress());
            st.setString(8, a.getEmail());
            st.setString(9, a.getPhone());
            st.setString(10, a.getUserName());
            st.setInt(11, shippingID);
            st.executeUpdate();

            String sql1 = "SELECT TOP (1) [orderID] FROM [project_duc].[dbo].[Orders] ORDER BY [orderID] DESC";

            PreparedStatement st1 = connection.prepareStatement(sql1);
            ResultSet rs = st1.executeQuery();

            if (rs.next()) {
                int orderID = rs.getInt(1);
                for (Item item : cart.getItems()) {
                    String sql2 = "INSERT INTO [dbo].[OrderDetails]\n"
                            + "           ([unitPrice]\n"
                            + "           ,[quantity]\n"
                            + "           ,[orderID]\n"
                            + "           ,[productID])\n"
                            + "     VALUES\n"
                            + "           ( ?, ?, ?, ?)";

                    PreparedStatement st2 = connection.prepareStatement(sql2);
                    st2.setDouble(1, item.getUnitPrice());
                    st2.setInt(2, item.getQuantity());
                    st2.setInt(3, orderID);
                    st2.setInt(4, item.getProduct().getProductID());
                    st2.executeUpdate();
                }
            }

            for (Item item : cart.getItems()) {
                String sql3 = "UPDATE [dbo].[Products] SET productQuantity = productQuantity - ? , productSeller = productSeller + ? WHERE productID = ?";
                PreparedStatement st3 = connection.prepareStatement(sql3);
                st3.setInt(1, item.getQuantity());
                st3.setInt(2, item.getQuantity());
                st3.setInt(3, item.getProduct().getProductID());
                st3.executeUpdate();
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
    }
}
