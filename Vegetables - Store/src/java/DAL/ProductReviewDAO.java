/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.Account;
import Model.Category;
import Model.Country;
import Model.Product;
import Model.ProductImage;
import Model.ProductReview;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class ProductReviewDAO extends DBContext {

    public void insert(String content, int rate, int productID, String userName) {
        try {

            String sql = "INSERT INTO [dbo].[ProductReviews]\n"
                    + "           ([reviewContent]\n"
                    + "           ,[reviewRate]\n"
                    + "           ,[reviewLike]\n"
                    + "           ,[reviewDislike]\n"
                    + "           ,[reviewTimeCreate]\n"
                    + "           ,[productID]\n"
                    + "           ,[userName])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, content);
            st.setInt(2, rate);
            st.setInt(3, 0);
            st.setInt(4, 0);
            st.setDate(5, Common.getCurrentDate());
            st.setInt(6, productID);
            st.setString(7, userName);

            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public List<ProductReview> getAll() {
        try {
            String sql = "SELECT pr.[reviewID]\n"
                    + "      ,pr.[reviewContent]\n"
                    + "      ,pr.[reviewRate]\n"
                    + "      ,pr.[reviewLike]\n"
                    + "      ,pr.[reviewDislike]\n"
                    + "      ,pr.[reviewTimeCreate]\n"
                    + "      ,pr.[productID]\n"
                    + "      ,pr.[userName]\n"
                    + "	     ,p.[productName]\n"
                    + "      ,p.[productDescribe]\n"
                    + "      ,p.[productPreservation]\n"
                    + "      ,p.[productTitle]\n"
                    + "      ,p.[makerName]\n"
                    + "      ,p.[productPrice]\n"
                    + "      ,p.[productDiscount]\n"
                    + "      ,p.[productQuantity]\n"
                    + "      ,p.[productSeller]\n"
                    + "      ,p.[productWeight]\n"
                    + "      ,p.[productStatus]\n"
                    + "      ,p.[productTimeCreate]\n"
                    + "      ,p.[productTimeUpdate]\n"
                    + "      ,p.[categoryID]\n"
                    + "      ,c.[categoryName]\n"
                    + "      ,c.[describe]\n"
                    + "      ,c.[cid]\n"
                    + "      ,[pi].[image1]\n"
                    + "      ,[pi].[image2]\n"
                    + "      ,[pi].[image3]\n"
                    + "      ,[pi].[image4]\n"
                    + "      ,[pi].[image5]\n"
                    + "      ,a.[password]\n"
                    + "      ,a.[firstName] \n"
                    + "      ,a.[lastName] \n"
                    + "      ,a.[phone] \n"
                    + "      ,a.[email] \n"
                    + "      ,a.[gender]\n"
                    + "      ,a.[address] \n"
                    + "      ,a.[avatar]\n"
                    + "      ,a.[birthDay]\n"
                    + "      ,a.[countryID]\n"
                    + "      ,a.[role]\n"
                    + "      ,ct.[countryName]\n"
                    + "      ,ct.[describe]\n"
                    + "      ,ct.[location]\n"
                    + "  FROM [project_duc].[dbo].[ProductReviews] pr inner join [project_duc].[dbo].Accounts a\n"
                    + "  ON pr.userName = a.userName  inner join [project_duc].[dbo].Products p\n"
                    + "     ON pr.productID = p.productID inner join [project_duc].[dbo].[Categories] c\n"
                    + "     ON p.categoryID = c.categoryID inner join [project_duc].[dbo].ProductImages [pi]\n"
                    + "     ON p.productID = [pi].productID inner join [project_duc].[dbo].Countries ct\n"
                    + "     on a.countryID = ct.countryID";
            
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            List<ProductReview> list = new ArrayList<>();
            
            while(rs.next()){
                Category c = new Category(rs.getInt("categoryID"), rs.getString("categoryName"), rs.getString("describe"), rs.getInt("cid"));

                ProductImage pi = new ProductImage(rs.getInt("productID"), rs.getString("image1"), rs.getString("image2"), rs.getString("image3"),
                        rs.getString("image4"), rs.getString("image5"));

                Product p = new Product(rs.getInt("productID"), rs.getString("productName"), rs.getString("productDescribe"), rs.getString("productPreservation"),
                        rs.getString("productTitle"), rs.getString("makerName"), rs.getDouble("productPrice"), rs.getDouble("productDiscount"), rs.getInt("productQuantity"),
                        rs.getInt("productSeller"), rs.getString("productWeight"), rs.getString("productStatus"), rs.getDate("productTimeCreate"), rs.getDate("productTimeUpdate"),
                        c, pi);
                
                Country country = new Country(rs.getInt("countryID"), rs.getString("countryName"), rs.getString("describe"), rs.getString("location"));
                Account a =  new Account(rs.getString("userName"), rs.getString("password"), rs.getString("firstName"),
                        rs.getString("lastName"), rs.getString("phone"), rs.getString("email"), rs.getInt("gender"), rs.getString("address"),
                        rs.getString("avatar"), rs.getDate("birthDay"), rs.getInt("role"), country);

                ProductReview pr = new ProductReview(rs.getInt("reviewID"), rs.getString("reviewContent"), rs.getInt("reviewRate"), rs.getInt("reviewLike")
                        , rs.getInt("reviewDislike"), rs.getDate("reviewTimeCreate"), p, a);
                list.add(pr);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
    
    public List<ProductReview> getReviewByProductID(int productID) {
        try {
            String sql = "SELECT pr.[reviewID]\n"
                    + "      ,pr.[reviewContent]\n"
                    + "      ,pr.[reviewRate]\n"
                    + "      ,pr.[reviewLike]\n"
                    + "      ,pr.[reviewDislike]\n"
                    + "      ,pr.[reviewTimeCreate]\n"
                    + "      ,pr.[productID]\n"
                    + "      ,pr.[userName]\n"
                    + "	     ,p.[productName]\n"
                    + "      ,p.[productDescribe]\n"
                    + "      ,p.[productPreservation]\n"
                    + "      ,p.[productTitle]\n"
                    + "      ,p.[makerName]\n"
                    + "      ,p.[productPrice]\n"
                    + "      ,p.[productDiscount]\n"
                    + "      ,p.[productQuantity]\n"
                    + "      ,p.[productSeller]\n"
                    + "      ,p.[productWeight]\n"
                    + "      ,p.[productStatus]\n"
                    + "      ,p.[productTimeCreate]\n"
                    + "      ,p.[productTimeUpdate]\n"
                    + "      ,p.[categoryID]\n"
                    + "      ,c.[categoryName]\n"
                    + "      ,c.[describe]\n"
                    + "      ,c.[cid]\n"
                    + "      ,[pi].[image1]\n"
                    + "      ,[pi].[image2]\n"
                    + "      ,[pi].[image3]\n"
                    + "      ,[pi].[image4]\n"
                    + "      ,[pi].[image5]\n"
                    + "      ,a.[password]\n"
                    + "      ,a.[firstName] \n"
                    + "      ,a.[lastName] \n"
                    + "      ,a.[phone] \n"
                    + "      ,a.[email] \n"
                    + "      ,a.[gender]\n"
                    + "      ,a.[address] \n"
                    + "      ,a.[avatar]\n"
                    + "      ,a.[birthDay]\n"
                    + "      ,a.[countryID]\n"
                    + "      ,a.[role]\n"
                    + "      ,ct.[countryName]\n"
                    + "      ,ct.[describe]\n"
                    + "      ,ct.[location]\n"
                    + "  FROM [project_duc].[dbo].[ProductReviews] pr inner join [project_duc].[dbo].Accounts a\n"
                    + "  ON pr.userName = a.userName  inner join [project_duc].[dbo].Products p\n"
                    + "     ON pr.productID = p.productID inner join [project_duc].[dbo].[Categories] c\n"
                    + "     ON p.categoryID = c.categoryID inner join [project_duc].[dbo].ProductImages [pi]\n"
                    + "     ON p.productID = [pi].productID inner join [project_duc].[dbo].Countries ct\n"
                    + "     on a.countryID = ct.countryID\n"
                    + " WHERE pr.productID = ?";
            
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, productID);
            ResultSet rs = st.executeQuery();
            List<ProductReview> list = new ArrayList<>();
            
            while(rs.next()){
                Category c = new Category(rs.getInt("categoryID"), rs.getString("categoryName"), rs.getString("describe"), rs.getInt("cid"));

                ProductImage pi = new ProductImage(rs.getInt("productID"), rs.getString("image1"), rs.getString("image2"), rs.getString("image3"),
                        rs.getString("image4"), rs.getString("image5"));

                Product p = new Product(rs.getInt("productID"), rs.getString("productName"), rs.getString("productDescribe"), rs.getString("productPreservation"),
                        rs.getString("productTitle"), rs.getString("makerName"), rs.getDouble("productPrice"), rs.getDouble("productDiscount"), rs.getInt("productQuantity"),
                        rs.getInt("productSeller"), rs.getString("productWeight"), rs.getString("productStatus"), rs.getDate("productTimeCreate"), rs.getDate("productTimeUpdate"),
                        c, pi);
                
                Country country = new Country(rs.getInt("countryID"), rs.getString("countryName"), rs.getString("describe"), rs.getString("location"));
                Account a =  new Account(rs.getString("userName"), rs.getString("password"), rs.getString("firstName"),
                        rs.getString("lastName"), rs.getString("phone"), rs.getString("email"), rs.getInt("gender"), rs.getString("address"),
                        rs.getString("avatar"), rs.getDate("birthDay"), rs.getInt("role"), country);

                ProductReview pr = new ProductReview(rs.getInt("reviewID"), rs.getString("reviewContent"), rs.getInt("reviewRate"), rs.getInt("reviewLike")
                        , rs.getInt("reviewDislike"), rs.getDate("reviewTimeCreate"), p, a);
                list.add(pr);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
    
    public double getAverageRateByProductID(int productID){
        List<ProductReview> list = getReviewByProductID(productID);
        int total = 0;
        double result = 0;
        for (ProductReview item : list) {
            total += item.getRate();
        }
        
        if(!list.isEmpty()){
            result = total / (list.size() * 1.0);
        }
        return result;
    }
    
    public int getTotalStart(int productID, int start){
        List<ProductReview> list = getReviewByProductID(productID);
        int total = 0;
        for (ProductReview item : list) {
            if(item.getRate() == start){
                total++;
            }
        }
        
        return total;
    }
    
    public String getPercentStart(int productID, int start){
        List<ProductReview> list = getReviewByProductID(productID);
        int total = 0;
        for (ProductReview item : list) {
            if(item.getRate() == start){
                total++;
            }
        }

        String rs = "width-" + (int)( (total / (list.size() * 1.0) ) * 100) + "percent";
        return rs;
    }
    
    public List<ProductReview> getTop5ReviewNew() {
        try {
            String sql = "SELECT TOP(4) pr.[reviewID]\n"
                    + "      ,pr.[reviewContent]\n"
                    + "      ,pr.[reviewRate]\n"
                    + "      ,pr.[reviewLike]\n"
                    + "      ,pr.[reviewDislike]\n"
                    + "      ,pr.[reviewTimeCreate]\n"
                    + "      ,pr.[productID]\n"
                    + "      ,pr.[userName]\n"
                    + "	     ,p.[productName]\n"
                    + "      ,p.[productDescribe]\n"
                    + "      ,p.[productPreservation]\n"
                    + "      ,p.[productTitle]\n"
                    + "      ,p.[makerName]\n"
                    + "      ,p.[productPrice]\n"
                    + "      ,p.[productDiscount]\n"
                    + "      ,p.[productQuantity]\n"
                    + "      ,p.[productSeller]\n"
                    + "      ,p.[productWeight]\n"
                    + "      ,p.[productStatus]\n"
                    + "      ,p.[productTimeCreate]\n"
                    + "      ,p.[productTimeUpdate]\n"
                    + "      ,p.[categoryID]\n"
                    + "      ,c.[categoryName]\n"
                    + "      ,c.[describe]\n"
                    + "      ,c.[cid]\n"
                    + "      ,[pi].[image1]\n"
                    + "      ,[pi].[image2]\n"
                    + "      ,[pi].[image3]\n"
                    + "      ,[pi].[image4]\n"
                    + "      ,[pi].[image5]\n"
                    + "      ,a.[password]\n"
                    + "      ,a.[firstName] \n"
                    + "      ,a.[lastName] \n"
                    + "      ,a.[phone] \n"
                    + "      ,a.[email] \n"
                    + "      ,a.[gender]\n"
                    + "      ,a.[address] \n"
                    + "      ,a.[avatar]\n"
                    + "      ,a.[birthDay]\n"
                    + "      ,a.[countryID]\n"
                    + "      ,a.[role]\n"
                    + "      ,ct.[countryName]\n"
                    + "      ,ct.[describe]\n"
                    + "      ,ct.[location]\n"
                    + "  FROM [project_duc].[dbo].[ProductReviews] pr inner join [project_duc].[dbo].Accounts a\n"
                    + "  ON pr.userName = a.userName  inner join [project_duc].[dbo].Products p\n"
                    + "     ON pr.productID = p.productID inner join [project_duc].[dbo].[Categories] c\n"
                    + "     ON p.categoryID = c.categoryID inner join [project_duc].[dbo].ProductImages [pi]\n"
                    + "     ON p.productID = [pi].productID inner join [project_duc].[dbo].Countries ct\n"
                    + "     on a.countryID = ct.countryID"
                    + " ORDER BY pr.reviewTimeCreate DESC";
            
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            List<ProductReview> list = new ArrayList<>();
            
            while(rs.next()){
                Category c = new Category(rs.getInt("categoryID"), rs.getString("categoryName"), rs.getString("describe"), rs.getInt("cid"));

                ProductImage pi = new ProductImage(rs.getInt("productID"), rs.getString("image1"), rs.getString("image2"), rs.getString("image3"),
                        rs.getString("image4"), rs.getString("image5"));

                Product p = new Product(rs.getInt("productID"), rs.getString("productName"), rs.getString("productDescribe"), rs.getString("productPreservation"),
                        rs.getString("productTitle"), rs.getString("makerName"), rs.getDouble("productPrice"), rs.getDouble("productDiscount"), rs.getInt("productQuantity"),
                        rs.getInt("productSeller"), rs.getString("productWeight"), rs.getString("productStatus"), rs.getDate("productTimeCreate"), rs.getDate("productTimeUpdate"),
                        c, pi);
                
                Country country = new Country(rs.getInt("countryID"), rs.getString("countryName"), rs.getString("describe"), rs.getString("location"));
                Account a =  new Account(rs.getString("userName"), rs.getString("password"), rs.getString("firstName"),
                        rs.getString("lastName"), rs.getString("phone"), rs.getString("email"), rs.getInt("gender"), rs.getString("address"),
                        rs.getString("avatar"), rs.getDate("birthDay"), rs.getInt("role"), country);

                ProductReview pr = new ProductReview(rs.getInt("reviewID"), rs.getString("reviewContent"), rs.getInt("reviewRate"), rs.getInt("reviewLike")
                        , rs.getInt("reviewDislike"), rs.getDate("reviewTimeCreate"), p, a);
                list.add(pr);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
}
