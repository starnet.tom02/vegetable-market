 /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.Account;
import Model.Category;
import Model.CompanyShip;
import Model.Country;
import Model.Order;
import Model.OrderDetail;
import Model.Product;
import Model.ProductImage;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author phanh
 */
public class OrderDAO extends DBContext {

    public List<Order> getAll() {
        try {

            String sql = "SELECT o.[orderID]\n"
                    + "      ,o.[status]\n"
                    + "      ,o.[totalMoney]\n"
                    + "      ,o.[note]\n"
                    + "      ,o.[orderTime]\n"
                    + "      ,o.[firstName]\n"
                    + "      ,o.[lastName]\n"
                    + "      ,o.[addressShipping]\n"
                    + "      ,o.[email]\n"
                    + "      ,o.[phone]\n"
                    + "      ,o.[userName]\n"
                    + "      ,o.[shippingID]\n"
                    + "      ,a.[password]\n"
                    + "      ,a.[firstName]\n"
                    + "      ,a.[lastName]\n"
                    + "      ,a.[phone]\n"
                    + "      ,a.[email]\n"
                    + "      ,a.[gender]\n"
                    + "      ,a.[address]\n"
                    + "      ,a.[avatar]\n"
                    + "      ,a.[birthDay]\n"
                    + "      ,a.[countryID]\n"
                    + "      ,a.[role]\n"
                    + "      ,cs.[companyShipName]\n"
                    + "      ,c.[countryName]\n"
                    + "      ,c.[describe]\n"
                    + "      ,c.[location]\n"
                    + "  FROM [project_duc].[dbo].[Orders] o inner join [project_duc].[dbo].Accounts a\n"
                    + "  on o.userName = a.userName inner join [project_duc].[dbo].CompanyShips cs\n"
                    + "  on o.shippingID = cs.companyShipID inner join [project_duc].[dbo].Countries c\n"
                    + "  on a.countryID = c.countryID";

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            List<Order> list = new ArrayList<>();

            while (rs.next()) {
                Country c = new Country(rs.getInt("countryID"), rs.getString("countryName"), rs.getString("describe"), rs.getString("location"));

                Account a = new Account(rs.getString("userName"), rs.getString("password"), rs.getString("firstName"), rs.getString("lastName"),
                        rs.getString("phone"), rs.getString("email"), rs.getInt("gender"), rs.getString("address"), rs.getString("avatar"),
                        rs.getDate("birthDay"), rs.getInt("role"), c);
                CompanyShip s = new CompanyShip(0, sql);

                Order o = new Order(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getString(4), rs.getDate(5), rs.getString(6),
                        rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), a, s);
                list.add(o);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Order> getTop5OrderNew() {
        try {

            String sql = "SELECT TOP(5) o.[orderID]\n"
                    + "      ,o.[status]\n"
                    + "      ,o.[totalMoney]\n"
                    + "      ,o.[note]\n"
                    + "      ,o.[orderTime]\n"
                    + "      ,o.[firstName]\n"
                    + "      ,o.[lastName]\n"
                    + "      ,o.[addressShipping]\n"
                    + "      ,o.[email]\n"
                    + "      ,o.[phone]\n"
                    + "      ,o.[userName]\n"
                    + "      ,o.[shippingID]\n"
                    + "      ,a.[password]\n"
                    + "      ,a.[firstName]\n"
                    + "      ,a.[lastName]\n"
                    + "      ,a.[phone]\n"
                    + "      ,a.[email]\n"
                    + "      ,a.[gender]\n"
                    + "      ,a.[address]\n"
                    + "      ,a.[avatar]\n"
                    + "      ,a.[birthDay]\n"
                    + "      ,a.[countryID]\n"
                    + "      ,a.[role]\n"
                    + "      ,cs.[companyShipName]\n"
                    + "      ,c.[countryName]\n"
                    + "      ,c.[describe]\n"
                    + "      ,c.[location]\n"
                    + "  FROM [project_duc].[dbo].[Orders] o inner join [project_duc].[dbo].Accounts a\n"
                    + "  on o.userName = a.userName inner join [project_duc].[dbo].CompanyShips cs\n"
                    + "  on o.shippingID = cs.companyShipID inner join [project_duc].[dbo].Countries c\n"
                    + "  on a.countryID = c.countryID"
                    + "  ORDER BY o.orderTime DESC";

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            List<Order> list = new ArrayList<>();

            while (rs.next()) {
                Country c = new Country(rs.getInt("countryID"), rs.getString("countryName"), rs.getString("describe"), rs.getString("location"));

                Account a = new Account(rs.getString("userName"), rs.getString("password"), rs.getString("firstName"), rs.getString("lastName"),
                        rs.getString("phone"), rs.getString("email"), rs.getInt("gender"), rs.getString("address"), rs.getString("avatar"),
                        rs.getDate("birthDay"), rs.getInt("role"), c);
                CompanyShip s = new CompanyShip(0, sql);

                Order o = new Order(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getString(4), rs.getDate(5), rs.getString(6),
                        rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), a, s);
                list.add(o);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public Order getOrderByOrderID(int orderID) {
        try {

            String sql = "SELECT o.[orderID]\n"
                    + "      ,o.[status]\n"
                    + "      ,o.[totalMoney]\n"
                    + "      ,o.[note]\n"
                    + "      ,o.[orderTime]\n"
                    + "      ,o.[firstName]\n"
                    + "      ,o.[lastName]\n"
                    + "      ,o.[addressShipping]\n"
                    + "      ,o.[email]\n"
                    + "      ,o.[phone]\n"
                    + "      ,o.[userName]\n"
                    + "      ,o.[shippingID]\n"
                    + "      ,a.[password]\n"
                    + "      ,a.[firstName] as 'aFirstName'\n"
                    + "      ,a.[lastName] as 'aLastName'\n"
                    + "      ,a.[phone] as 'aPhone'\n"
                    + "      ,a.[email] as 'aEmail'\n"
                    + "      ,a.[gender]\n"
                    + "      ,a.[address] \n"
                    + "      ,a.[avatar]\n"
                    + "      ,a.[birthDay]\n"
                    + "      ,a.[countryID]\n"
                    + "      ,a.[role]\n"
                    + "      ,cs.[companyShipName]\n"
                    + "      ,c.[countryName]\n"
                    + "      ,c.[describe]\n"
                    + "      ,c.[location]\n"
                    + "  FROM [project_duc].[dbo].[Orders] o inner join [project_duc].[dbo].Accounts a\n"
                    + "  on o.userName = a.userName inner join [project_duc].[dbo].CompanyShips cs\n"
                    + "  on o.shippingID = cs.companyShipID inner join [project_duc].[dbo].Countries c\n"
                    + "  on a.countryID = c.countryID\n"
                    + "  WHERE o.orderID = ?";

            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, orderID);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                Country c = new Country(rs.getInt("countryID"), rs.getString("countryName"), rs.getString("describe"), rs.getString("location"));

                Account a = new Account(rs.getString("userName"), rs.getString("password"), rs.getString("aFirstName"), rs.getString("aLastName"),
                        rs.getString("aPhone"), rs.getString("aEmail"), rs.getInt("gender"), rs.getString("address"), rs.getString("avatar"),
                        rs.getDate("birthDay"), rs.getInt("role"), c);
                CompanyShip s = new CompanyShip(rs.getInt("shippingID"), rs.getString("companyShipName"));

                Order o = new Order(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getString(4), rs.getDate(5), rs.getString(6),
                        rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), a, s);
                return o;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<OrderDetail> getOrderDetailByOrderID(int orderID) {
        try {

            String sql = "SELECT od.[orderDetailID]\n"
                    + "      ,od.[unitPrice]\n"
                    + "      ,od.[quantity]\n"
                    + "      ,od.[orderID]\n"
                    + "      ,od.[productID]\n"
                    + "      ,p.[productName]\n"
                    + "      ,p.[productDescribe]\n"
                    + "      ,p.[productPreservation]\n"
                    + "      ,p.[productTitle]\n"
                    + "      ,p.[makerName]\n"
                    + "      ,p.[productPrice]\n"
                    + "      ,p.[productDiscount]\n"
                    + "      ,p.[productQuantity]\n"
                    + "      ,p.[productSeller]\n"
                    + "      ,p.[productWeight]\n"
                    + "      ,p.[productStatus]\n"
                    + "      ,p.[productTimeCreate]\n"
                    + "      ,p.[productTimeUpdate]\n"
                    + "      ,p.[categoryID]\n"
                    + "      ,c.[categoryName]\n"
                    + "      ,c.[describe]\n"
                    + "      ,c.[cid]\n"
                    + "      ,[pi].[image1]\n"
                    + "      ,[pi].[image2]\n"
                    + "      ,[pi].[image3]\n"
                    + "      ,[pi].[image4]\n"
                    + "      ,[pi].[image5]\n"
                    + "      ,o.[status]\n"
                    + "      ,o.[totalMoney]\n"
                    + "      ,o.[note]\n"
                    + "      ,o.[orderTime]\n"
                    + "      ,o.[firstName]\n"
                    + "      ,o.[lastName]\n"
                    + "      ,o.[addressShipping]\n"
                    + "      ,o.[email]\n"
                    + "      ,o.[phone]\n"
                    + "      ,o.[userName]\n"
                    + "      ,o.[shippingID]\n"
                    + "      ,a.[password]\n"
                    + "      ,a.[firstName] as 'aFirstName'\n"
                    + "      ,a.[lastName] as 'aLastName'\n"
                    + "      ,a.[phone] as 'aPhone'\n"
                    + "      ,a.[email] as 'aEmail'\n"
                    + "      ,a.[gender]\n"
                    + "      ,a.[address] \n"
                    + "      ,a.[avatar]\n"
                    + "      ,a.[birthDay]\n"
                    + "      ,a.[countryID]\n"
                    + "      ,a.[role]\n"
                    + "      ,ct.[countryName]\n"
                    + "      ,ct.[describe]\n"
                    + "      ,ct.[location]\n"
                    + "      ,cs.[companyShipName]\n"
                    + "  FROM [project_duc].[dbo].[OrderDetails] od inner join [project_duc].[dbo].Products p\n"
                    + "     ON od.productID = p.productID inner join [project_duc].[dbo].[Categories] c\n"
                    + "     ON p.categoryID = c.categoryID inner join [project_duc].[dbo].ProductImages [pi]\n"
                    + "     ON p.productID = [pi].productID inner join [project_duc].[dbo].[Orders] o\n"
                    + "     ON od.orderID = o.orderID inner join [project_duc].[dbo].Accounts a\n"
                    + "     on o.userName = a.userName inner join [project_duc].[dbo].CompanyShips cs\n"
                    + "     on o.shippingID = cs.companyShipID inner join [project_duc].[dbo].Countries ct\n"
                    + "     on a.countryID = ct.countryID\n"
                    + "  WHERE od.orderID = ? ";

            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, orderID);
            ResultSet rs = st.executeQuery();
            List<OrderDetail> list = new ArrayList<>();
            while (rs.next()) {
                Country country = new Country(rs.getInt("countryID"), rs.getString("countryName"), rs.getString("describe"), rs.getString("location"));

                Account a = new Account(rs.getString("userName"), rs.getString("password"), rs.getString("aFirstName"), rs.getString("aLastName"),
                        rs.getString("aPhone"), rs.getString("aEmail"), rs.getInt("gender"), rs.getString("address"), rs.getString("avatar"),
                        rs.getDate("birthDay"), rs.getInt("role"), country);
                CompanyShip s = new CompanyShip(rs.getInt("shippingID"), rs.getString("companyShipName"));

                Order o = new Order(rs.getInt("orderID"), rs.getString("status"), rs.getDouble("totalMoney"), rs.getString("note"),
                         rs.getDate("orderTime"), rs.getString("firstName"), rs.getString("lastName"), rs.getString("addressShipping"),
                         rs.getString("email"), rs.getString("phone"), a, s);

                Category c = new Category(rs.getInt("categoryID"), rs.getString("categoryName"), rs.getString("describe"), rs.getInt("cid"));

                ProductImage pi = new ProductImage(rs.getInt("productID"), rs.getString("image1"), rs.getString("image2"), rs.getString("image3"),
                        rs.getString("image4"), rs.getString("image5"));

                Product p = new Product(rs.getInt("productID"), rs.getString("productName"), rs.getString("productDescribe"), rs.getString("productPreservation"),
                        rs.getString("productTitle"), rs.getString("makerName"), rs.getDouble("productPrice"), rs.getDouble("productDiscount"), rs.getInt("productQuantity"),
                        rs.getInt("productSeller"), rs.getString("productWeight"), rs.getString("productStatus"), rs.getDate("productTimeCreate"), rs.getDate("productTimeUpdate"),
                        c, pi);

                OrderDetail od = new OrderDetail(rs.getInt("orderDetailID"), rs.getDouble("unitPrice"), rs.getInt("quantity"), o, p);
                list.add(od);
            }
            return list;

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    
    public int getTotalOrder() {
        try {
            String sql = "SELECT COUNT(orderID)\n"
                    + "  FROM [project_duc].[dbo].Orders";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }
    
    public double getRevenueOrder() {
        try {
            String sql = "SELECT SUM([totalMoney]) \n" +
                        "  FROM [project_duc].[dbo].[Orders]";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                return rs.getDouble(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

}
