/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.Category;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author phanh
 */
public class CategoryDAO extends DBContext {

    public List<Category> getAllParentCategory() {
        try {
            String sql = "SELECT [categoryID]\n"
                    + "       ,[categoryName]\n"
                    + "       ,[describe]\n"
                    + "       ,[cid]\n"
                    + "FROM [project_duc].[dbo].[Categories]\n"
                    + "WHERE cid IS NULL";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            List<Category> list = new ArrayList<>();

            while (rs.next()) {
                Category c = new Category(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
                list.add(c);
            }

            return list;

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public Category getCategoryByProductID(int productID) {
        try {
            String sql = "SELECT c.[categoryID]"
                    + "         ,c.[categoryName]\n"
                    + "         ,c.[describe]\n"
                    + "         ,c.[cid]\n"
                    + "  FROM [project_duc].[dbo].[Products] p inner join [project_duc].[dbo].[Categories] c\n"
                    + "  ON p.categoryID = c.categoryID"
                    + " WHERE p.productID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, productID);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                Category c = new Category(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
                return c;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
}
