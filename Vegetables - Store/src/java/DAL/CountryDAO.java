/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.Country;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author phanh
 */
public class CountryDAO extends DBContext{
    public List<Country> getAll(){
        try {
            String sql = "SELECT [countryID]\n"
                    + "      ,[countryName]\n"
                    + "      ,[describe]\n"
                    + "      ,[location]\n"
                    + "  FROM [project_duc].[dbo].[Countries]";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            
            List<Country> list = new ArrayList<>();
            while (rs.next()) {                
                Country c = new Country(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
                list.add(c);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
}
