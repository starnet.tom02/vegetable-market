/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.Category;
import Model.Product;
import Model.ProductImage;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author phanh
 */
public class ProductDAO extends DBContext {

    public List<Product> getAll() {
        try {
            String sql = "SELECT p.[productID]\n"
                    + "      ,p.[productName]\n"
                    + "      ,p.[productDescribe]\n"
                    + "      ,p.[productPreservation]\n"
                    + "      ,p.[productTitle]\n"
                    + "      ,p.[makerName]\n"
                    + "      ,p.[productPrice]\n"
                    + "      ,p.[productDiscount]\n"
                    + "      ,p.[productQuantity]\n"
                    + "      ,p.[productSeller]\n"
                    + "      ,p.[productWeight]\n"
                    + "      ,p.[productStatus]\n"
                    + "      ,p.[productTimeCreate]\n"
                    + "      ,p.[productTimeUpdate]\n"
                    + "      ,p.[categoryID]\n"
                    + "      ,c.[categoryName]\n"
                    + "      ,c.[describe]\n"
                    + "      ,c.[cid]\n"
                    + "      ,[pi].[image1]\n"
                    + "      ,[pi].[image2]\n"
                    + "      ,[pi].[image3]\n"
                    + "      ,[pi].[image4]\n"
                    + "      ,[pi].[image5]\n"
                    + "  FROM [project_duc].[dbo].[Products] p inner join [project_duc].[dbo].[Categories] c\n"
                    + "  ON p.categoryID = c.categoryID inner join [project_duc].[dbo].ProductImages [pi]\n"
                    + "  ON p.productID = [pi].productID";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            List<Product> list = new ArrayList<>();
            while (rs.next()) {
                Category c = new Category(rs.getInt("categoryID"), rs.getString("categoryName"), rs.getString("describe"), rs.getInt("cid"));

                ProductImage pi = new ProductImage(rs.getInt("productID"), rs.getString("image1"), rs.getString("image2"), rs.getString("image3"),
                        rs.getString("image4"), rs.getString("image5"));

                Product p = new Product(rs.getInt("productID"), rs.getString("productName"), rs.getString("productDescribe"), rs.getString("productPreservation"),
                        rs.getString("productTitle"), rs.getString("makerName"), rs.getDouble("productPrice"), rs.getDouble("productDiscount"), rs.getInt("productQuantity"),
                        rs.getInt("productSeller"), rs.getString("productWeight"), rs.getString("productStatus"), rs.getDate("productTimeCreate"), rs.getDate("productTimeUpdate"),
                        c, pi);
                list.add(p);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Product> getTop3ProductSeller() {
        try {
            String sql = "SELECT TOP(3) p.[productID]\n"
                    + "                          ,p.[productName]\n"
                    + "                          ,p.[productDescribe]\n"
                    + "                          ,p.[productPreservation]\n"
                    + "                          ,p.[productTitle]\n"
                    + "                          ,p.[makerName]\n"
                    + "                          ,p.[productPrice]\n"
                    + "                          ,p.[productDiscount]\n"
                    + "                          ,p.[productQuantity]\n"
                    + "                          ,p.[productSeller]\n"
                    + "                          ,p.[productWeight]\n"
                    + "                          ,p.[productStatus]\n"
                    + "                          ,p.[productTimeCreate]\n"
                    + "                          ,p.[productTimeUpdate]\n"
                    + "                          ,p.[categoryID]\n"
                    + "                          ,c.[categoryName]\n"
                    + "                          ,c.[describe]\n"
                    + "                          ,c.[cid]\n"
                    + "                          ,[pi].[image1]\n"
                    + "                          ,[pi].[image2]\n"
                    + "                          ,[pi].[image3]\n"
                    + "                          ,[pi].[image4]\n"
                    + "                          ,[pi].[image5]\n"
                    + "                      FROM [project_duc].[dbo].[Products] p inner join [project_duc].[dbo].[Categories] c\n"
                    + "                      ON p.categoryID = c.categoryID inner join [project_duc].[dbo].ProductImages [pi]\n"
                    + "                      ON p.productID = [pi].productID\n"
                    + "                      ORDER BY productSeller DESC";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            List<Product> list = new ArrayList<>();
            while (rs.next()) {
                Category c = new Category(rs.getInt("categoryID"), rs.getString("categoryName"), rs.getString("describe"), rs.getInt("cid"));

                ProductImage pi = new ProductImage(rs.getInt("productID"), rs.getString("image1"), rs.getString("image2"), rs.getString("image3"),
                        rs.getString("image4"), rs.getString("image5"));

                Product p = new Product(rs.getInt("productID"), rs.getString("productName"), rs.getString("productDescribe"), rs.getString("productPreservation"),
                        rs.getString("productTitle"), rs.getString("makerName"), rs.getDouble("productPrice"), rs.getDouble("productDiscount"), rs.getInt("productQuantity"),
                        rs.getInt("productSeller"), rs.getString("productWeight"), rs.getString("productStatus"), rs.getDate("productTimeCreate"), rs.getDate("productTimeUpdate"),
                        c, pi);
                list.add(p);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Product> get10ProductByCategoryID(int categoryID) {
        try {
            String sql = "SELECT TOP(10) p.[productID]\n"
                    + "                          ,p.[productName]\n"
                    + "                          ,p.[productDescribe]\n"
                    + "                          ,p.[productPreservation]\n"
                    + "                          ,p.[productTitle]\n"
                    + "                          ,p.[makerName]\n"
                    + "                          ,p.[productPrice]\n"
                    + "                          ,p.[productDiscount]\n"
                    + "                          ,p.[productQuantity]\n"
                    + "                          ,p.[productSeller]\n"
                    + "                          ,p.[productWeight]\n"
                    + "                          ,p.[productStatus]\n"
                    + "                          ,p.[productTimeCreate]\n"
                    + "                          ,p.[productTimeUpdate]\n"
                    + "                          ,p.[categoryID]\n"
                    + "                          ,c.[categoryName]\n"
                    + "                          ,c.[describe]\n"
                    + "                          ,c.[cid]\n"
                    + "                          ,[pi].[image1]\n"
                    + "                          ,[pi].[image2]\n"
                    + "                          ,[pi].[image3]\n"
                    + "                          ,[pi].[image4]\n"
                    + "                          ,[pi].[image5]\n"
                    + "                      FROM [project_duc].[dbo].[Products] p inner join [project_duc].[dbo].[Categories] c\n"
                    + "                      ON p.categoryID = c.categoryID inner join [project_duc].[dbo].ProductImages [pi]\n"
                    + "                      ON p.productID = [pi].productID\n"
                    + "                      WHERE 1=1\n";

            if (categoryID != 0) {
                sql += " AND p.categoryID = " + categoryID + "\nORDER BY p.productSeller DESC";
            } else {
                sql += "\nORDER BY p.productSeller DESC";
            }

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            List<Product> list = new ArrayList<>();
            while (rs.next()) {
                Category c = new Category(rs.getInt("categoryID"), rs.getString("categoryName"), rs.getString("describe"), rs.getInt("cid"));

                ProductImage pi = new ProductImage(rs.getInt("productID"), rs.getString("image1"), rs.getString("image2"), rs.getString("image3"),
                        rs.getString("image4"), rs.getString("image5"));

                Product p = new Product(rs.getInt("productID"), rs.getString("productName"), rs.getString("productDescribe"), rs.getString("productPreservation"),
                        rs.getString("productTitle"), rs.getString("makerName"), rs.getDouble("productPrice"), rs.getDouble("productDiscount"), rs.getInt("productQuantity"),
                        rs.getInt("productSeller"), rs.getString("productWeight"), rs.getString("productStatus"), rs.getDate("productTimeCreate"), rs.getDate("productTimeUpdate"),
                        c, pi);
                list.add(p);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Product> getTop3ProductSellerBycategoryID(int categoryID) {
        try {
            String sql = "SELECT TOP(3) p.[productID]\n"
                    + "                          ,p.[productName]\n"
                    + "                          ,p.[productDescribe]\n"
                    + "                          ,p.[productPreservation]\n"
                    + "                          ,p.[productTitle]\n"
                    + "                          ,p.[makerName]\n"
                    + "                          ,p.[productPrice]\n"
                    + "                          ,p.[productDiscount]\n"
                    + "                          ,p.[productQuantity]\n"
                    + "                          ,p.[productSeller]\n"
                    + "                          ,p.[productWeight]\n"
                    + "                          ,p.[productStatus]\n"
                    + "                          ,p.[productTimeCreate]\n"
                    + "                          ,p.[productTimeUpdate]\n"
                    + "                          ,p.[categoryID]\n"
                    + "                          ,c.[categoryName]\n"
                    + "                          ,c.[describe]\n"
                    + "                          ,c.[cid]\n"
                    + "                          ,[pi].[image1]\n"
                    + "                          ,[pi].[image2]\n"
                    + "                          ,[pi].[image3]\n"
                    + "                          ,[pi].[image4]\n"
                    + "                          ,[pi].[image5]\n"
                    + "                      FROM [project_duc].[dbo].[Products] p inner join [project_duc].[dbo].[Categories] c\n"
                    + "                      ON p.categoryID = c.categoryID inner join [project_duc].[dbo].ProductImages [pi]\n"
                    + "                      ON p.productID = [pi].productID\n"
                    + "                      WHERE 1=1 ";
            if (categoryID != 0) {
                sql += " AND p.categoryID = " + categoryID + " \nORDER BY p.productSeller DESC";
            } else {
                sql += "\nORDER BY p.productSeller DESC";
            }
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            List<Product> list = new ArrayList<>();
            while (rs.next()) {
                Category c = new Category(rs.getInt("categoryID"), rs.getString("categoryName"), rs.getString("describe"), rs.getInt("cid"));

                ProductImage pi = new ProductImage(rs.getInt("productID"), rs.getString("image1"), rs.getString("image2"), rs.getString("image3"),
                        rs.getString("image4"), rs.getString("image5"));

                Product p = new Product(rs.getInt("productID"), rs.getString("productName"), rs.getString("productDescribe"), rs.getString("productPreservation"),
                        rs.getString("productTitle"), rs.getString("makerName"), rs.getDouble("productPrice"), rs.getDouble("productDiscount"), rs.getInt("productQuantity"),
                        rs.getInt("productSeller"), rs.getString("productWeight"), rs.getString("productStatus"), rs.getDate("productTimeCreate"), rs.getDate("productTimeUpdate"),
                        c, pi);
                list.add(p);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Product> getProductByCategoryID(int categoryID) {
        try {
            String sql = "SELECT p.[productID]\n"
                    + "      ,p.[productName]\n"
                    + "      ,p.[productDescribe]\n"
                    + "      ,p.[productPreservation]\n"
                    + "      ,p.[productTitle]\n"
                    + "      ,p.[makerName]\n"
                    + "      ,p.[productPrice]\n"
                    + "      ,p.[productDiscount]\n"
                    + "      ,p.[productQuantity]\n"
                    + "      ,p.[productSeller]\n"
                    + "      ,p.[productWeight]\n"
                    + "      ,p.[productStatus]\n"
                    + "      ,p.[productTimeCreate]\n"
                    + "      ,p.[productTimeUpdate]\n"
                    + "      ,p.[categoryID]\n"
                    + "      ,c.[categoryName]\n"
                    + "      ,c.[describe]\n"
                    + "      ,c.[cid]\n"
                    + "      ,[pi].[image1]\n"
                    + "      ,[pi].[image2]\n"
                    + "      ,[pi].[image3]\n"
                    + "      ,[pi].[image4]\n"
                    + "      ,[pi].[image5]\n"
                    + "  FROM [project_duc].[dbo].[Products] p inner join [project_duc].[dbo].[Categories] c\n"
                    + "  ON p.categoryID = c.categoryID inner join [project_duc].[dbo].ProductImages [pi]\n"
                    + "  ON p.productID = [pi].productID"
                    + "  WHERE 1=1 ";

            if (categoryID != 0) {
                sql += " AND p.categoryID = " + categoryID;
            }
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            List<Product> list = new ArrayList<>();
            while (rs.next()) {
                Category c = new Category(rs.getInt("categoryID"), rs.getString("categoryName"), rs.getString("describe"), rs.getInt("cid"));

                ProductImage pi = new ProductImage(rs.getInt("productID"), rs.getString("image1"), rs.getString("image2"), rs.getString("image3"),
                        rs.getString("image4"), rs.getString("image5"));

                Product p = new Product(rs.getInt("productID"), rs.getString("productName"), rs.getString("productDescribe"), rs.getString("productPreservation"),
                        rs.getString("productTitle"), rs.getString("makerName"), rs.getDouble("productPrice"), rs.getDouble("productDiscount"), rs.getInt("productQuantity"),
                        rs.getInt("productSeller"), rs.getString("productWeight"), rs.getString("productStatus"), rs.getDate("productTimeCreate"), rs.getDate("productTimeUpdate"),
                        c, pi);
                list.add(p);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public Product getProductByProductID(int productID) {
        try {
            String sql = "SELECT p.[productID]\n"
                    + "      ,p.[productName]\n"
                    + "      ,p.[productDescribe]\n"
                    + "      ,p.[productPreservation]\n"
                    + "      ,p.[productTitle]\n"
                    + "      ,p.[makerName]\n"
                    + "      ,p.[productPrice]\n"
                    + "      ,p.[productDiscount]\n"
                    + "      ,p.[productQuantity]\n"
                    + "      ,p.[productSeller]\n"
                    + "      ,p.[productWeight]\n"
                    + "      ,p.[productStatus]\n"
                    + "      ,p.[productTimeCreate]\n"
                    + "      ,p.[productTimeUpdate]\n"
                    + "      ,p.[categoryID]\n"
                    + "      ,c.[categoryName]\n"
                    + "      ,c.[describe]\n"
                    + "      ,c.[cid]\n"
                    + "      ,[pi].[image1]\n"
                    + "      ,[pi].[image2]\n"
                    + "      ,[pi].[image3]\n"
                    + "      ,[pi].[image4]\n"
                    + "      ,[pi].[image5]\n"
                    + "  FROM [project_duc].[dbo].[Products] p inner join [project_duc].[dbo].[Categories] c\n"
                    + "  ON p.categoryID = c.categoryID inner join [project_duc].[dbo].ProductImages [pi]\n"
                    + "  ON p.productID = [pi].productID"
                    + "  WHERE p.productID = ? ";

            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, productID);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                Category c = new Category(rs.getInt("categoryID"), rs.getString("categoryName"), rs.getString("describe"), rs.getInt("cid"));

                ProductImage pi = new ProductImage(rs.getInt("productID"), rs.getString("image1"), rs.getString("image2"), rs.getString("image3"),
                        rs.getString("image4"), rs.getString("image5"));

                Product p = new Product(rs.getInt("productID"), rs.getString("productName"), rs.getString("productDescribe"), rs.getString("productPreservation"),
                        rs.getString("productTitle"), rs.getString("makerName"), rs.getDouble("productPrice"), rs.getDouble("productDiscount"), rs.getInt("productQuantity"),
                        rs.getInt("productSeller"), rs.getString("productWeight"), rs.getString("productStatus"), rs.getDate("productTimeCreate"), rs.getDate("productTimeUpdate"),
                        c, pi);
                return p;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Product> getListByPage(List<Product> list, int start, int end) {
        List<Product> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public void delete(int productID) {
        try {

            String sql = "DELETE FROM [dbo].[ProductImages]\n"
                    + "      WHERE productID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, productID);
            st.executeUpdate();

            String sql1 = "DELETE FROM [dbo].[ProductReviews]\n"
                    + "      WHERE productID = ?";
            PreparedStatement st1 = connection.prepareStatement(sql1);
            st1.setInt(1, productID);
            st1.executeUpdate();

            String sql2 = "DELETE FROM [dbo].[OrderDetails]\n"
                    + "      WHERE productID = ?";
            PreparedStatement st2 = connection.prepareStatement(sql2);
            st2.setInt(1, productID);
            st2.executeUpdate();

            String sql3 = "DELETE FROM [dbo].[Products]\n"
                    + "      WHERE productID = ?";
            PreparedStatement st3 = connection.prepareStatement(sql3);
            st3.setInt(1, productID);
            st3.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    public List<Product> getTop5ProductSeller() {
        try {
            String sql = "SELECT TOP(5) p.[productID]\n"
                    + "                          ,p.[productName]\n"
                    + "                          ,p.[productDescribe]\n"
                    + "                          ,p.[productPreservation]\n"
                    + "                          ,p.[productTitle]\n"
                    + "                          ,p.[makerName]\n"
                    + "                          ,p.[productPrice]\n"
                    + "                          ,p.[productDiscount]\n"
                    + "                          ,p.[productQuantity]\n"
                    + "                          ,p.[productSeller]\n"
                    + "                          ,p.[productWeight]\n"
                    + "                          ,p.[productStatus]\n"
                    + "                          ,p.[productTimeCreate]\n"
                    + "                          ,p.[productTimeUpdate]\n"
                    + "                          ,p.[categoryID]\n"
                    + "                          ,c.[categoryName]\n"
                    + "                          ,c.[describe]\n"
                    + "                          ,c.[cid]\n"
                    + "                          ,[pi].[image1]\n"
                    + "                          ,[pi].[image2]\n"
                    + "                          ,[pi].[image3]\n"
                    + "                          ,[pi].[image4]\n"
                    + "                          ,[pi].[image5]\n"
                    + "                      FROM [project_duc].[dbo].[Products] p inner join [project_duc].[dbo].[Categories] c\n"
                    + "                      ON p.categoryID = c.categoryID inner join [project_duc].[dbo].ProductImages [pi]\n"
                    + "                      ON p.productID = [pi].productID"
                    + " ORDER BY p.productSeller  DESC";
            
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            List<Product> list = new ArrayList<>();
            while (rs.next()) {
                Category c = new Category(rs.getInt("categoryID"), rs.getString("categoryName"), rs.getString("describe"), rs.getInt("cid"));

                ProductImage pi = new ProductImage(rs.getInt("productID"), rs.getString("image1"), rs.getString("image2"), rs.getString("image3"),
                        rs.getString("image4"), rs.getString("image5"));

                Product p = new Product(rs.getInt("productID"), rs.getString("productName"), rs.getString("productDescribe"), rs.getString("productPreservation"),
                        rs.getString("productTitle"), rs.getString("makerName"), rs.getDouble("productPrice"), rs.getDouble("productDiscount"), rs.getInt("productQuantity"),
                        rs.getInt("productSeller"), rs.getString("productWeight"), rs.getString("productStatus"), rs.getDate("productTimeCreate"), rs.getDate("productTimeUpdate"),
                        c, pi);
                list.add(p);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
    

}
