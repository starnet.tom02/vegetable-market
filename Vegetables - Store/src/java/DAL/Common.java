
package DAL;

import java.sql.Date;
import java.time.LocalDate;


public class Common {
    public static Date getCurrentDate() {
        LocalDate curDate = java.time.LocalDate.now();
        return Date.valueOf(curDate.toString());
    }
}
