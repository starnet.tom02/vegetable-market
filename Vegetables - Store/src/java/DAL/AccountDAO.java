/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Model.Account;
import Model.Country;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author phanh
 */
public class AccountDAO extends DBContext {

    public Account checkLogin(String userName, String password) {
        try {
            String sql = "SELECT [userName]\n"
                    + "      ,[password]\n"
                    + "      ,[firstName]\n"
                    + "      ,[lastName]\n"
                    + "      ,[phone]\n"
                    + "      ,[email]\n"
                    + "      ,[gender]\n"
                    + "      ,[address]\n"
                    + "      ,[avatar]\n"
                    + "      ,[birthDay]\n"
                    + "      ,a.[countryID]\n"
                    + "      ,[role]\n"
                    + "      ,[countryName]\n"
                    + "      ,[describe]\n"
                    + "      ,[location]\n"
                    + "  FROM [project_duc].[dbo].[Accounts] a inner join [project_duc].[dbo].[Countries] c\n"
                    + "  ON a.countryID = c.countryID"
                    + "  WHERE userName = ? AND password = ? ";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, userName);
            st.setString(2, password);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Country country = new Country(rs.getInt("countryID"), rs.getString("countryName"), rs.getString("describe"), rs.getString("location"));
                return new Account(userName, password, rs.getString("firstName"),
                        rs.getString("lastName"), rs.getString("phone"), rs.getString("email"), rs.getInt("gender"), rs.getString("address"),
                        rs.getString("avatar"), rs.getDate("birthDay"), rs.getInt("role"), country);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public Account checkRegister(String userName) {
        try {
            String sql = "SELECT [userName]\n"
                    + "      ,[password]\n"
                    + "      ,[firstName]\n"
                    + "      ,[lastName]\n"
                    + "      ,[phone]\n"
                    + "      ,[email]\n"
                    + "      ,[gender]\n"
                    + "      ,[address]\n"
                    + "      ,[avatar]\n"
                    + "      ,[birthDay]\n"
                    + "      ,a.[countryID]\n"
                    + "      ,[role]\n"
                    + "      ,[countryName]\n"
                    + "      ,[describe]\n"
                    + "      ,[location]\n"
                    + "  FROM [project_duc].[dbo].[Accounts] a inner join [project_duc].[dbo].[Countries] c\n"
                    + "  ON a.countryID = c.countryID"
                    + "  WHERE userName = ? ";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, userName);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Country country = new Country(rs.getInt("countryID"), rs.getString("countryName"), rs.getString("describe"), rs.getString("location"));
                return new Account(userName, rs.getString("password"), rs.getString("firstName"),
                        rs.getString("lastName"), rs.getString("phone"), rs.getString("email"), rs.getInt("gender"), rs.getString("address"),
                        rs.getString("avatar"), rs.getDate("birthDay"), rs.getInt("role"), country);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void register(Account a) {
        try {
            String sql = "INSERT INTO [dbo].[Accounts]\n"
                    + "           ([userName]\n"
                    + "           ,[password]\n"
                    + "           ,[firstName]\n"
                    + "           ,[lastName]\n"
                    + "           ,[phone]\n"
                    + "           ,[email]\n"
                    + "           ,[gender]\n"
                    + "           ,[address]\n"
                    + "           ,[avatar]\n"
                    + "           ,[birthDay]\n"
                    + "           ,[countryID]\n"
                    + "           ,[role])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, a.getUserName());
            st.setString(2, a.getPassword());
            st.setString(3, a.getFirstName());
            st.setString(4, a.getLastName());
            st.setString(5, a.getPhone());
            st.setString(6, a.getEmail());
            st.setInt(7, -1);
            st.setString(8, a.getAddress());
            st.setString(9, a.getAvatar());
            st.setDate(10, a.getBirthDay());
            st.setInt(11, 1);
            st.setInt(12, 0);
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    public void changePass(Account a){
        try {
            String sql = "UPDATE [dbo].[Accounts]\n"
                    + "   SET [password] = ?\n" 
                    + " WHERE userName = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, a.getPassword());
            st.setString(2, a.getUserName());
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void update(Account a) {
        try {
            String sql = "UPDATE [dbo].[Accounts]\n"
                    + "   SET [firstName] = ?\n"
                    + "      ,[lastName] = ?\n"
                    + "      ,[phone] = ?\n"
                    + "      ,[email] = ?\n"
                    + "      ,[address] = ?\n"
                    + "      ,[countryID] = ?\n"
                    + " WHERE userName = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, a.getFirstName());
            st.setString(2, a.getLastName());
            st.setString(3, a.getPhone());
            st.setString(4, a.getEmail());
            st.setString(5, a.getAddress());
            st.setInt(6, a.getCountry().getCountryID());
            st.setString(7, a.getUserName());
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void updateAdmin(Account a) {
        try {
            String sql = "UPDATE [dbo].[Accounts]\n"
                    + "   SET [firstName] = ?\n"
                    + "      ,[lastName] = ?\n"
                    + "      ,[phone] = ?\n"
                    + "      ,[email] = ?\n"
                    + "      ,[address] = ?\n"
                    + "      ,[countryID] = ?\n"
                    + "      ,[gender] = ?\n"
                    + " WHERE userName = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, a.getFirstName());
            st.setString(2, a.getLastName());
            st.setString(3, a.getPhone());
            st.setString(4, a.getEmail());
            st.setString(5, a.getAddress());
            st.setInt(6, a.getCountry().getCountryID());
            st.setInt(7, a.getGender());
            st.setString(8, a.getUserName());
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public int getTotalAccount() {
        try {
            String sql = "  SELECT COUNT(userName)\n"
                    + "  FROM [project_duc].[dbo].[Accounts]";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }
}
