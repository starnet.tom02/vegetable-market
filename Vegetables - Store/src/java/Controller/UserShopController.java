/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controller;

import DAL.CategoryDAO;
import DAL.ProductDAO;
import Model.Cart.Cart;
import Model.Product;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author phanh
 */
@WebServlet(name="UserShopController", urlPatterns={"/shop"})
public class UserShopController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserShopController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserShopController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        CategoryDAO cd = new CategoryDAO();
        ProductDAO pd = new ProductDAO();

        List<Product> prods = pd.getAll();
        Cookie arr[] = request.getCookies();
        String txt = "";

        for (Cookie item : arr) {
            if (item.getName().equals("cart")) {
                txt = txt + item.getValue();
            }
        }
        Cart cart = new Cart(txt, prods);
        request.setAttribute("cart", cart);
        
        String cid_raw = request.getParameter("cid");
        int cid;
        try {
            
            cid = Integer.parseInt(cid_raw);
            
        } catch (NumberFormatException e) {
            System.out.println(e);
            cid = 0;
        }
        
        List<Product> listP = pd.getProductByCategoryID(cid);
        request.setAttribute("cid", cid);
        
        int page, numberPerPage = 8;
        int size = listP.size();
        String xpage = request.getParameter("page");
        
        int numberOfPage = ( (size % 8 == 0) ? (size / 8) : (size / 8 + 1) );
        if(xpage == null){
            page = 1;
        }else {
            page = Integer.parseInt(xpage);
        }
        
        int start, end;
        start = (page - 1) * numberPerPage;
        end = Math.min(page * numberPerPage, size);
        
        List<Product> listByPage = pd.getListByPage(listP, start, end);
        
        request.setAttribute("page", page);
        request.setAttribute("numberPage", numberOfPage);
        request.setAttribute("start", start + 1);
        request.setAttribute("end", end);
        request.setAttribute("listByPage", listByPage);
        
        
        
        
        
        
        
        request.setAttribute("listProduct", pd.getAll());
        request.setAttribute("listProductSeller", pd.getTop3ProductSeller());
        request.setAttribute("listCategory", cd.getAllParentCategory());
        request.getRequestDispatcher("views/user/shop.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        ProductDAO pd = new ProductDAO();
        List<Product> listP = pd.getAll();
        Cookie arr[] = request.getCookies();
        String txt = "";
        if(arr != null){
            for (Cookie item : arr) {
                if (item.getName().equals("cart")) {
                    txt += item.getValue();
                    item.setMaxAge(0);
                    response.addCookie(item);
                }
            }
        }
        
        String productID_raw = request.getParameter("id");
        String quantity = request.getParameter("quantity");
        int productID;
        try {
            productID = Integer.parseInt(productID_raw);
            
        } catch (NumberFormatException e) {
            productID = 0;
        }
        Cart cartCheck = new Cart(txt, listP);
        Product productCheck = pd.getProductByProductID(productID);
        
        if(txt.isEmpty()){
            txt = productID + "_" + quantity;
        } else if (cartCheck.getItemByID(productCheck.getProductID()) == null) {
            if (txt.isEmpty()) {
                txt = productID + "_" + quantity;
            } else {
                txt = txt + "-" + productID + "_" + quantity;
            }
        } else if(cartCheck.getItemByID(productID).getQuantity() >= productCheck.getQuantity()){
            // không cộng vào cart nữa
        } else {
            txt = txt + "-" + productID + "_" + quantity;
        }
        
        Cookie c = new Cookie("cart", txt);
        c.setMaxAge(7 *24 * 60 * 60);
        response.addCookie(c);
        response.sendRedirect("shop");
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
