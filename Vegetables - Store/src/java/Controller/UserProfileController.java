/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controller;

import DAL.AccountDAO;
import DAL.CategoryDAO;
import DAL.CountryDAO;
import DAL.ProductDAO;
import Model.Account;
import Model.Cart.Cart;
import Model.Product;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author phanh
 */
@WebServlet(name="UserProfileController", urlPatterns={"/profiles"})
public class UserProfileController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserProfileController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserProfileController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        ProductDAO pd = new ProductDAO();
        CategoryDAO cd = new CategoryDAO();
        CountryDAO ctd = new CountryDAO();

        List<Product> prods = pd.getAll();
        Cookie arr[] = request.getCookies();
        String txt = "";

        for (Cookie item : arr) {
            if (item.getName().equals("cart")) {
                txt = txt + item.getValue();
            }
        }
        Cart cart = new Cart(txt, prods);
        request.setAttribute("cart", cart);

        request.setAttribute("fruit", pd.get10ProductByCategoryID(1));
        request.setAttribute("listFruitSeller", pd.getTop3ProductSellerBycategoryID(1));
        request.setAttribute("top10ProductSeller", pd.get10ProductByCategoryID(0));
        request.setAttribute("listCategory", cd.getAllParentCategory());
        
        request.setAttribute("country", ctd.getAll());
        request.getRequestDispatcher("views/user/profile.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String address = request.getParameter("address");
        String phone = request.getParameter("phone");
        String countryID_raw = request.getParameter("countryID");
        String birthday = request.getParameter("birthday");
        String email = request.getParameter("email");
        int countryID;
        try {
            countryID = Integer.parseInt(countryID_raw);
        } catch (NumberFormatException e) {
            System.out.println(e);
            countryID = 1;
        }
        
        AccountDAO ad = new AccountDAO();
        HttpSession session = request.getSession();
        
        Account a = (Account)session.getAttribute("account");
        
        if(a != null){
            if(firstName != null){
                a.setFirstName(firstName);
            }
            if(lastName != null){
                a.setLastName(lastName);
            }
            if(phone != null){
                a.setPhone(phone);
            }
            if(address != null){
                a.setAddress(address);
            }
            if(countryID_raw != null){
                a.getCountry().setCountryID(countryID);
            }
            if(email != null){
                a.setEmail(email);
            }
            
            if(birthday != null){
                a.setBirthDay(Date.valueOf(birthday));
            }
        }
        ad.update(a);
        session.removeAttribute("account");
        session.setAttribute("account", a);
        response.sendRedirect("profiles");
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
