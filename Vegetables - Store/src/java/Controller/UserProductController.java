/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import DAL.CategoryDAO;
import DAL.CountryDAO;
import DAL.ProductDAO;
import DAL.ProductReviewDAO;
import Model.Cart.Cart;
import Model.Product;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author phanh
 */
@WebServlet(name = "UserProductController", urlPatterns = {"/product"})
public class UserProductController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserProductController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserProductController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ProductDAO pd = new ProductDAO();
        CountryDAO ctd = new CountryDAO();
        CategoryDAO cd = new CategoryDAO();
        ProductReviewDAO prd = new ProductReviewDAO();

        List<Product> prods = pd.getAll();
        Cookie arr[] = request.getCookies();
        String txt = "";

        for (Cookie item : arr) {
            if (item.getName().equals("cart")) {
                txt = txt + item.getValue();
            }
        }
        Cart cart = new Cart(txt, prods);
        request.setAttribute("cart", cart);

        String id_raw = request.getParameter("id");
        int id;
        try {
            id = Integer.parseInt(id_raw);

        } catch (NumberFormatException e) {
            id = 0;
        }

        request.setAttribute("category", cd.getCategoryByProductID(id));
        request.setAttribute("product", pd.getProductByProductID(id));
        request.setAttribute("relatedProduct", pd.getProductByCategoryID(cd.getCategoryByProductID(id).getCategoryID()));
        request.setAttribute("country", ctd.getAll());
        request.setAttribute("review", prd.getReviewByProductID(id));
        request.setAttribute("avarageReview", prd.getAverageRateByProductID(id));
        request.setAttribute("fiveStart", prd.getTotalStart(id, 5));
        request.setAttribute("fourStart", prd.getTotalStart(id, 4));
        request.setAttribute("threeStart", prd.getTotalStart(id, 3));
        request.setAttribute("secondStart", prd.getTotalStart(id, 2));
        request.setAttribute("oneStart", prd.getTotalStart(id, 1));
        request.setAttribute("fivePercent", prd.getPercentStart(id, 5));
        request.setAttribute("fourPercent", prd.getPercentStart(id, 4));
        request.setAttribute("threePercent", prd.getPercentStart(id, 3));
        request.setAttribute("secondPercent", prd.getPercentStart(id, 2));
        request.setAttribute("onePercent", prd.getPercentStart(id, 1));

        request.getRequestDispatcher("views/user/product.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductDAO pd = new ProductDAO();
        List<Product> listP = pd.getAll();
        Cookie arr[] = request.getCookies();
        String txt = "";
        if (arr != null) {
            for (Cookie item : arr) {
                if (item.getName().equals("cart")) {
                    txt += item.getValue();
                    item.setMaxAge(0);
                    response.addCookie(item);
                }
            }
        }

        String productID_raw = request.getParameter("id");
        String quantity = request.getParameter("qty12554");
        int productID;
        try {
            productID = Integer.parseInt(productID_raw);

        } catch (NumberFormatException e) {
            productID = 0;
        }
        Cart cartCheck = new Cart(txt, listP);
        Product productCheck = pd.getProductByProductID(productID);

        if (txt.isEmpty()) {
            txt = productID + "_" + quantity;
        } else if (cartCheck.getItemByID(productCheck.getProductID()) == null) {
            if (txt.isEmpty()) {
                txt = productID + "_" + quantity;
            } else {
                txt = txt + "-" + productID + "_" + quantity;
            }
        } else if (cartCheck.getItemByID(productID).getQuantity() >= productCheck.getQuantity()) {
            // không cộng vào cart nữa
        } else {
            txt = txt + "-" + productID + "_" + quantity;
        }

        Cookie c = new Cookie("cart", txt);
        c.setMaxAge(7 * 24 * 60 * 60);
        response.addCookie(c);
        String url = request.getRequestURL().toString() + "?id=" + productID_raw;
        response.sendRedirect(url);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
