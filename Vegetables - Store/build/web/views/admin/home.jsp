<%-- 
    Document   : home
    Created on : 20-10-2022, 18:03:23
    Author     : phanh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
        <!-- Favicon-->
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="admin/assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="admin/assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css"/>
        <link rel="stylesheet" href="admin/assets/plugins/morrisjs/morris.css" />

        <!-- Custom Css -->
        <link rel="stylesheet" href="admin/assets/css/style.min.css">

        <title>ADMIN</title>
    </head>

    <body class="theme-blue">

        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="m-t-30"><img class="zmdi-hc-spin" src="admin/assets/images/loader.svg" width="48" height="48" alt="Aero"></div>
                <p>Please wait...</p>
            </div>
        </div>

        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>

        <!-- Main Search -->


        <!-- Right Icon menu Sidebar -->
        <div class="navbar-right">
            <ul class="navbar-nav">

                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" title="App" data-toggle="dropdown" role="button"><i class="zmdi zmdi-apps"></i></a>
                    <ul class="dropdown-menu slideUp2">
                        <li class="header">App Sortcute</li>
                        <li class="body">
                            <ul class="menu app_sortcut list-unstyled">
                                <li>
                                    <a href="image-gallery.html">
                                        <div class="icon-circle mb-2 bg-blue"><i class="zmdi zmdi-camera"></i></div>
                                        <p class="mb-0">Photos</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle mb-2 bg-amber"><i class="zmdi zmdi-translate"></i></div>
                                        <p class="mb-0">Translate</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="events.html">
                                        <div class="icon-circle mb-2 bg-green"><i class="zmdi zmdi-calendar"></i></div>
                                        <p class="mb-0">Calendar</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="contact.html">
                                        <div class="icon-circle mb-2 bg-purple"><i class="zmdi zmdi-account-calendar"></i></div>
                                        <p class="mb-0">Contacts</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle mb-2 bg-red"><i class="zmdi zmdi-tag"></i></div>
                                        <p class="mb-0">News</p>
                                    </a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="js-right-sidebar" title="Setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>
                <li><a href="a-logout" class="mega-menu" title="Sign Out"><i class="zmdi zmdi-power"></i></a></li>
            </ul>
        </div>

        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <div class="navbar-brand">
                <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>

                <a href="a-home"><img src="admin/assets/images/logo.svg" width="25" alt="Aero"><span class="m-l-10">ADMIN</span></a>
            </div>
            <div class="menu">
                <ul class="list">
                    <li>
                        <div class="user-info">
                            <a class="image" href="a-profile"><img src="${account.avatar}" alt="User"></a>
                            <div class="detail">
                                <h4>${account.fullName}</h4>
                                <small>Admin</small>                        
                            </div>
                        </div>
                    </li>
                    <li class="active"><a href="a-home"><i class="zmdi zmdi-home"></i><span>Home</span></a></li>
                    <li class="open"><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-shopping-cart"></i><span>Product</span></a>
                        <ul class="ml-menu">
                            <li><a href="a-product-list">Product List</a></li>
                            <li><a href="a-order-list">Order List</a></li>
                        </ul>
                    </li>   
                           

                </ul>
            </div>
        </aside>

        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs sm">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>

            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="setting">
                    <div class="slim_scroll">
                        <div class="card">
                            <h6>Theme Option</h6>
                            <div class="light_dark">
                                <div class="radio">
                                    <input type="radio" name="radio1" id="lighttheme" value="light" checked="">
                                    <label for="lighttheme">Light Mode</label>
                                </div>
                                <div class="radio mb-0">
                                    <input type="radio" name="radio1" id="darktheme" value="dark">
                                    <label for="darktheme">Dark Mode</label>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>                
            </div>       
        </div>
    </aside>

    <!-- Main Content -->
    <section class="content">
        <div class="body_scroll">
            <jsp:useBean id="ad" class="DAL.AccountDAO"/>
            <jsp:useBean id="pd" class="DAL.ProductDAO"/>
            <jsp:useBean id="prd" class="DAL.ProductReviewDAO"/>
            <jsp:useBean id="od" class="DAL.OrderDAO"/>

            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-6 text-center">
                        <div class="card">
                            <div class="body">                            
                                <input type="text" class="knob" value="${ad.totalAccount}" data-linecap="round" data-width="100" data-height="100" data-thickness="0.08" data-fgColor="#00adef" readonly>
                                <p>Account</p>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-6 text-center">
                        <div class="card">
                            <div class="body">                            
                                <input type="text" class="knob" value="${od.totalOrder}" data-linecap="round" data-width="100" data-height="100" data-thickness="0.08" data-fgColor="#ee2558" readonly>
                                <p>Total Orders</p>
                                
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6 col-6 text-center">
                        <div class="card">
                            <div class="body">                            
                                <input type="text" class="knob" value="${od.revenueOrder}" data-linecap="round" data-width="100" data-height="100" data-thickness="0.08" data-fgColor="#f67a82" readonly>
                                <p>Revenue</p>
                                <div class="d-flex bd-highlight text-center mt-4">
                                    <div class="flex-fill bd-highlight">
                                        <small class="text-muted">Inbound</small>
                                        <h5 class="mb-0">${od.revenueOrder}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xl-8 col-lg-7 col-md-12">
                        <div class="card">
                            <div class="body">
                                <div class="header">
                                    <h2><strong>Review</strong> Mới</h2>
                                </div>
                                <ul class="row list-unstyled c_review">
                                    <c:forEach items="${prd.top5ReviewNew}" var="pr">
                                        <li class="col-12">
                                            <div class="avatar">
                                                <a href="javascript:void(0);"><img class="rounded" src="${pr.account.avatar}" alt="user" width="60"></a>
                                            </div>                                
                                            <div class="comment-action">
                                                <h6 class="c_name">${pr.account.fullName}</h6>
                                                <p class="c_msg m-b-0">${pr.content}</p>
                                                <div class="badge badge-info">${pr.product.productName}</div>
                                                <span class="m-l-10">
                                                    <c:set var="count" value="1"/>
                                                    <c:forEach begin="1" end="${pr.rate}" >
                                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star col-amber"></i></a>
                                                            <c:set var="count" value="${count + 1}"/>
                                                        </c:forEach>
                                                        <c:if test="${count <= 5}">
                                                            <c:forEach begin="${count}" end="5" >
                                                            <a href="javascript:void(0);"><i class="zmdi zmdi-star-outline col-amber"></i></a>
                                                            </c:forEach>
                                                        </c:if>
                                                </span>
                                                <small class="comment-date float-sm-right">${pr.reviewTimeCreate}</small>
                                            </div>                                
                                        </li>
                                    </c:forEach>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="card">
                            <div class="header">
                                <h2><strong>Sản Phẩm</strong> Bán Chạy</h2>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover theme-color c_table">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Name</th>
                                            <th>SL Bán</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${pd.top5ProductSeller}" var="p">
                                            <tr>
                                                <td class="w70"><img class="w50" src="${p.image.image1}" alt=""></td>
                                                <td><a href="a-product?id=${p.productID}" class="text-muted">${p.productName}</a></td>
                                                <td>${p.seller}</td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>              
                        </div>
                    </div>
                </div>
                <div class="row clearfix">

                </div>
                <div class="row clearfix">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="header">
                                <h2><strong>Orders</strong> Trong Tuần</h2>

                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover c_table">
                                    <thead>
                                        <tr>
                                            <th style="width:60px;">#</th>
                                            <th>Name</th>
                                            <th>Address</th>
                                            <th>Amount</th>                                    
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${od.top5OrderNew}" var="o">
                                            <tr>
                                                <td><img src="${o.account.avatar}" alt="Product img"></td>
                                                <td>${o.account.fullName}</td>
                                                <td>${o.address}</td>
                                                <td>${o.totalMoney} đ</td>
                                                <td><span class="badge ${o.status == 'Chờ Xử Lý' ? 'badge-warning' : 'badge-success'}">${o.status}</span></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>        
            </div>
        </div>
    </section>
    <!-- Jquery Core Js --> 
    <script src="admin/assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
    <script src="admin/assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

    <script src="admin/assets/bundles/morrisscripts.bundle.js"></script> <!-- Morris Plugin Js -->
    <script src="admin/assets/bundles/jvectormap.bundle.js"></script> <!-- JVectorMap Plugin Js -->
    <script src="admin/assets/bundles/sparkline.bundle.js"></script> <!-- Sparkline Plugin Js -->
    <script src="admin/assets/bundles/knob.bundle.js"></script> <!-- Jquery Knob Plugin Js -->

    <script src="admin/assets/bundles/mainscripts.bundle.js"></script>
    <script src="admin/assets/js/pages/ecommerce.js"></script>
    <script src="admin/assets/js/pages/charts/jquery-knob.min.js"></script>
</body>


</html>