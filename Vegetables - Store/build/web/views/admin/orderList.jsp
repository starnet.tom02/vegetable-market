<%-- 
    Document   : orderList
    Created on : 20-10-2022, 18:58:35
    Author     : phanh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html class="no-js " lang="en">


    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

        <title>:: Aero Bootstrap4 Admin :: Jquery DataTables</title>
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <!-- Favicon-->
        <link rel="stylesheet" href="admin/assets/plugins/bootstrap/css/bootstrap.min.css">
        <!-- JQuery DataTable Css -->
        <link rel="stylesheet" href="admin/assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css">
        <!-- Custom Css -->
        <link  rel="stylesheet" href="admin/assets/css/style.min.css">
    </head>

    <body class="theme-blue">

        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="m-t-30"><img class="zmdi-hc-spin" src="admin/assets/images/loader.svg" width="48" height="48" alt="Aero"></div>
                <p>Please wait...</p>
            </div>
        </div>

        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>

        <!-- Main Search -->


        <!-- Right Icon menu Sidebar -->
        <div class="navbar-right">
            <ul class="navbar-nav">

                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" title="App" data-toggle="dropdown" role="button"><i class="zmdi zmdi-apps"></i></a>
                    <ul class="dropdown-menu slideUp2">
                        <li class="header">App Sortcute</li>
                        <li class="body">
                            <ul class="menu app_sortcut list-unstyled">
                                <li>
                                    <a href="image-gallery.html">
                                        <div class="icon-circle mb-2 bg-blue"><i class="zmdi zmdi-camera"></i></div>
                                        <p class="mb-0">Photos</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle mb-2 bg-amber"><i class="zmdi zmdi-translate"></i></div>
                                        <p class="mb-0">Translate</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="events.html">
                                        <div class="icon-circle mb-2 bg-green"><i class="zmdi zmdi-calendar"></i></div>
                                        <p class="mb-0">Calendar</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="contact.html">
                                        <div class="icon-circle mb-2 bg-purple"><i class="zmdi zmdi-account-calendar"></i></div>
                                        <p class="mb-0">Contacts</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle mb-2 bg-red"><i class="zmdi zmdi-tag"></i></div>
                                        <p class="mb-0">News</p>
                                    </a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="js-right-sidebar" title="Setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>
                <li><a href="a-logout" class="mega-menu" title="Sign Out"><i class="zmdi zmdi-power"></i></a></li>
            </ul>
        </div>

        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <div class="navbar-brand">
                <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>

                <a href="a-home"><img src="admin/assets/images/logo.svg" width="25" alt="Aero"><span class="m-l-10">ADMIN</span></a>
            </div>
            <div class="menu">
                <ul class="list">
                    <li>
                        <div class="user-info">
                            <a class="image" href="a-profile"><img src="${account.avatar}" alt="User"></a>
                            <div class="detail">
                                <h4>${account.fullName}</h4>
                                <small>Admin</small>                        
                            </div>
                        </div>
                    </li>
                    <li class="active"><a href="a-home"><i class="zmdi zmdi-home"></i><span>Home</span></a></li>
                    <li class="open"><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-shopping-cart"></i><span>Product</span></a>
                        <ul class="ml-menu">
                            <li><a href="a-product-list">Product List</a></li>
                            <li><a href="a-order-list">Order List</a></li>
                        </ul>
                    </li>   
                        

                </ul>
            </div>
        </aside>

        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs sm">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#setting"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li>

            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="setting">
                    <div class="slim_scroll">
                        <div class="card">
                            <h6>Theme Option</h6>
                            <div class="light_dark">
                                <div class="radio">
                                    <input type="radio" name="radio1" id="lighttheme" value="light" checked="">
                                    <label for="lighttheme">Light Mode</label>
                                </div>
                                <div class="radio mb-0">
                                    <input type="radio" name="radio1" id="darktheme" value="dark">
                                    <label for="darktheme">Dark Mode</label>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>                
            </div>       
        </div>
    </aside>

        <section class="content">
            <div class="body_scroll">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-7 col-md-6 col-sm-12">
                            <h2>Danh Sách Đơn Hàng</h2>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="home"><i class="zmdi zmdi-home"></i> home</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Product</a></li>
                                <li class="breadcrumb-item active">Danh Sách Đơn Hàng</li>
                            </ul>
                            <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                        </div>
                        <div class="col-lg-5 col-md-6 col-sm-12">                
                            <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>                                
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <!-- Exportable Table -->
                    <div class="row clearfix">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover dataTable js-exportable table-hover c_table mb-0">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Tên KH</th>
                                                    <th>Số Điện Thoại</th>
                                                    <th>Tổng Tiền</th>
                                                    <th>Thời Gian</th>
                                                    <th>Trạng Thái</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${listOrder}" var="o">
                                                    <tr>
                                                        <td>${o.orderID}</td>
                                                        <td>${o.account.fullName}</td>
                                                        <td>${o.phone}</td>
                                                        <td>${o.totalMoney}</td>
                                                        <td>${o.orderTime}</td>
                                                        <td>${o.status}</td>
                                                        <td>
                                                            <a href="a-order?id=${o.orderID}" class="btn btn-default waves-effect waves-float btn-sm waves-green"><i class="zmdi zmdi-edit"></i></a>
                                                            <a href="javascript:void(0);" class="btn btn-default waves-effect waves-float btn-sm waves-red"><i class="zmdi zmdi-delete"></i></a>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>


        <!-- Jquery Core Js --> 
        <script src="admin/assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
        <script src="admin/assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

        <!-- Jquery DataTable Plugin Js --> 
        <script src="admin/assets/bundles/datatablescripts.bundle.js"></script>
        <script src="admin/assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
        <script src="admin/assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
        <script src="admin/assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
        <script src="admin/assets/plugins/jquery-datatable/buttons/buttons.flash.min.js"></script>
        <script src="admin/assets/plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
        <script src="admin/assets/plugins/jquery-datatable/buttons/buttons.print.min.js"></script>

        <script src="admin/assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
        <script src="admin/assets/js/pages/tables/jquery-datatable.js"></script>
    </body>


</html>