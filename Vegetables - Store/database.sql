﻿USE [master]
GO

/*******************************************************************************
   Drop database if it exists
********************************************************************************/
IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'project_duc')
BEGIN
	ALTER DATABASE [project_duc] SET OFFLINE WITH ROLLBACK IMMEDIATE;
	ALTER DATABASE [project_duc] SET ONLINE;
	DROP DATABASE [project_duc];
END

GO

CREATE DATABASE [project_duc]
GO

USE [project_duc]
GO

/*******************************************************************************
	Drop tables if exists
*******************************************************************************/
DECLARE @sql nvarchar(MAX) 
SET @sql = N'' 

SELECT @sql = @sql + N'ALTER TABLE ' + QUOTENAME(KCU1.TABLE_SCHEMA) 
    + N'.' + QUOTENAME(KCU1.TABLE_NAME) 
    + N' DROP CONSTRAINT ' -- + QUOTENAME(rc.CONSTRAINT_SCHEMA)  + N'.'  -- not in MS-SQL
    + QUOTENAME(rc.CONSTRAINT_NAME) + N'; ' + CHAR(13) + CHAR(10) 
FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS AS RC 

INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KCU1 
    ON KCU1.CONSTRAINT_CATALOG = RC.CONSTRAINT_CATALOG  
    AND KCU1.CONSTRAINT_SCHEMA = RC.CONSTRAINT_SCHEMA 
    AND KCU1.CONSTRAINT_NAME = RC.CONSTRAINT_NAME 

EXECUTE(@sql) 

GO
DECLARE @sql2 NVARCHAR(max)=''

SELECT @sql2 += ' Drop table ' + QUOTENAME(TABLE_SCHEMA) + '.'+ QUOTENAME(TABLE_NAME) + '; '
FROM   INFORMATION_SCHEMA.TABLES
WHERE  TABLE_TYPE = 'BASE TABLE'

Exec Sp_executesql @sql2 
GO 

CREATE TABLE Accounts(
	[userName] nvarchar(255) not null primary key,
	[password] nvarchar(255) not null,
	[firstName] nvarchar(100)null ,
	[lastName] nvarchar(100)null ,
	[phone] nvarchar(255)  null,
	[email] nvarchar(255) null,
	[gender] int default(-1) null,
	[address] nvarchar(255) null,
	[avatar] nvarchar(255) null,
	[birthDay] DATE NULL,
	[countryID] int null ,
	[role] int not null
)
GO

CREATE TABLE Countries (
	[countryID] int identity(1, 1) not null primary key,
	[countryName] nvarchar(255) not null unique,
	[describe] nvarchar(255),
	[location] nvarchar(255) not null
)
GO

CREATE TABLE Orders (
	[orderID] int identity(1, 1) not null primary key,
	[status] nvarchar(255) not null,
	[totalMoney] money,
	[note] nvarchar(255) null,
	[orderTime] DATE NULL,
	[firstName] nvarchar(255),
	[lastName] nvarchar(255),
	[addressShipping] nvarchar(255),
	[email] nvarchar(255),
	[phone] nvarchar(255),
	[userName] nvarchar(255),
	[shippingID] int,
)
GO

CREATE TABLE OrderDetails(
	[orderDetailID] int identity(1,1) not null,
	[unitPrice] money,
	[quantity] int,
	[orderID] int,
	[productID] int,
	PRIMARY KEY ([orderDetailID], [orderID],[productID])
)
GO

CREATE TABLE Products (
	[productID] int identity(1, 1) not null primary key,
	[productName] nvarchar(255) not null unique,
	[productDescribe] nvarchar(255) null,
	[productPreservation] nvarchar(255) null,
	[productTitle] nvarchar(255) null,
	[makerName] nvarchar(255) null,
	[productPrice] money,
	[productDiscount] money,
	[productQuantity] int,
	[productSeller] int,
	[productWeight] nvarchar(255),
	[productStatus] nvarchar(255),
	[productTimeCreate] DATE,
	[productTimeUpdate] DATE,
	[categoryID] int
)
GO

CREATE TABLE ProductImages (
	[productID] int primary key,
	[image1] nvarchar(255) null,
	[image2] nvarchar(255) null,
	[image3] nvarchar(255) null,
	[image4] nvarchar(255) null,
	[image5] nvarchar(255) null,
)
GO

CREATE TABLE ProductReviews (
	[reviewID] int identity(1, 1) not null,
	[reviewContent] nvarchar(255),
	[reviewRate] int,
	[reviewLike] int,
	[reviewDislike] int,
	[reviewTimeCreate] DATE,
	[productID] int,
	[userName] nvarchar(255),
	PRIMARY KEY ([reviewID], [productID], [userName])
)
GO

CREATE TABLE Categories (
	[categoryID] int identity(1, 1) not null primary key,
	[categoryName] nvarchar(255) not null unique,
	[describe] nvarchar(255) null,
	[cid] int,
)
GO

CREATE TABLE CompanyShips (
	[companyShipID] int identity(1, 1) not null primary key,
	[companyShipName] nvarchar(255),
)
GO


ALTER TABLE Accounts
ADD CONSTRAINT fk_account_country FOREIGN KEY([countryID]) REFERENCES Countries([countryID])
GO

ALTER TABLE Orders
ADD CONSTRAINT fk_order_account FOREIGN KEY([userName]) REFERENCES Accounts([userName]),
	CONSTRAINT fk_order_companyShip FOREIGN KEY([shippingID]) REFERENCES CompanyShips([companyShipID])
GO

ALTER TABLE OrderDetails
ADD CONSTRAINT fk_order_orderDetail FOREIGN KEY([orderID]) REFERENCES Orders([orderID]),
	CONSTRAINT fk_order_product FOREIGN KEY([productID]) REFERENCES Products([productID])
GO

ALTER TABLE ProductImages 
ADD	CONSTRAINT fk_productImage_product FOREIGN KEY([productID]) REFERENCES Products([productID])
GO

ALTER TABLE ProductReviews 
ADD	CONSTRAINT fk_productReview_product FOREIGN KEY([productID]) REFERENCES Products([productID]),
	CONSTRAINT fk_productReview_account FOREIGN KEY([userName]) REFERENCES Accounts([userName])
GO

ALTER TABLE Categories 
ADD	CONSTRAINT fk_category_category FOREIGN KEY([cid]) REFERENCES Categories([categoryID])
GO

ALTER TABLE Products 
ADD	CONSTRAINT fk_product_category FOREIGN KEY([categoryID]) REFERENCES Categories([categoryID])
GO

INSERT INTO [dbo].[CompanyShips] ([companyShipName]) VALUES('VNpost')
GO
INSERT INTO [dbo].[CompanyShips] ([companyShipName]) VALUES('Viettel Post')
GO
INSERT INTO [dbo].[CompanyShips] ([companyShipName]) VALUES(N'Giao Hàng Nhanh')
GO
INSERT INTO [dbo].[CompanyShips] ([companyShipName]) VALUES(N'Giao Hàng Tiết Kiệm')
GO
INSERT INTO [dbo].[CompanyShips] ([companyShipName]) VALUES('Ahamove')
GO
INSERT INTO [dbo].[CompanyShips] ([companyShipName]) VALUES('Nhanh.vn')
GO
INSERT INTO [dbo].[CompanyShips] ([companyShipName]) VALUES('J&T Express')
GO
INSERT INTO [dbo].[CompanyShips] ([companyShipName]) VALUES('Best Express')
GO

INSERT INTO [dbo].[Countries] ([countryName], [describe], [location])
     VALUES (N'Việt Nam', N'Cộng hòa Xã hội chủ nghĩa Việt Nam', N'Đông Nam Á')
GO
INSERT INTO [dbo].[Countries] ([countryName], [describe], [location])
     VALUES (N'Campuchia', N'Vương quốc Campuchia', N'Đông Nam Á')
GO
INSERT INTO [dbo].[Countries] ([countryName], [describe], [location])
     VALUES (N'Lào', N'Cộng hòa Dân chủ Nhân dân Lào', N'Đông Nam Á')
GO
INSERT INTO [dbo].[Countries] ([countryName], [describe], [location])
     VALUES (N'Thái Lan', N'Vương quốc Thái Lan', N'Đông Nam Á')
GO
INSERT INTO [dbo].[Countries] ([countryName], [describe], [location])
     VALUES (N'Myanmar', N'Cộng hòa Liên bang Myanmar', N'Đông Nam Á')
GO
INSERT INTO [dbo].[Countries] ([countryName], [describe], [location])
     VALUES (N'Malaysia', N'Malaysia', N'Đông Nam Á')
GO
INSERT INTO [dbo].[Countries] ([countryName], [describe], [location])
     VALUES (N'Singapore', N'Cộng hòa Singapore', N'Đông Nam Á')
GO

INSERT INTO [dbo].[Countries] ([countryName], [describe], [location])
     VALUES (N'Indonesia', N'Cộng hòa Indonesia', N'Đông Nam Á')
GO
INSERT INTO [dbo].[Countries] ([countryName], [describe], [location])
     VALUES (N'Brunei', N'Negara Brunei Darussalam', N'Đông Nam Á')
GO
INSERT INTO [dbo].[Countries] ([countryName], [describe], [location])
     VALUES (N'Philippines', N'Cộng hòa Philippines', N'Đông Nam Á')
GO
INSERT INTO [dbo].[Countries] ([countryName], [describe], [location])
     VALUES (N'Đông Timor', N'Cộng hòa Dân chủ Đông Timor', N'Đông Nam Á')
GO


INSERT INTO [dbo].[Accounts] ([userName], [password], [firstName], [lastName], [phone], [email], [gender], [address], [avatar], [birthDay], [countryID], [role])
     VALUES ('1', '1', N'Hoàng', N'Đức', '0326887855', 'starnet.tom02@gmail.com', 1, N'Tổ 67b, khu 5, phường Cao Xanh, thành phố Hạ Long - Quảng Ninh', 'images/avatar/anhthducdeptrai.jpg', '2002/04/17', 1, 1)
GO

INSERT INTO [dbo].[Categories]	([categoryName], [describe], [cid])
     VALUES	(N'Fruit & Nut Gifts', N'Quà Tặng Trái Cây Và Hạt', null)
GO
INSERT INTO [dbo].[Categories]	([categoryName], [describe], [cid])
     VALUES	(N'Vegetables', N'Rau', null)
GO
INSERT INTO [dbo].[Categories]	([categoryName], [describe], [cid])
     VALUES	(N'Fresh Berries', N'Quả Mọng Tươi', null)
GO
INSERT INTO [dbo].[Categories]	([categoryName], [describe], [cid])
     VALUES	(N'Ocean Foods', N'Thực Phẩm Đại Dương', null)
GO
INSERT INTO [dbo].[Categories]	([categoryName], [describe], [cid])
     VALUES	(N'Fresh Meat', N'Thịt Tươi Sống', null)
GO
INSERT INTO [dbo].[Categories]	([categoryName], [describe], [cid])
     VALUES	(N'Fresh Onion', N'Hành Tươi', null)
GO
INSERT INTO [dbo].[Categories]	([categoryName], [describe], [cid])
     VALUES	(N'Spice', N'Gia Vị', null)
GO
INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Dưa leo – Loại 1kg'
           ,N'Theo nghiên cứu 1 quả dưa leo sống, chưa gọt vỏ, nặng 11 ounce (300 gram) chứa những những dưỡng chất sau:; Lượng calo: 45; Tổng chất béo: 0 gram; Carbs: 11 gram; Chất đạm: 2 gam; Chất xơ: 2 gam; Vitamin C: 14% RDI; Vitamin…'
           ,N'Trước khi bảo quản dưa leo, hãy rửa loại quả này sạch sẽ dưới vòi nước, để trong không khí một lúc cho đến khi dưa leo khô. Loại quả này được khuyến cáo cần được giữ trong điều kiện sạch sẽ và khô ráo.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 19500, 0, 100, 0 , N'1 kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 1)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (1, 'images/product/dua-leo.jpg', 'images/product/dua-leo-loai-1kg.jpg', 'images/product/dua-leo-loai-1kg (1).jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Cà chua – Loại 1kg'
           ,N'Cà chua tỷ lệ chiếm cao nhất là nước 95%, 5% còn lại chủ yếu bao gồm carbohydrate và chất xơ. Trong 100 gam cà chua sống bao gồm thành phần dinh dưỡng sau: 18 kcal, 0,9 gam đạm, 3,9 gam carb, 2,6 gam đường, 1,2 gam chất xơ, 0,2 gam chất béo…'
           ,N'Cà chua chín đỏ với những quả cà chua đã chín đỏ, bạn nên dùng giấy bọc lại rồi cho vào ngăn mát tủ lạnh, điều chỉnh nhiệt độ ở mức 2 – 5 độ C và độ ẩm từ 85 – 90% để cà chua được giữ tươi lâu hơn, không bị nhăn nheo hay nhanh hỏng'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 28000, 0, 100, 0 , N'1 kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 1)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (2, 'images/product/ca-chua.jpg', 'images/product/ca-chua-loai-1kg.jpg', 'images/product/ca-chua-loai-1kg-2.jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Bí đỏ hồ lô – Loại 1kg'
           ,N'Calo: 49; Carbs: 12 gram; Chất xơ: 3 gram; Protein: 2 gram; Vitamin K: 49% RDI; Vitamin C: 19% RDI; Kali: 16% RDI; Đồng, mangan và riboflavin: 11% RDI; Vitamin E: 10% RDI; Sắt: 8% RDI; Folate: 6% RDI Niacin, axit pantothenic, vitamin B6 và thiamin: 5% RDI'
           ,N'Khi bảo quản bí đỏ hồ lô nấu trong tủ lạnh bạn nhất định phải bỏ vào hộp đựng có nắp đậy hoặc túi zip, không để gần các loại thực phẩm nặng mùi. Bí đỏ tươi bạn không được cho trực tiếp vào tủ lạnh mà phải dùng túi đựng, làm bí nhanh phân hủy'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 18000, 0, 100, 0 , N'1 kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 1)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (3, 'images/product/bi-do-ho-lo-loai-1kg.jpg', 'images/product/bi-do-ho-lo-loai-1kg-tuoi.jpg', 'images/product/bi-do-ho-lo-loai-1kg (1).jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Chuối già hương – Loại 1kg'
           ,N'Trong 100gr chuối già hương có chứa: Nước 74,91gr; năng lượng 89 calo, protein 1,09gr; chất béo 0,03gr; carbohydrate 22,84gr; chất xơ 2,6gr; đường 12,23gr.Canxi 5mg; photpho 22mg; sắt 0,26mg; kali 358mg; kẽm 0,15mg; natri 1mg'
           ,N'Sử dụng một miếng màng bọc thực phẩm. Gói riêng từng cuống chuối sẽ giúp làm chậm quá trình chín của chuối. Vì vậy, ngay sau khi mua một nải, bạn hãy tách từng quả chuối ra và dùng màng bọc thực phẩm bọc các cuống lại.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 17000, 0, 100, 0 , N'1 kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 1)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (4, 'images/product/chuoi-gia-huong-loai-1kg.jpg', 'images/product/chuoi-gia-huong-tuoi.jpg', 'images/product/chuoi-gia-huong.jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Bí xanh – Loại 1kg'
           ,N'Hàm lượng dinh dưỡng có trong 500g như sau:8g đường; 1,5g albumin; 6,1g vitamin C và canxi. Ngoài ra còn có nhiều photpho, sắt, vitamin B1, B2…'
           ,N'Bảo quản bí nơi khô thoáng, tránh ánh nắng mặt trời'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 18000, 0, 100, 0 , N'1 kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 1)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (5, 'images/product/bi-xanh-loai-1kg-tuoi.jpg', 'images/product/bi-xanh-loai-1kg.jpg', 'images/product/bi-xanh-loai-1kg.jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Khổ qua – Loại 1kg'
           ,N'Trong 124gram khổ quag nấu chín mà không có thêm chất béo như: Lượng calo: 24; Chất béo: 0,2g; Natri: 392mg; Carbohydrate: 5,4g; Chất xơ: 2,5g; Đường: 2,4g; Chất đạm: 1g; '
           ,N'Rửa sạch và để ráo nước hoặc dùng khăn để lau khô. Dùng giấy báo bọc khổ qua lại rồi cho vào túi zip hoặc hộp đựng thực phẩm, đóng nắp lại sau đó cho vào ngăn mát tủ lạnh để bảo quản.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 25500, 0, 100, 0 , N'1 kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 1)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (6, 'images/product/kho-qua-loai-1kg.jpg', 'images/product/kho-qua.jpg', 'images/product/kho-qua-loai-1kg (1).jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Bầu dài – Loại 1kg'
           ,N'Trái bầu không chỉ có hàm lượng nước cao mà còn cung cấp nhiều dưỡng chất, vitamin và khoáng chất thiết yếu cho sức khỏe như tinh bột, chất xơ, canxi, sắt, magiê, phốtpho, kali, natri, kẽm, vitamin C, vitamin B6…'
           ,N'Bảo quản bầu ở nơi khô ráo, thoáng mát, tránh ánh nắng mặt trời. Nếu không ăn hết, thì phần còn lại nhất định phải bọc giấy hoặc cho vào túi nilon có lỗ, cất trong tủ lạnh và nên được dùng trong vòng 2 ngày sau đó'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 18000, 0, 100, 0 , N'1 kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 1)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (7, 'images/product/bau-dai-loai-1kg.jpg', 'images/product/bau-dai.jpg', 'images/product/bau-dai-loai-1kg-9.jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Mướp – Loại 1kg'
           ,N'Với 300g mướp sẽ cung cấp khoảng 51 calo cho cơ thể. Ngoài ra, trong mướp còn có chứa 95gr nước, 0,9gr protit, 0,1gr lipid, 3gr glucit, 0,5gr xenluloza, 28mg sắt, 160 mcg beta caroten và rất nhiều vitamin B, C…'
           ,N'Dùng màng bọc thực phẩm hoặc túi ni lông bọc kín mướp lại và đem đi bảo quản. Nếu nhiệt độ quá cao, có thể cho mướp vào ngâm trong thùng nước. Làm như vậy, trong khoảng 1 tuần tới, mướp sẽ vẫn luôn tươi ngon'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 18000, 0, 100, 0 , N'1 kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 1)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (8, 'images/product/muop-huong-loai-1kg.jpg', 'images/product/muop-loai-1kg.jpg', 'images/product/muop.jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Trái bắp Mỹ – Loại 1kg'
           ,N'Trong 100g trái bắp mỹ chứa Calo: 96; Nước: 73 %; Protein: 3.4 g; Carb: 21 g; Đường: 4.5 g; Chất xơ: 2.4 g; Chất béo: 1.5 g; Chất béo bão hòa: 0.2 g; Chất béo không bão hòa đơn: 0.37 g; Không sinh cholesterol: 0.6 g; Omega-3: 0.02 g; Omega-6: 0.59g'
           ,N'Dùng tay lột lớp vỏ ngoài cùng của bắp rồi dùng kéo hoặc dao cắt bớt đi phần râu bắp cho sạch và tiết kiệm không gian tủ lạnh. Dùng màng bọc thực phẩm bọc kín trái bắp lại để giữ độ ẩm cho bắp và không bị ám mùi của những thực phẩm khác trong tủ lạnh'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 28000, 0, 100, 0 , N'1 kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 1)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (9, 'images/product/trai-bap-my-tuoi.jpg', 'images/product/trai-bap-my-loai-1kg.jpg', 'images/product/trai-bap-my.jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Cà tím – Loại 1kg'
           ,N'Năng lượng; Carbs: 5 g; chất xơ: 3 g ; protein: 1 g; Mangan: 10% RDI; Folate: 5% RDI; Kali: 5% RDI; vitamin K: 4% RDI; vitamin C: 3% RDI. Ngoài ra, cà tím cũng chứa một lượng nhỏ chất dinh dưỡng khác bao gồm niacin, magiê và đồng'
           ,N'Nên giữ cà tím ở nơi thoáng mát, tránh xa ánh sáng trực tiếp từ mặt trời, và sử dụng cà tím càng sớm tốt sau khi thu hoạch hoặc mua'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 18000, 0, 100, 0 , N'1 kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 1)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (10, 'images/product/ca-tim-loai-1kg.jpg', 'images/product/ca-tim-loai-1kg-tuoi.jpg', 'images/product/ca-tim-ca-dai-de-loai-1kg.jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Thịt heo xay CP – Khay 300g'
           ,N'Trong 100gr thịt heo xay CP chứa: Lượng calo: 297; Nước: 53%; Protein: 25,7 gam; Carb: 0 gram; Đường: 0 gram; Chất xơ: 0 gram; Lipid: 20,8 gam.'
           ,N'Bảo quản từ 2 – 3 ngày trong ngăn mát tủ lạnh ở nhiệt độ 0 – 4 độ C. Hoặc bảo quản cấp đông ở nhiệt độ -18 độ C từ 7 – 15 ngày.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 34000, 0, 100, 0 , N'300g', N'Còn Hàng', '2022/10/01', '2022/10/01', 5)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (11, 'images/product/thit-heo-xay-cp-khay-300g.jpg', 'images/product/thit-heo-xay-cp-khay-300g-2.jpg', 'images/product/thit-heo-xay-cp-khay-300g (1).jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Thịt heo Ba chỉ (ba rọi) rút sườn CP – Khay 300g'
           ,N'Trong 100gr Thịt heo Ba chỉ (ba rọi) rút sườn CP chứa 16,5g protein; 10μg vitamin A; 21,5g mỡ; 178mg phosphor; 9mg canxi; 1,5mg sắt; 285mg kali; 1,91mg kẽm; 55mg natri.'
           ,N'Bảo quản từ 2 – 3 ngày trong ngăn mát tủ lạnh ở nhiệt độ 0 – 4 độ C. Hoặc bảo quản cấp đông ở nhiệt độ -18 độ C từ 7 – 15 ngày.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 69000, 0, 100, 0 , N'300g', N'Còn Hàng', '2022/10/01', '2022/10/01', 5)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (12, 'images/product/ba-chi-ba-roi-rut-suon-cp-khay-300.jpg', 'images/product/thit-heo-ba-chi-ba-roi-rut-suon-cp-khay-300g-4.jpg', 'images/product/thit-heo-ba-chi-ba-roi-rut-suon-cp-khay-300g-5.jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Thịt dăm heo CP – Khay 300g'
           ,N'Trong 100gr thịt dăm heo CP chứa: 176 kCal; Đạm: 17g; Chất béo: 12g.; Canxi: 8mg; Sắt: 1,2mg; Phospho: 186mg; Natri: 69mg; Kali: 324mg; Cholesterol: 67mg; Vitamin C: 1,5mg; Vitamin B1: 0,71mg; Vitamin PP: 3,9mg; Vitamin A: 6 microgam.'
           ,N'Bảo quản từ 2 – 3 ngày trong ngăn mát tủ lạnh ở nhiệt độ 0 – 4 độ C. Hoặc bảo quản cấp đông ở nhiệt độ -18 độ C từ 7 – 15 ngày.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 43000, 0, 100, 0 , N'300g', N'Còn Hàng', '2022/10/01', '2022/10/01', 5)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (13, 'images/product/thit-dam-heo-cp-khay-300g.jpg', 'images/product/thit-dam-heo-cp-khay-300g-1-1.jpg', 'images/product/thit-dam-heo-cp-khay-300g-1.jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Thịt heo ba chỉ (ba rọi) CP – Khay 300g'
           ,N'Trong 100gr Thịt heo Ba chỉ (ba rọi) CP chứa 16,5g protein; 10μg vitamin A; 21,5g mỡ; 178mg phosphor; 9mg canxi; 1,5mg sắt; 285mg kali; 1,91mg kẽm; 55mg natri.'
           ,N'Bảo quản từ 2 – 3 ngày trong ngăn mát tủ lạnh ở nhiệt độ 0 – 4 độ C. Hoặc bảo quản cấp đông ở nhiệt độ -18 độ C từ 7 – 15 ngày.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 50000, 0, 100, 0 , N'300g', N'Còn Hàng', '2022/10/01', '2022/10/01', 5)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (14, 'images/product/thit-heo-ba-chi-ba-roi-khay-300g-cp.jpg', 'images/product/thit-heo-ba-chi-ba-roi-cp-1.jpg', 'images/product/thit-heo-ba-chi-ba-roi-cp-khay-300g.jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Thịt cốt lết heo có xương CP – Khay 300g'
           ,N'Trong cốt lết heo có xương CP chứa nhiều chất béo, chất đạm, natri, kali, canxi và các vitamin A, vitamin B,… cần thiết cho cơ thể.'
           ,N'Bảo quản từ 2 – 3 ngày trong ngăn mát tủ lạnh ở nhiệt độ 0 – 4 độ C. Hoặc bảo quản cấp đông ở nhiệt độ -18 độ C từ 7 – 15 ngày.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 36500, 0, 100, 0 , N'300g', N'Còn Hàng', '2022/10/01', '2022/10/01', 5)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (15, 'images/product/thit-cot-let-heo-co-xuong-cp-khay-300g.jpg', 'images/product/thit-cot-let-heo-co-xuong-cp-khay-300g-1.jpg', 'images/product/thit-cot-let-heo-co-xuong-cp-khay-300g-1-1.jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Sườn non heo CP – Khay 300g'
           ,N'Thành phần protein trong sườn non heo CP chiếm 70% giá trị dinh dưỡng. Ngoài ra sường non heo còn bao gồm các dưỡng chất như: sắt, kali, chất béo, Vitamin B12, Vitaimin B6, Niacin…'
           ,N'Bảo quản từ 2 – 3 ngày trong ngăn mát tủ lạnh ở nhiệt độ 0 – 4 độ C. Hoặc bảo quản cấp đông ở nhiệt độ -18 độ C từ 7 – 15 ngày.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 69000, 0, 100, 0 , N'300g', N'Còn Hàng', '2022/10/01', '2022/10/01', 5)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (16, 'images/product/suon-non-heo-cp-khay-300g.jpg', 'images/product/suon-non-heo-cp-khay-300g-1-1099x800.jpg', 'images/product/suon-non-heo-cp-khay-300g-1-1.jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Xương ống heo CP – Khay 500g'
           ,N'Xương ống heo CP chứa hàm lượng calo: 110; chất béo: 12 gram; Protein: 1 gram; Vitamin B12: 7% giá trị dinh dưỡng khuyến nghị hằng ngày (RDI); Riboflavin: 6% RDI; Sắt: 4% RDI; Vitamin E: 2% RDI; Photpho: 1% RDI; Thiamine: 1% RDI; Vitamin A: 1% RDI.'
           ,N'Bảo quản từ 2 – 3 ngày trong ngăn mát tủ lạnh ở nhiệt độ 0 – 4 độ C. Hoặc bảo quản cấp đông ở nhiệt độ -18 độ C từ 7 – 15 ngày.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 29500, 0, 100, 0 , N'500g', N'Còn Hàng', '2022/10/01', '2022/10/01', 5)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (17, 'images/product/xuong-ong-heo-cp-khay-500g.jpg', 'images/product/xuong-ong-heo-cp-khay-500g-1.jpg', 'images/product/xuong-ong-heo-cp-khay-500g-1-1.jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Thịt bắp giò trước CP – Khay 300g'
           ,N'Thịt bắp giò trước CP chứa rất nhiều hàm lượng dinh dưỡng như: protein, canxi, sắt, vitamin,….'
           ,N'Bảo quản từ 2 – 3 ngày trong ngăn mát tủ lạnh ở nhiệt độ 0 – 4 độ C. Hoặc bảo quản cấp đông ở nhiệt độ -18 độ C từ 7 – 15 ngày.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 38000, 0, 100, 0 , N'300gb', N'Còn Hàng', '2022/10/01', '2022/10/01', 5)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (18, 'images/product/thit-bap-gio-heo-truoc-cp-khay-300g.jpg', 'images/product/thit-bap-gio-truoc-cp-khay-300g.jpg', 'images/product/thit-bap-gio-truoc-cp.jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Thịt heo đùi CP – Khay 300g'
           ,N'Trong 100gr thịt heo đùi CP chứa: 19g protein, 7g mỡ, 7mg canxi, 190mg phosphor, 1.5mg sắt, 2.5mg kẽm, 341mg kali, 76mg natri, 2μg vitamin A.'
           ,N'Bảo quản từ 2 – 3 ngày trong ngăn mát tủ lạnh ở nhiệt độ 0 – 4 độ C. Hoặc bảo quản cấp đông ở nhiệt độ -18 độ C từ 7 – 15 ngày.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 33000, 0, 100, 0 , N'300gb', N'Còn Hàng', '2022/10/01', '2022/10/01', 5)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (19, 'images/product/thit-heo-dui-cp-khay-300g.jpg', 'images/product/thit-heo-dui-cp-khay-300g-1.jpg', 'images/product/thit-heo-dui-cp-khay-300g-1-1.jpg', null , null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Mỡ heo tảng CP – Khay 300g'
           ,N'Mỡ heo tảng CP chứa hàm lượng dinh dưỡng dồi dào như: khoáng chất vitamin A, B, D, K, E…Thành phần chính của mỡ heo là chất béo. Trong mỡ heo chứa khoảng 39% chất béo bão hòa, khoảng 41,8% chất béo không bão hòa đơn và khoảng 4% chất béo không bão hòa đa.'
           ,N'Bảo quản từ 2 – 3 ngày trong ngăn mát tủ lạnh ở nhiệt độ 0 – 4 độ C. Hoặc bảo quản cấp đông ở nhiệt độ -18 độ C từ 7 – 15 ngày.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 20000, 0, 100, 0 , N'300gb', N'Còn Hàng', '2022/10/01', '2022/10/01', 5)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (20, 'images/product/mo-heo-tang-cp-khay-300g.jpg', 'images/product/mo-heo-tang-cp-khay-300g-2.jpg', 'images/product/mo-heo-tang-cp-khay-300g-1.jpg', 'images/product/mo-heo-tang-cp-khay-300g-1-1.jpg', null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Đậu xanh hột – Gói 150g'
           ,N'100g đậu xanh hột chứa: 328 Kcal – 1371 KJ; Carbohydrate: 53,1 g; Chất xơ: 4,7g; Chất đạm: 23,4 g; Chất béo: 2,4 g; Sắt: 4,8 mg; Magie: 270 mg; Canxi: 64 mg; Phốt pho: 377 mg; Kali: 1132 mg;'
           ,N'Cách bảo quản đậu xanh hột để lâu không mối mọt. Cho đậu xanh vào nước sôi ngâm từ 1-2 phút để diệt hết trứng mọt, sau đó đưa ra phơi khô, vào vào lọ kín đậy nắp để dành.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 10500, 0, 100, 0 , N'150g', N'Còn Hàng', '2022/10/01', '2022/10/01', 1)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (21, 'images/product/dau-xanh-hot-goi-150g.jpg', 'images/product/dau-xanh-hot-goi-150g-1.jpg', 'images/product/dau-xanh-hot-goi-150g-tuoi.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Đậu phộng sống – Gói 150g'
           ,N'Hàm lượng chất béo dao động từ 44 – 56% và chủ yếu là chất béo dạng đơn, là axit oleic (40 – 60%) và axit linoleic, hàm lượng protein dao động từ 22 – 30% calo, vitamin B3, folate (vitamin B9), mangan, vitamin E, thiamin (vitamin B1)'
           ,N'Cách bảo quản đậu xanh hột để lâu không mối mọt. Cho đậu xanh vào nước sôi ngâm từ 1-2 phút để diệt hết trứng mọt, sau đó đưa ra phơi khô, vào vào lọ kín đậy nắp để dành.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 15000, 0, 100, 0 , N'150g', N'Còn Hàng', '2022/10/01', '2022/10/01', 1)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (22, 'images/product/dau-phong-song-goi-150g.jpg', 'images/product/dau-phong-song-goi-150g-1.jpg', 'images/product/dau-phong-song-goi-150g-tuoi.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Đậu xanh không vỏ – Gói 150g'
           ,N'100g đậu xanh không vỏ chứa: 328 Kcal – 1371 KJ; Carbohydrate: 53,1 g; Chất xơ: 4,7g; Chất đạm: 23,4 g; Chất béo: 2,4 g; Sắt: 4,8 mg; Magie: 270 mg; Canxi: 64 mg; Phốt pho: 377 mg; Kali: 1132 mg;'
           ,N'Cách bảo quản hạt đậu xanh không vỏ để lâu không mối mọt. Cho đậu xanh vào nước sôi ngâm từ 1-2 phút để diệt hết trứng mọt, sau đó đưa ra phơi khô, vào vào lọ kín đậy nắp để dành.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 13000, 0, 100, 0 , N'150g', N'Còn Hàng', '2022/10/01', '2022/10/01', 1)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (23, 'images/product/dau-xanh-khong-vo-goi-150g.jpg', 'images/product/dau-xanh-khong-vo-goi-150g-1.jpg', 'images/product/dau-xanh-khong-vo-goi-150g-gia-tot.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Đậu đen xanh lòng – Gói 300g'
           ,N'Đậu đen xanh lòng chứa protit 24.2%, chất béo 1.7%, gluxit 53.3%, tro 2.8%. Bên cạnh đó, các chất canxi 56mg%, p 354mg%, sắt 6.1mg%, carten 0.06mg%, vitamin B, vitamin PP 0.51%, vitamin C 3mg%.'
           ,N'Bảo quản đậu đen xanh lòng nên phân loại các hạt, đậu bị hư, không đủ chất lượng, chỉ giữ những hạt bình thường to, tròn, săn chắc. Sau đó, bạn đem phơi dưới nắng mặt trời để loại bỏ hơi ẩm, trứng sâu mọt còn tồn trong hạt.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 28000, 0, 100, 0 , N'300g', N'Còn Hàng', '2022/10/01', '2022/10/01', 1)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (24, 'images/product/dau-den-xanh-long-goi-300g.jpg', 'images/product/dau-den-xanh-long-goi-300g-1.jpg', 'images/product/dau-den-xanh-long-goi-300g-1-1.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Đậu xanh cà – Loại 150g'
           ,N'100g đậu xanh cà chứa năng lượng: 328 Kcal – 1371 KJ; Carbohydrate: 53,1 g; Chất xơ: 4,7g; Chất đạm: 23,4 g; Chất béo: 2,4 g; Sắt: 4,8 mg; Magie: 270 mg; Canxi: 64 mg; Phốt pho: 377 mg; Kali: 1132 mg'
           ,N'Cách bảo quản hạt đậu xanh cà loại 150g để lâu không mối mọt. Cho đậu xanh vào nước sôi ngâm từ 1-2 phút để diệt hết trứng mọt, sau đó đưa ra phơi khô, vào vào lọ kín đậy nắp để dành'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 28000, 0, 100, 0 , N'300g', N'Còn Hàng', '2022/10/01', '2022/10/01', 1)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (25, 'images/product/dau-xanh-ca-goi-150g.jpg', 'images/product/dau-xanh-ca-loai-150g.jpg', 'images/product/dau-xanh-ca-loai-150g-gia-tot.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Tôm thẻ nguyên con CP (Premium) – Túi 500g'
           ,N'Theo nghiên cứu tôm thẻ nguyên con CP chứa hàm lượng caxi rất cao, chứa nhiều chất đạm, các vitamin và nguyên tố vi lượng. So với thịt nạc, lượng đạm của tôm biển cao hơn 20%, ít chất béo hơn khoảng 40%, lượng vitamin A cao hơn chừng 40%…'
           ,N'Sản phẩm nên sử dụng ngay để đảm bảo được độ tươi ngon, nếu không sử dụng kịp, bạn có thể bảo quản trong ngăn đông tủ lạnh ở nhiệt độ dưới -18 độ C.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 102000, 0, 100, 0 , N'500g', N'Còn Hàng', '2022/10/01', '2022/10/01', 4)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (26, 'images/product/tom-the-cp-dong-lanh-500g.jpg', 'images/product/tom-the-nguyen-con-cp-premium-size-41-50-tui-500g.jpg', 'images/product/tom-the-nguyen-con-cp-premium-size-41-50-tui-500g-1.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Cá điêu hồng (làm sạch) – Loại 1kg'
           ,N'100g cá điêu hồng chứa những thành phần sau đây: Protein: 26gr; Chất béo: 3gr; Niacin: 24% RDI; Vitamin B12: 31% RDI; Photpho: 20% RDI; Selen: 78% RDI; Kali: 20% RDI.'
           ,N'Sản phẩm nên sử dụng ngay để đảm bảo được độ tươi ngon, nếu không sử dụng kịp, bạn có thể bảo quản trong ngăn đông tủ lạnh ở nhiệt độ dưới -18 độ C.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 81500, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 4)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (27, 'images/product/ca-dieu-hong.jpg', 'images/product/ca-dieu-hong-lam-sach-loai-1kg-2.jpg', 'images/product/ca-dieu-hong-lam-sach-loai-1kg-3.jpg', 'images/product/ca-dieu-hong-lam-sach-loai-1kg-4.jpg', null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Cá hú nguyên con (làm sạch, bỏ đầu) – Loại 1kg'
           ,N'Trong cá hú nguyên con làm sạch bỏ đầu chứa hàm lượng protein cao, ngoài ra cá hú còn chứa nhiều thành phần dinh dưỡng quan trọng, cần thiết đối với cơ thể con người như: Chất đạm, chất béo, omega 3, vitamin D…'
           ,N'Sản phẩm nên sử dụng ngay để đảm bảo được độ tươi ngon, nếu không sử dụng kịp, bạn có thể bảo quản trong ngăn đông tủ lạnh ở nhiệt độ dưới -18 độ C.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 119500, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 4)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (28, 'images/product/ca-hu-nguyen-con-lam-sach-bo-dau-loai-1kg.jpg', 'images/product/ca-hu-nguyen-con-lam-sach-bo-dau-loai-1kg-2.jpg', 'images/product/ca-hu-nguyen-con-lam-sach-bo-dau-loai-1kg-1.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Cá đù 1 nắng Phan Thiết – Loại 500g'
           ,N'Trong cá đù 1 nắng Phan Thiết là nguồn cung cấp nhóm B. Bên cạnh đó, nó còn giàu vitamin A, D và giúp bổ sung các khoáng chất cần thiết cho người dùng như: canxi, flo, photpho, iot, protein, omega 3, DHA cùng các yếu tố vi lượng như đồng, kẽm…'
           ,N'Sản phẩm nên sử dụng ngay để đảm bảo được độ tươi ngon, nếu không sử dụng kịp, bạn có thể bảo quản trong ngăn đông tủ lạnh ở nhiệt độ dưới -18 độ C.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 114500, 0, 100, 0 , N'500g', N'Còn Hàng', '2022/10/01', '2022/10/01', 4)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (29, 'images/product/ca-du-1-nang-phan-thiet-loai-500g.jpg', 'images/product/ca-du-1-nang-phan-thiet-loai-500g-2.jpg', 'images/product/ca-du-1-nang-phan-thiet-loai-500g-1.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Cá basa cắt khúc (cá tra) đông lạnh CP – Loại 1kg'
           ,N'Trong bao tử cá basa cắt khúc chứa hàm lượng calo là: 158; Protein: 22,5 gram.; Chất béo: 7 gram; Chất béo bão hòa: 2 gram; Cholesterol: 73 mg; Carbs: 0 gram; Natri: 89 mg…'
           ,N'Sản phẩm nên sử dụng ngay để đảm bảo được độ tươi ngon, nếu không sử dụng kịp, bạn có thể bảo quản trong ngăn đông tủ lạnh ở nhiệt độ dưới -18 độ C.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 81000, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 4)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (30, 'images/product/ca-basa-cat-khuc-dong-lanh-CP-500G.jpg', 'images/product/ca-basa-cat-khuc-ca-tra-dong-lanh-cp-loai-1kg.jpg', 'images/product/ca-basa-cat-khuc-ca-tra-dong-lanh-cp-loai-1kg-1.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Nghêu sống – Loại 1kg'
           ,N'Trong 100g thịt nghêu có chứa 10,8g chất đạm; 1,6g chất béo; nhiều vi tố nguyên lượng khác như kẽm, sắt, kali, mangan, đồng, iot… và các vitamin B1, B6, B12 và vitamin C'
           ,N'Sản phẩm nên sử dụng ngay để đảm bảo được độ tươi ngon, nếu không sử dụng kịp, bạn có thể bảo quản trong ngăn đông tủ lạnh ở nhiệt độ dưới -18 độ C.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 62500, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 4)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (31, 'images/product/ngheu-song-loai-1kg-2.jpg', 'images/product/ngheu-song-loai-1kg.jpg', 'images/product/ngheu-song-loai-1kg-1.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Cá chép – Loại 1kg'
           ,N'Trong 100g cá chép chứa hàm lượng dinh dưỡng gồm chất đạm 38,9g 78%; Vitamin A 1%; Vitamin C 5%; Canxi 9%; Sắt 15%; Thiamin 16%; Riboflavin 7%; Vitamin B6 19%; Vitamin B12 42%; Niacin 18%; Magie 16%; Phốt pho 90%; Kẽm 22%; Đồng 6%; Axit pantothenic 15%.'
           ,N'Sản phẩm nên sử dụng ngay để đảm bảo được độ tươi ngon, nếu không sử dụng kịp, bạn có thể bảo quản trong ngăn đông tủ lạnh ở nhiệt độ dưới -18 độ C.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 88000, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 4)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (32, 'images/product/ca-chep-loai-1kg.jpg', 'images/product/ca-chep-loai-1kg-1.jpg', 'images/product/ca-chep.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Cá nục thường (làm sạch) – Loại 1kg'
           ,N'Trong 100g cá nục thường làm sạch cấp đông có chữa khoảng 120 calo, cùng với đó là 20,2g chất đạm; 85mg canxi và 3,3g chất béo, 76,3g nước và 160mg phốt pho…'
           ,N'Sản phẩm nên sử dụng ngay để đảm bảo được độ tươi ngon, nếu không sử dụng kịp, bạn có thể bảo quản trong ngăn đông tủ lạnh ở nhiệt độ dưới -18 độ C.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 94500, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 4)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (33, 'images/product/ca-nuc-thuong-lam-sach-loai-1kg-2.jpg', 'images/product/ca-nuc-thuong-lam-sach-loai-1kg-1.jpg', 'images/product/ca-nuc-thuong-lam-sach-loai-1kg.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Mực trứng Phan Thiết – Khay 500g'
           ,N'Trong 85g mực trứng Phan Thiết cho chứa đến: 90% đồng, 63% selen, 30% protein, 23% vitamin B2 và hàng chục dưỡng chất khác…'
           ,N'Sản phẩm nên sử dụng ngay để đảm bảo được độ tươi ngon, nếu không sử dụng kịp, bạn có thể bảo quản trong ngăn đông tủ lạnh ở nhiệt độ dưới -18 độ C.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 126500, 0, 100, 0 , N'500g', N'Còn Hàng', '2022/10/01', '2022/10/01', 4)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (34, 'images/product/muc-trung-phan-thiet-khay-500g-2.jpg', 'images/product/muc-trung-phan-thiet-khay-500g-1.jpg', 'images/product/muc-trung-phan-thiet-khay-500g.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Cồi sò điệp Phan Thiết – Hộp 500g'
           ,N'Trong 84 gram cồi sò điệp chứa hàm lượng Calo là: 94; Chất béo: 1,2 gram; Protein: 19,5 gram; Axit béo omega-3: 333 mg; Vitamin B12: 18%'
           ,N'Sản phẩm nên sử dụng ngay để đảm bảo được độ tươi ngon, nếu không sử dụng kịp, bạn có thể bảo quản trong ngăn đông tủ lạnh ở nhiệt độ dưới -18 độ C.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 114500, 0, 100, 0 , N'500g', N'Còn Hàng', '2022/10/01', '2022/10/01', 4)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (35, 'images/product/coi-so-diep-phan-thiet-hop-500g-3.jpg', 'images/product/coi-so-diep-phan-thiet-hop-500g-2.jpg', 'images/product/coi-so-diep-phan-thiet-hop-500g-1.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Cam sành'
           ,N'Mỗi 100 gram cam sành chứa: 87,6 g nước; 104 microgram carotene – một loại vitamin chống oxy hóa; 30 mg vitamin C; 10,9 g chất tinh bột; 93 mg kali; 26 mg canxi; 9 mg magnesium; 0,3 g chất xơ; 4,5 mg natri; 7 mg Chromium; 20 mg phốt pho; 0,32 mg sắt'
           ,N'Để trái cam bảo quản lâu vẫn ngon nhất thì cần chọn cam tươi, quả con nguyên cuống và lá, lá có thể hơi khô những vẫn dính trên cuống.Cam mua về tốt nhất vẫn nên rửa sạch và ngâm muối để đảm bảo loại bỏ tồn dư hóa chất trên phần da quả.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 28000, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 3)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (36, 'images/product/cam-sanh-loai-1kg.jpg', 'images/product/cam-sanh.jpg', 'images/product/cam-sanh-tuoi.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Trái Ổi – Loại 1kg'
           ,N'ổi chứa các loại vitamin và khoáng chất sau: Chất xơ; Nước; Kali; Sắt; Carbohydrate; Chất béo; Phospho; Protein; Natri; Magie; Kẽm; Canxi; Đồng; Choline; Mangan; Axit Pantothenic; Vitamin A; Vitamin C; Vitamin D; Vitamin B6; Vitamin B12; Vitamin K…'
           ,N'Mua ổi về cần chọn lựa những quả ổi bị hỏng, bị dập, có vết thâm đen ra riêng, dùng dao sạch cắt bỏ đi những phần bị hỏng nhiều nhất có thể. Cho vào túi zip, túi nilong hoặc túi lưới, không nên bịt kín miệng túi, có các lỗ nhỏ để không khí lưu thông'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 17000, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 3)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (37, 'images/product/trai-oi-loai-1kg.jpg', 'images/product/trai-oi-tuoi.jpg', 'images/product/trai-oi.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Dưa hấu – Loại 1kg'
           ,N'Cứ 100g dưa hấu có chứa tới Calo: 30; Nước: 91%; Protein: 0,6 gram; Carbs: 7,6 gram; Đường: 6,2 gram; Chất xơ: 0,4 gram; Chất béo: 0,2 gram.'
           ,N'Bảo quản dưa hấu nguyên trái để trong ngăn mát tủ lạnh, nếu dưa hấu đã cắt ra thì dùng màng bọc thực phẩm để gói kín phần dưa đã cắt còn để vỏ. Bảo quản dưa hấu trong ngăn đựng riêng để tránh bị nhiễm khuẩn'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 18000, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 3)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (38, 'images/product/dua-hau (1).jpg', 'images/product/dua-hau-loai-1kg.jpg', 'images/product/dua-hau.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Thơm trái loại thường - Trái'
           ,N'Thơm là nguồn chứa rất nhiều chất dinh dưỡng có lợi cho sức khỏe kali, đồng, mangan, canxi, magiê, vitamin C, beta-caroten, thiamin, B6 và folate, chất xơ hòa tan/không hòa tan và bromelain.'
           ,N'Thơm chưa sơ chế hay cắt thái thì bạn có thể bảo quản chúng trong khoảng từ 5 ngày đến 1 tuần. Điều bạn cần là để chúng thật sự khô ráo, tránh đặt trực tiếp dưới ánh nắng mặt trời hay những nơi ẩm ướt.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 14000, 0, 100, 0 , N'Trái', N'Còn Hàng', '2022/10/01', '2022/10/01', 3)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (39, 'images/product/thom-trai-thuong.jpg', 'images/product/thom-got.jpg', 'images/product/thom-trai-thuong-tuoi.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Thanh long ruột đỏ – Loại 1kg'
           ,N'Mỗi 100g thanh long ruột đỏ chứa 2,83g fructose và 7,83g glucose. Ngoài vitamin C, nó còn chứa 23,3mg axit malic, 2,8mg axit shikimic, 20mg axit oxalic, 19,1mg axit quinic, axit succinic, axit fumaric'
           ,N'Nếu sử dụng trong thời ngắn từ 3 – 5 ngày, bạn hoàn toàn có thể bảo quản thanh long ruột đỏ ở nhiệt độ trong phòng (từ 20 – 24 độ C), với không gian thoáng mát và tránh ánh nắng trực tiếp.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 28000, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 3)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (40, 'images/product/thanh-long-ruot-do-loai-1kg.jpg', 'images/product/thanh-long-ruot-do-tuoi.jpg', 'images/product/thanh-long-ruot-do.jpg', 'images/product/thanh-long-ruot-do-loai-1kg-1.jpg', null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Táo đỏ (táo mèo lớn) – Loại 1kg'
           ,N'Trong táo mèo có chứa nhiều thành phần hoạt chất có lợi cho sức khỏe như phenolic, flavonoid, axit triterpenic, vitamin C và polysaccharide. Bên cạnh đó nó còn chứa hàm lượng vitamin, khoáng chất, chất xơ cực kỳ cao.'
           ,N'Bảo quản táo mèo trong túi zip đặt trong ngăn mát tủ lạnh, hoặc có thể phơi khô.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 63000, 0, 0, 0 , N'1kg', N'Hết Hàng', '2022/10/01', '2022/10/01', 3)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (41, 'images/product/tao-do-tao-meo-lon-sach.jpg', 'images/product/tao-do-tao-meo-lon-loai-1kg.jpg', 'images/product/tao-do-tao-meo.jpg', 'images/product/tao-do-tao-meo-lon.jpg', null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Bơ sáp – Loại 1kg'
           ,N'Trong 100 gram bơ sáp có chứa các chất dinh dưỡng như: Vitamin K: 26% giá trị hàng ngày (DV); Folate: 20% của DV; Vitamin C: 17% DV; Kali: 14% của DV; Vitamin B5: 14% của DV; Vitamin B6: 13% của DV; Vitamin E: 10% của DV.'
           ,N'Quét một ít nước cốt chanh phủ lên bề mặt quả bơ sáp rồi sau đó cho đưa vào túi zip hoặc túi hút chân không để cho vào ngăn đá tủ lạnh.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 42000, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 3)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (42, 'images/product/bo-sap.jpg', 'images/product/bo-sap-da-lat.jpg', 'images/product/bo-sap (1).jpg', 'images/product/bo-sap-loai-1kg.jpg', null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Nhãn lồng – Loại 1kg'
           ,N'Trong 100g nhãn lồng sẽ cung cấp các thành phần dinh dưỡng sau đây: Calo: 48; Nước: 86.3g; Protein: 0.9g; Carbohydrate: 10.9g; Lipid: 0.1g; Chất xơ: 1.0g; Canxi: 21mg; Sắt: 0.4mg; Mangan: 0.1mg; Magie: 10mg; '
           ,N'Nhãn lồng rửa sạch, dùng kéo cắt nhãn ra khỏi cành (cắt sát núm), để ráo nước, loại bỏ những quả hỏng rồi cho vào túi ni long có khe hở rồi đặt trong ngăn mát tủ lạnh để bảo quản'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 68000, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 3)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (43, 'images/product/nhan-long-loai-1kg.jpg', 'images/product/nhan-long-1.jpg', 'images/product/nhan-long-tuoi.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Dưa lưới – Loại 1kg'
           ,N'Dưa lưới cung cấp rất nhiều tiền vitamin A (β-carotene), vitamin C, các loại dinh dưỡng như vitamin E và axit folic.'
           ,N'Để bảo quản dưa lưới chín, bạn hãy dùng màng nhựa bọc dưa lưới lại rồi cho vào hộc đựng hoa quả trong tủ lạnh, thêm vào đó, bạn nên bảo quản dưa lưới ở nhiệt độ từ 0-15 độ C sẽ giúp dưa có được độ tươi ngon mong muốn'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 56000, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 3)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (44, 'images/product/dua-luoi.jpg', 'images/product/dua-luoi-loai-1kg.jpg', 'images/product/dua-luoi-loai-2kg.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Dâu tây Đà Lạt – Loại 1kg'
           ,N'100g dâu tây Đà Lạt chứa các thành phần dinh dưỡng như: Nước: 91% nước; Năng lượng: 32 calo; Carbs: 7,7 gram; Protein: 0,7 gram; Đường: 4,9 gram; Chất béo: 0,3 gram; Chất xơ: 2 gram; '
           ,N'Cách bảo quản dâu tây Đà Lạt trong ngăn mát như sau: gói dâu tây trong hộp giấy để chúng hút ẩm, đậy kín và cho vào hộc đựng rau trong tủ lạnh, dâu tây sẽ tươi từ 5 đến 7 ngày. '
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 209000, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 3)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (45, 'images/product/dau-da-lat-sach.jpg', 'images/product/dau-da-lat.jpg', 'images/product/dau-da-lat-loai-1kg.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Bông cải xanh – Loại 1kg'
           ,N'Trong 91 grams bông cải tươi chứa: Calo: 31; Nước: 89%; Đạm: 2.5 grams; Carbohydrates: 6 grams; Đường: 1.5 grams; Chất xơ: 2.4 grams; Chất béo: 0.4 grams; Vitamin C: 135% RDI; Vitamin A: 11% RDI; Vitamin K: 116% RDI; Vitamin B9 (Folate): 14% RDI.'
           ,N'Với bông cải xanh hay súp lơ xanh, cách bảo quản bạn cho vào túi nilon hoặc hộp đựng thực phẩm, nhớ khoét nhiều lỗ để thông khí rồi cho bông cải xanh vào ngăn mát tủ lạnh hoặc đặt ở nơi khô thoáng.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 66000, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 2)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (46, 'images/product/bong-cai-xanh-loai-1kg (1).jpg', 'images/product/bong-cai-xanh-loai-1kg.jpg', 'images/product/bong-cai-xanh.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Su su bào sợi – Loại 1kg'
           ,N'Một quả su su bào sợi khoảng 203g cung cấp các chất dinh dưỡng như sau: Calo: 39; Carb: 9g; Protein: 2g; Chất béo: 0g; Chất xơ: 4g, chiếm 14% lượng tiêu thụ được khuyến cáo hàng ngày (RDI); Vitamin C: 26% RDI; Vitamin B9 (folate): 47% RDI'
           ,N'Bảo quản su su bào sợi trong ngăn mát tủ lạnh đặt trong hộp kín.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 24000, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 2)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (47, 'images/product/su-su-bao-soi-loai-1kg.jpg', 'images/product/su-su-bao-soi-gia-tot.jpg', 'images/product/su-su-bao-soi.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Bông Cải Trắng – Loại 1kg'
           ,N'Trong 100g bông cải trắng chứa: Năng lượng khoảng 25kcal. Chất xơ 2,5g, carbohydrate 5.3g, chất béo 0.1g, protein 2g,… Vitamin C 46.4mg, vitamin K 16 mcg, vitamin B6 0.2 mcg, vitamin B5 0.7 mg, thiamine 0.1 mg, riboflavin 0.1 mg, folate 57mcg.'
           ,N'Với bông cải trắng hay súp lơ trắng, cách bảo quản tốt nhất là bạn dùng túi nilon có đục lỗ để đựng và cất chúng trong ngăn mát tủ lạnh hoặc nơi mát, tránh ánh sáng trực tiếp'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 56000, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 2)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (48, 'images/product/bong-cai-trang-loai-1kg.jpg', 'images/product/bong-cai-trang.jpg', 'images/product/bong-cai-trang-loai-1kg (1).jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Bông bí – Loại 1kg'
           ,N'Tiêu thụ 33 gram bông bí cung cấp: 9,2 mg Vitamin C; 19mg Vitamin B9; 32mg vitamin A; 0,23 mg Sắt; 16 mg Phốt pho; 0,025 mg Vitamin B2; 8 mg Magiê; 0,2 mg Selenium; 0,228 mg Vitamin B3.'
           ,N'Bảo quản bông bí trong túi zip, nilon để trong tủ lạnh.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 56000, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 2)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (49, 'images/product/bong-bi.jpg', 'images/product/bong-bi (1).jpg', 'images/product/bong-bi-loai-1kg.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Chuối bào – Loại 1kg'
           ,N'Bắp chuối bao gồm chất xơ, kali, canxi, đồng, phốt pho, sắt, magiê và vitamin E, Protein: 1.6g , Chất béo: 0.6g , Carbohydrate: 9.9g , Chất xơ: 5.7g , Canxi: 56mg , Phốt pho: 73.3mg, Sắt: 56.4mg , Đồng: 13mg , Kali: 553.3mg.'
           ,N'Bảo quản bắp chuối xào trong túi zip, nilon hoặc hộp kín để trong ngăn mát tủ lạnh.'
           ,N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 35000, 0, 100, 0 , N'1kg', N'Còn Hàng', '2022/10/01', '2022/10/01', 2)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (50, 'images/product/chuoi-bao-sach.jpg', 'images/product/chuoi-bao-loai-1kg.jpg', 'images/product/chuoi-bao.jpg', null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Muối i ốt – Loại 500g'
           , null
           , null
           , N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 5000, 0, 100, 0 , N'500g', N'Còn Hàng', '2022/10/01', '2022/10/01', 7)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (51, null, 'images/product/muoi-i-ot-goi-500g.jpg', null, null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Bơ Tường An – Loại 200g'
           , null
           , null
           , N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 19500, 0, 100, 0 , N'Hộp', N'Còn Hàng', '2022/10/01', '2022/10/01', 7)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (52, null, 'images/product/bo-tuong-an-200g.jpg', null, null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Nước tương Magi (nắp vàng) – Loại 700ml'
           , null
           , null
           , N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 28000, 0, 100, 0 , N'Chai', N'Còn Hàng', '2022/10/01', '2022/10/01', 7)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (53, null, 'images/product/nuoc-tuong-maggi-chai-700ml.jpg', null, null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Tương cà Cholimex – Loại 270g'
           , null
           , null
           , N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 12500, 0, 100, 0 , N'Chai', N'Còn Hàng', '2022/10/01', '2022/10/01', 7)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (54, null, 'images/product/tuong-ot-cholimex-270g.jpg', null, null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Hạt nêm Knorr Heo – Loại 400g'
           , null
           , null
           , N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 34000, 0, 100, 0 , N'Gói', N'Còn Hàng', '2022/10/01', '2022/10/01', 7)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (55, null, 'images/product/hat-nem-knorr-goi-400g.jpg', null, null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Đường vàng – Loại 1kg'
           , null
           , null
           , N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 36500, 0, 100, 0 , N'Gói', N'Còn Hàng', '2022/10/01', '2022/10/01', 7)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (56, null, 'images/product/duong-vang-bien-hoa-goi-1kg.jpg', null, null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Dầu ăn Tường An – Loại 1 lít'
           , null
           , null
           , N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 52500, 0, 100, 0 , N'Chai', N'Còn Hàng', '2022/10/01', '2022/10/01', 7)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (57, null, 'images/product/dau-an-tuong-an-1l.jpg', null, null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Bột ngọt Ajinomoto – Loai 1kg'
           , null
           , null
           , N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 72000, 0, 100, 0 , N'Gói', N'Còn Hàng', '2022/10/01', '2022/10/01', 7)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (58, null, 'images/product/bot-ngot-ajinomoto-loai-1kg.jpg', null, null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Gia vị La gu Nam Ấn – Loại 50g'
           , null
           , null
           , N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 6000, 0, 100, 0 , N'Gói', N'Còn Hàng', '2022/10/01', '2022/10/01', 7)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (59, null, 'images/product/gia-vi-nau-la-gu.jpg', null, null, null)
GO

INSERT INTO [dbo].[Products]
           ([productName], [productDescribe], [productPreservation], [productTitle], [makerName], [productPrice], [productDiscount], [productQuantity], [productSeller]
           ,[productWeight], [productStatus], [productTimeCreate], [productTimeUpdate], [categoryID])
     VALUES
           (N'Sauce thịt nướng Cholimex – Chai 600g'
           , null
           , null
           , N'Chất lượng hàng thực phẩm tươi mới, an toàn, sạch xanh, Giá bán cạnh tranh, ưu đãi mỗi ngày, Giao hàng đúng hẹn, đúng và đủ số lượng cam kết, Chính sách đổi trả – hoàn tiền dễ dàng, nhanh chóng, Nhân viên Tư vấn – Giao hàng thân thiện, nhiệt tình, tận tâm'
           , NULL, 57000, 0, 100, 0 , N'Gói', N'Còn Hàng', '2022/10/01', '2022/10/01', 7)
GO
INSERT INTO [dbo].[ProductImages]
           ([productID], [image1], [image2], [image3], [image4], [image5])
     VALUES
           (60, null, 'images/product/sauce-uop-thit-nuong-cholimex-chai-600g.jpg', null, null, null)
GO